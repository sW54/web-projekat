package repository;

import model.Entity;

import java.io.*;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public abstract class Table<T extends Entity> {
    private final String tableName;
    private final String storagePath;
    private final Map<Integer, T> instances;
    private final Map<Integer, T> deletedInstances;
    private final Map<String, Table<?>> foreignTables;
    private int nextId;

    public Table(String tableName, String storagePath) {
        this.tableName = tableName;
        this.storagePath = storagePath;
        this.instances = new HashMap<Integer, T>();
        this.deletedInstances = new HashMap<Integer, T>();
        this.foreignTables = new HashMap<String, Table<?>>();
        this.nextId = 0;
        this.createCSVFile();
    }

    public Map<Integer, T> all() {
        return instances;
    }

    public T get(int id) {
        return instances.get(id);
    }

    public T get(String id) {
        return get(Integer.parseInt(id));
    }

    public void add(T instance) {
        if (instance.isStored()) {
            instances.put(instance.getId(), instance);
        } else {
            instance.setId(nextId);
            instances.put(nextId++, instance);
        }
    }

    public void add(T... instances) {
        Arrays.stream(instances).forEach(this::add);
    }

    public void remove(T instance)  {
        int id = instance.getId();
        T storedInstance = instances.get(id);
        if (!instance.equals(storedInstance))
            return;

        instances.remove(id);
        instance.setDeleted(true);
        deletedInstances.put(id, instance);
    }

    public void remove(int id) {
        T instance = instances.get(id);
        instances.remove(id);
        instance.setDeleted(true);
        deletedInstances.put(id, instance);
    }

    public void remove(String id) {
        remove(Integer.parseInt(id));
    }

    public void saveChanges() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(storagePath + tableName + ".csv"), false);
            instances.values().forEach(e -> out.println(e.serialize()));
            deletedInstances.values().forEach(e -> out.println(e.serialize()));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            instances.clear();
            BufferedReader in = new BufferedReader(new FileReader(storagePath + tableName + ".csv"));
            String line;
            while ((line = in.readLine()) != null) {
                if (line.equals("") || line.startsWith("#"))
                    continue;
                T instance = deserialize(line);
                if (!instance.isDeleted())
                    instances.put(instance.getId(), instance);
            }
            in.close();
            nextId = 1 + Math.max(instances.keySet().stream().max(Comparator.naturalOrder()).orElse(-1),
                                  deletedInstances.keySet().stream().max(Comparator.naturalOrder()).orElse(-1));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addForeign(Table<?>... tables) {
        Arrays.stream(tables).forEach(e -> foreignTables.put(e.tableName, e));
    }

    protected Table<?> fromTable(String tableName) {
        return foreignTables.get(tableName);
    }

    private void createCSVFile() {
        try {
            File file = new File(storagePath + tableName + ".csv");
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract T deserialize(String line);

    public static String urlDecode(String encoded) {
        try {
            return URLDecoder.decode(encoded, "UTF-8");
        } catch (UnsupportedEncodingException ignore) {
            return "";
        }
    }
}
