package repository;

import model.Friendship;
import model.User;
import repository.annotation.Query;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FriendshipTable extends Table<Friendship> {

    public FriendshipTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    @Query
    public List<User> getByUser(User user) {
        return all().values().stream().map(e -> {
                if (e.getFirst().equals(user))
                    return e.getSecond();
                if (e.getSecond().equals(user))
                    return e.getFirst();
                return null; })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Query
    public Friendship getByUsers(User first, User second) {
        return all().values().stream().filter(e -> (e.getFirst().equals(first) && e.getSecond().equals(second)) ||
                                                   (e.getFirst().equals(second) && e.getSecond().equals(first))).findFirst().orElse(null);
    }

    @Query
    public boolean areFriends(User first, User second) {
        return first.getFriends().contains(second);
    }

    @Override
    protected Friendship deserialize(String line) {
        String[] fields = line.split("\\|");
        Friendship friendship = new Friendship(
                (User) fromTable("users").get(fields[2]),
                (User) fromTable("users").get(fields[3]));
        friendship.setId(Integer.parseInt(fields[0]));
        friendship.setDeleted(Boolean.parseBoolean(fields[1]));

        return friendship;
    }
}
