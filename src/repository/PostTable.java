package repository;

import model.Post;
import model.User;
import model.enums.PostType;
import repository.annotation.Query;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PostTable extends Table<Post> {

    public PostTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    @Query
    public List<Post> getByAuthor(User author) {
        return all().values().stream()
                .filter(e -> e.getAuthor().equals(author))
                .sorted(Comparator.comparing(Post::getTime).reversed())
                .collect(Collectors.toList());
    }

    @Override
    protected Post deserialize(String line) {
        String[] fields = line.split("\\|");
        Post post = new Post(
                (User) fromTable("users").get(fields[2]),
                fields[3].equals("") ? null : fields[3],
                fields[4].equals("") ? null : urlDecode(fields[4]),
                LocalDateTime.parse(fields[5]),
                PostType.valueOf(fields[6]));
        post.setId(Integer.parseInt(fields[0]));
        post.setDeleted(Boolean.parseBoolean(fields[1]));

        return post;
    }

    public void updateExtra(CommentTable comments, LikeTable likes) {
        for (Post post : all().values()) {
            post.setComments(comments.getByPost(post));
            post.setLikes(likes.getByPost(post));
        }
    }
}
