package repository;

import model.Comment;
import model.Post;
import model.User;
import repository.annotation.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class CommentTable extends Table<Comment> {

    public CommentTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    @Query
    public List<Comment> getByPost(Post post) {
        return all().values().stream().filter(e -> e.getPost().equals(post)).collect(Collectors.toList());
    }

    @Override
    protected Comment deserialize(String line) {
        String[] fields = line.split("\\|");
        Comment comment = new Comment(
                (User) fromTable("users").get(fields[2]),
                (Post) fromTable("posts").get(fields[3]),
                urlDecode(fields[4]),
                LocalDateTime.parse(fields[5]),
                fields[6].equals("null") ? null : LocalDateTime.parse(fields[6]));
        comment.setId(Integer.parseInt(fields[0]));
        comment.setDeleted(Boolean.parseBoolean(fields[1]));

        return comment;
    }
}
