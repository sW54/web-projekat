package repository;

import model.Conversation;
import model.Message;
import model.User;
import repository.annotation.Query;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ConversationTable extends Table<Conversation> {

    public ConversationTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    @Query
    public List<Conversation> getByUser(User user) {
        return all().values().stream()
                .filter(e -> (e.getFirstParticipant().equals(user) || e.getSecondParticipant().equals(user)))
                .sorted(Comparator.comparing(Conversation::getTime).reversed())
                .collect(Collectors.toList());
    }

    @Query
    public Conversation getByUserPair(User first, User second) {
        return all().values().stream()
                .filter(e -> (e.getFirstParticipant().equals(first) & e.getSecondParticipant().equals(second)) ||
                             (e.getFirstParticipant().equals(second) & e.getSecondParticipant().equals(first)))
                .findFirst().orElse(null);
    }

    @Override
    protected Conversation deserialize(String line) {
        String[] fields = line.split("\\|");
        Conversation conversation = new Conversation(
                (User) fromTable("users").get(fields[2]),
                (User) fromTable("users").get(fields[3]),
                LocalDateTime.parse(fields[4]),
                Boolean.parseBoolean(fields[5]));
        conversation.setId(Integer.parseInt(fields[0]));
        conversation.setDeleted(Boolean.parseBoolean(fields[1]));

        return conversation;
    }

    public void updateExtra(MessageTable messages) {
        for (Conversation c : all().values()) {
            List<Message> m = messages.getByConversation(c);
            c.setMessages(m);

            if (m.size() > 0)
                c.setTime(m.get(m.size()-1).getTime());
        }
    }
}
