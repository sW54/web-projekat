package repository;

import model.Conversation;
import model.Message;
import model.User;
import repository.annotation.Query;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MessageTable extends Table<Message> {

    public MessageTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    @Query
    public List<Message> getByConversation(Conversation conversation) {
        return all().values().stream()
                .filter(e -> e.getConversation().equals(conversation))
                .sorted(Comparator.comparing(Message::getTime))
                .collect(Collectors.toList());
    }

    @Override
    protected Message deserialize(String line) {
        String[] fields = line.split("\\|");
        Message message = new Message(
                (Conversation) fromTable("conversations").get(fields[2]),
                (User) fromTable("users").get(fields[3]),
                urlDecode(fields[4]),
                LocalDateTime.parse(fields[5]));
        message.setId(Integer.parseInt(fields[0]));
        message.setDeleted(Boolean.parseBoolean(fields[1]));

        return message;
    }
}
