package repository;

import model.User;
import model.enums.Role;
import repository.annotation.Query;

import java.time.LocalDate;

public class UserTable extends Table<User> {

    public UserTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    @Query
    public User getByUsername(String username) {
        return all().values().stream()
                .filter(e -> e.getUsername().equals(username))
                .findFirst()
                .orElse(null);
    }

    @Query
    public User getByEmail(String email) {
        return all().values().stream()
                .filter(e -> e.getEmail().equals(email))
                .findFirst()
                .orElse(null);
    }


    @Override
    protected User deserialize(String line) {
        String[] fields = line.split("\\|");
        User user = new User(
                fields[2],
                fields[3],
                fields[4],
                fields[5],
                fields[6],
                LocalDate.parse(fields[7]),
                fields[8],
                Role.valueOf(fields[9]),
                fields[10].equals("null") ? null : fields[10],
                urlDecode(fields[11]),
                Boolean.parseBoolean(fields[12]),
                Boolean.parseBoolean(fields[13]));
        user.setId(Integer.parseInt(fields[0]));
        user.setDeleted(Boolean.parseBoolean(fields[1]));

        return user;
    }

    public void updateExtra(RequestTable requests, FriendshipTable friendships, PostTable posts, ConversationTable conversations) {
        for (User user : all().values()) {
            user.setSentRequests(requests.getBySender(user));
            user.setReceivedRequests(requests.getByReceiver(user));
            user.setFriends(friendships.getByUser(user));
            user.setPosts(posts.getByAuthor(user));
            user.setConversations(conversations.getByUser(user));
        }
    }
}
