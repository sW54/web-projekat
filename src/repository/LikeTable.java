package repository;

import model.Like;
import model.Post;
import model.User;
import repository.annotation.Query;

import java.util.List;
import java.util.stream.Collectors;

public class LikeTable extends Table<Like> {

    public LikeTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    @Query
    public List<Like> getByPost(Post post) {
        return all().values().stream().filter(e -> e.getPost().equals(post)).collect(Collectors.toList());
    }

    @Query
    public Like getByPostAndUser(Post post, User user) {
        return all().values().stream()
                .filter(e -> e.getPost().equals(post) && e.getUser().equals(user))
                .findFirst()
                .orElse(null);
    }

    @Override
    protected Like deserialize(String line) {
        String[] fields = line.split("\\|");
        Like like = new Like(
                (User) fromTable("users").get(fields[2]),
                (Post) fromTable("posts").get(fields[3]));
        like.setId(Integer.parseInt(fields[0]));
        like.setDeleted(Boolean.parseBoolean(fields[1]));

        return like;
    }
}
