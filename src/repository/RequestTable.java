package repository;

import model.Request;
import model.User;
import model.enums.Status;
import repository.annotation.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class RequestTable extends Table<Request>{

    public RequestTable(String tableName, String storagePath) {
        super(tableName, storagePath);
    }

    // queries go here
    @Query
    public List<Request> getBySender(User sender) {
        return all().values().stream().filter(e -> e.getSender().equals(sender)).collect(Collectors.toList());
    }

    @Query
    public List<Request> getByReceiver(User receiver) {
        return all().values().stream().filter(e -> e.getReceiver().equals(receiver)).collect(Collectors.toList());
    }

    @Query
    public Request getPendingBySenderAndReceiver(User sender, User receiver) {
        return all().values().stream()
                .filter(e -> e.getSender().equals(sender) && e.getReceiver().equals(receiver) && e.getStatus().equals(Status.PENDING))
                .findFirst().orElse(null);
    }

    @Query
    public boolean hasPendingRequest(User sender, User receiver) {
        return sender.getSentRequests().stream()
                .filter(e -> e.getReceiver().equals(receiver) && e.getStatus().equals(Status.PENDING))
                .findFirst()
                .orElse(null) != null;
    }

    @Override
    protected Request deserialize(String line) {
        String[] fields = line.split("\\|");
        Request request = new Request(
                (User) fromTable("users").get(fields[2]),
                (User) fromTable("users").get(fields[3]),
                Status.valueOf(fields[4]),
                LocalDateTime.parse(fields[5]));
        request.setId(Integer.parseInt(fields[0]));
        request.setDeleted(Boolean.parseBoolean(fields[1]));

        return request;
    }
}
