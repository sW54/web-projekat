package main;

import model.*;
import model.enums.PostType;
import model.enums.Role;
import model.enums.Status;
import service.Auth;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class RandomDataSupplier {

    public static void generate() {
        // add superusers
        System.out.println("[data] Generating superusers...");
        Application.users.add(new User("s4ndu",
                Auth.hashPassword("test"),
                "dusan@gmail.com",
                "Dušan",
                "Lazić",
                LocalDate.of(2000, 4, 11),
                "M",
                Role.SUPERUSER,
                "test-s4ndu",
                "SW 4/2019 \uD83D\uDCC8",
                false,
                false));
        Application.users.add(new User("milsek",
                Auth.hashPassword("test"),
                "milan@gmail.com",
                "Milan",
                "Sekulić",
                LocalDate.of(2000, 7, 18),
                "M",
                Role.SUPERUSER,
                null,
                "SW 54/2019 \uD83D\uDCC8",
                false,
                false));

        // add random users
        System.out.println("[data] Generating random users...");
        String[] namesm = {"Aleksa", "Aleksandar", "Bogdan", "Bojan", "Veljko", "Goran", "David", "Dejan", "Dušan", "Zoran", "Ivan",
                "Jovan", "Lazar", "Marko", "Milan", "Nenad", "Petar", "Pavle", "Uroš", "Filip"};
        String[] namesf = {"Ana", "Bojana", "Zorana", "Ivana", "Jovana","Marija", "Marina", "Milica", "Sofija", "Tamara", "Nataša",
                "Anđela", "Katarina", "Tea", "Aleksandra", "Anja", "Andrea"};
        String[] surnames = {"Jovanović", "Petrović", "Nikolić", "Ilić", "Đorđević", "Pavlović", "Marković", "Popović", "Stojanović", "Živković", "Janković",
                "Todorović", "Stanković", "Ristić", "Kostić", "Milošević", "Cvetković", "Kovačević", "Dimitrijević", "Tomić", "Krstić", "Ivanović", "Lazić",
                "Sekulić", "Vuković", "Đukanović", "Rakić", "Vajagić", "Mihajlović", "Gajić", "Savić", "Mirković", "Ivić", "Šljapić", "Simić", "Matić",
                "Milović"};

        Random r = new Random();
        int nm = namesm.length;
        int nf = namesf.length - 1;
        int s = surnames.length - 1;

        String name, surname, sufix, username, email, email2, gender, avatar, bio, nameAscii, surnameAscii;
        int day, month, year;
        boolean locked;

        for (int i = 0; i < 50; i++) {
            gender = r.nextBoolean() ? "M" : "F";
            name = gender.equals("M") ? namesm[r.nextInt(nm)] : namesf[r.nextInt(nf)];
            surname = surnames[r.nextInt(s)];

            nameAscii = name.toLowerCase().replace("š", "s")
                                          .replace("đ", "dj")
                                          .replace("ž", "z")
                                          .replaceAll("č|ć", "c");
            surnameAscii = surname.toLowerCase().replace("š", "s")
                                                .replace("đ", "dj")
                                                .replace("ž", "z")
                                                .replaceAll("č|ć", "c");

            sufix = String.valueOf(r.nextInt(100));
            username = r.nextBoolean() ? nameAscii + sufix : surnameAscii + sufix;

            day = r.nextInt(27) + 1;
            month = r.nextInt(12) + 1;
            year = r.nextInt(2005-1990) + 1990;

            sufix = String.valueOf(r.nextInt(10));
            email2 = r.nextBoolean() ? "@gmail.com" : "@protonmail.com";
            email = r.nextBoolean() ? nameAscii + surnameAscii + sufix + email2 : surnameAscii + nameAscii + sufix + email2;

            if (r.nextInt(100) < 95)
                avatar = gender.equals("M") ? r.nextInt(70) + "M" : r.nextInt(70) + "F";
            else
                avatar = null;

            bio = "";
            switch (r.nextInt(4)) {
                case 0:
                    bio = String.valueOf(year);
                    break;
                case 1:
                    bio = String.valueOf(2022 - year);
                    break;
                case 2:
                    bio = String.valueOf(2021 - year);
                    break;
                case 3:
                    bio = email;
                    break;
            }

            locked = r.nextBoolean();
            Application.users.add(new User(username,
                    Auth.hashPassword("test"),
                    email,
                    name,
                    surname,
                    LocalDate.of(year, month, day),
                    gender,
                    Role.USER,
                    avatar,
                    bio,
                    locked,
                    false));
        }
        Application.users.updateExtra(Application.requests, Application.friendships, Application.posts, Application.conversations);

        // add requests
        System.out.println("[data] Generating friend requests...");
        for (int i = 0; i < 1000; i++) {
            User first = Application.users.get(r.nextInt(Application.users.all().size()));
            User second = Application.users.get(r.nextInt(Application.users.all().size()));
            if (Application.requests.getPendingBySenderAndReceiver(first, second) != null
                    || Application.requests.getPendingBySenderAndReceiver(second, first) != null
                    || first.equals(second))
                continue;

            Request request = new Request(
                    first,
                    second,
                    r.nextBoolean() ? Status.PENDING : Status.DECLINED,
                    LocalDateTime.now().minusMinutes(r.nextInt(144000)));
            Application.requests.add(request);
        }

        // confirm some requests (add friends)
        System.out.println("[data] Generating friendships...");
        for (Request request : Application.requests.all().values()) {
            if (request.getStatus().equals(Status.PENDING)) {
                boolean cont = r.nextInt(10) == 9;
                if (cont)
                    continue;
                request.setStatus(Status.ACCEPTED);
                Application.friendships.add(new Friendship(request.getSender(), request.getReceiver()));
            }
        }

        String[] content = {
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                "Vestibulum id risus vitae magna vulputate rutrum.",
                "Nullam quis est eget magna varius tristique at a lectus.",
                "Morbi a mi convallis, semper ante a, fringilla mi.",
                "Aenean varius libero mollis dictum molestie.",
                "Praesent euismod mauris ut sapien accumsan commodo.",
                "Phasellus vulputate nunc et gravida porttitor.",
                "Curabitur fringilla felis sit amet metus rutrum gravida.",
                "Vivamus fringilla neque tristique nisl hendrerit, sed maximus dui lobortis.",
                "Ut iaculis leo a nunc mattis, id gravida mauris viverra.",
                "Nullam rhoncus justo quis bibendum facilisis.",
                "Quisque quis nibh mattis, iaculis libero et, commodo lorem.",
                "Aenean sit amet massa a dolor sagittis pretium.",
                "Duis consequat justo vitae interdum cursus.",
                "Suspendisse nec eros commodo eros semper luctus.",
                "Vivamus maximus odio at purus congue condimentum.",
                "Integer a orci eget quam ornare tristique.",
                "Nullam semper justo lacinia, faucibus est imperdiet, euismod arcu.",
                "Mauris suscipit massa eu tincidunt pharetra." };

        // posts
        System.out.println("[data] Generating posts...");
        PostType type;
        for (int i = 0; i < 600; i++) {
            type = r.nextInt(10) > 3 ? PostType.TEXT : PostType.PHOTO;
            if (type.equals(PostType.TEXT))
                Application.posts.add(new Post(
                        Application.users.get(r.nextInt(Application.users.all().size())),
                        r.nextBoolean() ? "test-" + r.nextInt(64) : null,
                        content[r.nextInt(content.length)],
                        LocalDateTime.now().minusMinutes(r.nextInt(144000)),
                        type));
            else
                Application.posts.add(new Post(
                        Application.users.get(r.nextInt(Application.users.all().size())),
                        "test-" + r.nextInt(64),
                        r.nextBoolean() ? content[r.nextInt(content.length)] : null,
                        LocalDateTime.now().minusMinutes(r.nextInt(144000)),
                        type));
        }

        for (User user : Application.users.all().values()) {
            if (user.getAvatar() != null) {
                Application.posts.add(new Post(
                        user,
                        user.getAvatar(),
                        r.nextBoolean() ? content[r.nextInt(content.length)] : null,
                        LocalDateTime.now().minusMinutes(r.nextInt(144000)),
                        PostType.PHOTO));
            }
        }

        Application.users.updateExtra(Application.requests, Application.friendships, Application.posts, Application.conversations);
        // comments
        System.out.println("[data] Generating comments...");
        LocalDateTime time;
        for (int i = 0; i < 400;) {
            User author = Application.users.get(r.nextInt(Application.users.all().size()));
            Post post = Application.posts.all().get(r.nextInt(Application.posts.all().size()));
            if (!Application.friendships.areFriends(author, post.getAuthor()))
                continue;

            time = post.getTime().plusMinutes(r.nextInt(4320));
            Application.comments.add(new Comment(
                    author,
                    post,
                    content[r.nextInt(content.length)],
                    time,
                    r.nextInt(10) == 9 ? null : time.plusMinutes(r.nextInt(10))));
            i++;
        }

        // likes
        System.out.println("[data] Generating likes...");
        for (Post post : Application.posts.all().values()) {
            List<User> authorsFriends = post.getAuthor().getFriends();
            for (User user : authorsFriends) {
                if (r.nextInt(10) > 2) {
                    Application.likes.add(new Like(
                            user,
                            post
                    ));
                } else {
                    continue;
                }
            }
        }

        User testUser1 = Application.users.getByUsername("s4ndu");
        User testUser2 = Application.users.getByUsername("milsek");

        for (User friend : testUser1.getFriends()) {
            if (Application.conversations.getByUserPair(testUser1, friend) == null)
                Application.conversations.add(new Conversation(testUser1, friend, LocalDateTime.now(), false));
        }

        for (User friend : testUser2.getFriends()) {
            if (Application.conversations.getByUserPair(testUser2, friend) == null)
                Application.conversations.add(new Conversation(testUser2, friend, LocalDateTime.now(), false));
        }

        for (int i = 0; i < 10; i++) {
            Application.posts.add(new Post(
                    testUser1,
                    "test-" + r.nextInt(64),
                    r.nextBoolean() ? content[r.nextInt(content.length)] : null,
                    LocalDateTime.now().minusMinutes(r.nextInt(144000)),
                    PostType.PHOTO));
        }

        Application.users.updateExtra(Application.requests, Application.friendships, Application.posts, Application.conversations);
        Application.posts.updateExtra(Application.comments, Application.likes);
        Application.conversations.updateExtra(Application.messages);
    }
}
