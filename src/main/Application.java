package main;

import chat.MessageHandler;
import com.google.gson.JsonParser;
import controller.*;
import repository.*;

import java.io.File;

import static spark.Spark.*;

public class Application {
    public static final String STATIC_PATH = "./static/";
    public static final String STORAGE_PATH = "./storage/";
    public static final String UPLOADS_PATH = "uploads/";

    public static UserTable users;
    public static RequestTable requests;
    public static FriendshipTable friendships;
    public static PostTable posts;
    public static CommentTable comments;
    public static ConversationTable conversations;
    public static MessageTable messages;
    public static LikeTable likes;

    public static final JsonParser jsonParser = new JsonParser();

    public static void main(String[] args) throws Exception {
        port(8080);
        staticFiles.externalLocation(new File("./static").getCanonicalPath());
        webSocket("/chat", MessageHandler.class);

        createDatabase();
        RandomDataSupplier.generate();
        // loadData();
        saveData();
        createRoutes();
    }

    private static void createRoutes() {
        // auth
        post("/auth/login", AuthController.login);
        get("/auth/check", AuthController.check);

        // account
        get("/account", UserController.getAccountInfo);
        put("/account", UserController.updateAccountInfo);
        post("/account/password", UserController.updatePassword);

        // users
        get("/users/:username", UserController.getUserInfo);
        get("/users/:username/mutual-friends", UserController.getMutualFriends);
        get("/users/:username/friend-list", UserController.getFriendList);
        get("/users", UserController.searchUsers);

        //friendship
        get("/users/:username/friend", FriendshipController.getFriendshipStatus);
        post("/users/:username/friend/:option", FriendshipController.updateFriendshipStatus);
        get("/account/requests", FriendshipController.getRequests);
        get("/account/friends", FriendshipController.getFriends);

        //posts
        get("/feed", PostController.getPostsForFeed);
        get("/users/:username/posts", PostController.getPostsByUser);
        get("/users/:username/posts/:type", PostController.getPostsByUserAndType);
        get("/posts/:id", PostController.getPost);
        post("/posts", PostController.addPost);
        delete("/posts/:id", PostController.deletePost);

        //comments
        post("/comments", CommentController.postComment);
        put("/comments", CommentController.editComment);
        delete("/comments/:id", CommentController.deleteComment);

        //likes
        post("/posts/:id/like", LikeController.toggleLike);

        // registration
        post("/users/register", UserController.register);

        // messages
        get("/account/conversations", MessageController.getConversationHeads);
        get("/account/conversations/:username", MessageController.getConversationMessages);
        post("/account/conversations/:username/read", MessageController.markConversationAsRead);

        // admin
        post("/users/:username/ban", SuperuserController.toggleBanned);
        post("/users/:username/delete-reason", SuperuserController.sendDeleteReason);
    }

    private static void createDatabase() {
        // initialize tables
        users = new UserTable("users", STORAGE_PATH);
        requests = new RequestTable("requests", STORAGE_PATH);
        friendships = new FriendshipTable("friendships", STORAGE_PATH);
        posts = new PostTable("posts", STORAGE_PATH);
        comments = new CommentTable("comments", STORAGE_PATH);
        conversations = new ConversationTable("conversations", STORAGE_PATH);
        messages = new MessageTable("messages", STORAGE_PATH);
        likes = new LikeTable("likes", STORAGE_PATH);

        // connect foreign tables
        requests.addForeign(users);
        friendships.addForeign(users);
        posts.addForeign(users);
        comments.addForeign(users, posts);
        conversations.addForeign(users);
        messages.addForeign(users, conversations);
        likes.addForeign(users, posts);
    }

    private static void loadData() {
        // load data
        users.load();
        requests.load();
        friendships.load();
        posts.load();
        comments.load();
        conversations.load();
        messages.load();
        likes.load();

        // load extra
        conversations.updateExtra(messages);
        users.updateExtra(requests, friendships, posts, conversations);
        posts.updateExtra(comments, likes);
    }

    public static void saveData() {
        users.saveChanges();
        requests.saveChanges();
        friendships.saveChanges();
        posts.saveChanges();
        comments.saveChanges();
        conversations.saveChanges();
        messages.saveChanges();
        likes.saveChanges();
    }
}
