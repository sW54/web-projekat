package model;

import model.annotation.Extra;
import model.annotation.Field;
import model.enums.PostType;

import java.time.LocalDateTime;
import java.util.List;

import static main.Application.UPLOADS_PATH;

public class Post extends Entity {

    @Field
    private User author;

    @Field
    private String imageUUID;

    @Field
    private String content;

    @Field
    private LocalDateTime time;

    @Field
    private PostType type;

    @Extra
    private List<Like> likes;

    @Extra
    private List<Comment> comments;

    public Post(User author, String imageUUID, String content, LocalDateTime time, PostType type) {
        this.author = author;
        this.imageUUID = imageUUID;
        this.content = content;
        this.time = time;
        this.type = type;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getImageUUID() {
        return imageUUID;
    }

    public void setImageUUID(String imageUUID) {
        this.imageUUID = imageUUID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public PostType getType() {
        return type;
    }

    public void setType(PostType type) {
        this.type = type;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public String getImageURI() {
        return imageUUID == null ? null : "/" + UPLOADS_PATH + imageUUID;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s|%s|%s|%s",
                id,
                deleted,
                author.getId(),
                imageUUID == null ? "" : imageUUID,
                content == null ? "" : urlEncode(content),
                time,
                type);
    }
}
