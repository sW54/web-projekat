package model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public abstract class Entity {
    protected int id;

    protected boolean deleted;

    public Entity() {
        this.id = -1;
        this.deleted = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isStored() {
        return this.id > -1;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public abstract String serialize();

    public static String urlEncode(String raw) {
        try {
            return URLEncoder.encode(raw, "UTF-8");
        } catch (UnsupportedEncodingException ignore) {
            return "";
        }
    }
}
