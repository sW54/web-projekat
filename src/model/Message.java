package model;

import model.annotation.Field;

import java.time.LocalDateTime;

public class Message extends Entity {

    @Field
    private Conversation conversation;

    @Field
    private User sender;

    @Field
    private String content;

    @Field
    private LocalDateTime time;

    public Message(Conversation conversation, User sender, String content, LocalDateTime time) {
        this.conversation = conversation;
        this.sender = sender;
        this.content = content;
        this.time = time;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s|%s|%s",
                id,
                deleted,
                conversation.getId(),
                sender.getId(),
                urlEncode(content),
                time);
    }
}
