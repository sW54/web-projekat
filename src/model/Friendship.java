package model;

import model.annotation.Field;

public class Friendship extends Entity {

    @Field
    private User first;

    @Field
    private User second;

    public Friendship(User first, User second) {
        if (first.getId() < second.getId()) {
            this.first = first;
            this.second = second;
        } else {
            this.first = second;
            this.second = first;
        }
    }

    public User getFirst() {
        return first;
    }

    public void setFirst(User first) {
        this.first = first;
    }

    public User getSecond() {
        return second;
    }

    public void setSecond(User second) {
        this.second = second;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s",
                id,
                deleted,
                first.getId(),
                second.getId());
    }
}