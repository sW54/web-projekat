package model;

import model.annotation.Field;
import model.enums.Status;

import java.time.LocalDateTime;

public class Request extends Entity {

    @Field
    private User sender;

    @Field
    private User receiver;

    @Field
    private Status status;

    @Field
    private LocalDateTime date;

    public Request(User sender, User receiver, Status status, LocalDateTime date) {
        this.sender = sender;
        this.receiver = receiver;
        this.status = status;
        this.date = date;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s|%s|%s",
                id,
                deleted,
                sender.getId(),
                receiver.getId(),
                status,
                date);
    }
}
