package model;

import model.annotation.Extra;
import model.annotation.Field;
import model.enums.Role;

import java.time.LocalDate;
import java.util.List;

import static main.Application.UPLOADS_PATH;

public class User extends Entity {

    @Field
    private String username;

    @Field
    private String passwordHash;

    @Field
    private String email;

    @Field
    private String firstName;

    @Field
    private String lastName;

    @Field
    private LocalDate birthdate;

    @Field
    private String gender;

    @Field
    private Role role;

    @Field
    private String avatar;

    @Field
    private String bio;

    @Field
    private boolean locked;

    @Field
    private boolean banned;

    @Extra
    private List<Request> sentRequests;

    @Extra
    private List<Request> receivedRequests;

    @Extra
    private List<User> friends;

    @Extra
    private List<Post> posts;

    @Extra
    private List<Conversation> conversations;

    public User(String username, String passwordHash, String email, String firstName, String lastName, LocalDate birthdate, String gender, Role role, String avatar, String bio, boolean locked, boolean banned) {
        this.username = username;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.gender = gender;
        this.role = role;
        this.avatar = avatar;
        this.bio = bio;
        this.locked = locked;
        this.banned = banned;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public List<Request> getSentRequests() {
        return sentRequests;
    }

    public void setSentRequests(List<Request> sentRequests) {
        this.sentRequests = sentRequests;
    }

    public List<Request> getReceivedRequests() {
        return receivedRequests;
    }

    public void setReceivedRequests(List<Request> receivedRequests) {
        this.receivedRequests = receivedRequests;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }

    public String getAvatarImageURI() {
        return avatar == null ? null : "/" + UPLOADS_PATH + avatar;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s",
                id,
                deleted,
                username,
                passwordHash,
                email,
                firstName,
                lastName,
                birthdate,
                gender,
                role,
                avatar,
                urlEncode(bio),
                locked,
                banned);
    }
}
