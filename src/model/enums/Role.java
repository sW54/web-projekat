package model.enums;

public enum Role {
    USER,
    SUPERUSER
}
