package model;

import model.annotation.Field;

public class Like extends Entity {

    @Field
    private User user;

    @Field
    private Post post;

    public Like(User user, Post post) {
        this.user = user;
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s",
                id,
                deleted,
                user.getId(),
                post.getId());
    }
}
