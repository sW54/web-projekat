package model;

import model.annotation.Field;

import java.time.LocalDateTime;

public class Comment extends Entity {

    @Field
    private User author;

    @Field
    private Post post;

    @Field
    private String content;

    @Field
    private LocalDateTime postedAt;

    @Field
    private LocalDateTime editedAt;

    public Comment(User author, Post post, String content, LocalDateTime postedAt, LocalDateTime editedAt) {
        this.author = author;
        this.post = post;
        this.content = content;
        this.postedAt = postedAt;
        this.editedAt = editedAt;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(LocalDateTime postedAt) {
        this.postedAt = postedAt;
    }

    public LocalDateTime getEditedAt() {
        return editedAt;
    }

    public void setEditedAt(LocalDateTime editedAt) {
        this.editedAt = editedAt;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s|%s|%s|%s",
                id,
                deleted,
                author.getId(),
                post.getId(),
                urlEncode(content),
                postedAt,
                editedAt);
    }
}
