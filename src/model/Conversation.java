package model;

import model.annotation.Extra;
import model.annotation.Field;

import java.time.LocalDateTime;
import java.util.List;

public class Conversation extends Entity {

    @Field
    private User firstParticipant;

    @Field
    private User secondParticipant;

    @Field
    private LocalDateTime time;

    @Field
    private boolean unread;

    @Extra
    private List<Message> messages;

    public Conversation(User firstParticipant, User secondParticipant, LocalDateTime time, boolean unread) {
        this.firstParticipant = firstParticipant;
        this.secondParticipant = secondParticipant;
        this.time = time;
        this.unread = unread;
    }

    public User getFirstParticipant() {
        return firstParticipant;
    }

    public void setFirstParticipant(User firstParticipant) {
        this.firstParticipant = firstParticipant;
    }

    public User getSecondParticipant() {
        return secondParticipant;
    }

    public void setSecondParticipant(User secondParticipant) {
        this.secondParticipant = secondParticipant;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    @Override
    public String serialize() {
        return String.format("%s|%s|%s|%s|%s|%s",
                id,
                deleted,
                firstParticipant.getId(),
                secondParticipant.getId(),
                time.toString(),
                unread);
    }
}
