package service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import model.Post;
import model.User;
import model.enums.PostType;
import model.enums.Role;
import spark.QueryParamsMap;
import spark.Request;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static main.Application.posts;
import static main.Application.users;
import static service.Auth.hashPassword;
import static service.Auth.validatePassword;
import static service.FriendshipService.areFriends;
import static service.Util.error;
import static service.Util.success;
import static spark.Spark.halt;

public class UserService {
    public static JsonObject returnAccountInfo(User currentUser) {
        JsonObject accountInfo = new JsonObject();
        accountInfo.addProperty("username", currentUser.getUsername());
        accountInfo.addProperty("email", currentUser.getEmail());
        accountInfo.addProperty("firstName", currentUser.getFirstName());
        accountInfo.addProperty("lastName", currentUser.getLastName());
        accountInfo.addProperty("birthDate", currentUser.getBirthdate().toString());
        accountInfo.addProperty("gender", currentUser.getGender());
        accountInfo.addProperty("avatar", currentUser.getAvatarImageURI());
        accountInfo.addProperty("bio", currentUser.getBio());
        accountInfo.addProperty("locked", currentUser.isLocked());
        return accountInfo;
    }

    public static JsonObject returnUserInfo(User selectedUser) {
        JsonObject userInfo = new JsonObject();
        userInfo.addProperty("id", selectedUser.getId());
        userInfo.addProperty("username", selectedUser.getUsername());
        userInfo.addProperty("firstName", selectedUser.getFirstName());
        userInfo.addProperty("lastName", selectedUser.getLastName());
        userInfo.addProperty("avatar", selectedUser.getAvatarImageURI());
        userInfo.addProperty("bio", selectedUser.getBio());
        userInfo.addProperty("locked", selectedUser.isLocked());
        userInfo.addProperty("banned", selectedUser.isBanned());
        userInfo.addProperty("role", selectedUser.getRole().toString());
        userInfo.addProperty("postCount", selectedUser.getPosts().size());
        userInfo.addProperty("friendCount", selectedUser.getFriends().size());
        return userInfo;
    }

    public static JsonArray returnMutualFriends(User currentUser, User selectedUser) {
        JsonArray mutualFriendsArray = new JsonArray();

        List<User> selectedUserFriends = selectedUser.getFriends();
        List<User> mutualFriends = currentUser.getFriends().stream()
                                              .filter(selectedUserFriends::contains)
                                              .collect(Collectors.toList());

        for (User u : mutualFriends) {
            JsonObject searchResult = new JsonObject();
            searchResult.addProperty("id", u.getId());
            searchResult.addProperty("username", u.getUsername());
            searchResult.addProperty("firstName", u.getFirstName());
            searchResult.addProperty("lastName", u.getLastName());
            searchResult.addProperty("avatar", u.getAvatarImageURI());
            mutualFriendsArray.add(searchResult);
        }
        return mutualFriendsArray;
    }

    public static JsonArray returnFriendList(User currentUser, User selectedUser) {
        JsonArray friendsArray = new JsonArray();

        List<User> selectedUserFriends = selectedUser.getFriends();

        if (currentUser != null)
            selectedUserFriends.sort(Comparator.comparing(e -> !areFriends(currentUser, e)));

        for (User u : selectedUserFriends) {
            JsonObject friendInfo = new JsonObject();
            friendInfo.addProperty("id", u.getId());
            friendInfo.addProperty("username", u.getUsername());
            friendInfo.addProperty("firstName", u.getFirstName());
            friendInfo.addProperty("lastName", u.getLastName());
            friendInfo.addProperty("avatar", u.getAvatarImageURI());
            if (currentUser != null) friendInfo.addProperty("isFriend", areFriends(currentUser, u));
            friendsArray.add(friendInfo);
        }
        return friendsArray;
    }

    public static JsonArray returnSearchResults(Request request, User currentUser) {
        JsonArray searchResultsArray = new JsonArray();

        QueryParamsMap queryParams = request.queryMap();

        String firstName = queryParams.get("firstName").value();
        String lastName = queryParams.get("lastName").value();
        String birthdateMin = queryParams.get("birthdateMin").value();
        String birthdateMax = queryParams.get("birthdateMax").value();
        String sort = queryParams.get("sort").value();
        String order = queryParams.get("order").value();
        String email = null;

        if (currentUser != null && currentUser.getRole().equals(Role.SUPERUSER)) {
            email = queryParams.get("email").value();
        }
        String finalEmail = email;

        List<User> foundUsers = users.all().values().stream()
                .filter(e -> firstName == null || e.getFirstName().toLowerCase().contains(firstName.toLowerCase()))
                .filter(e -> lastName == null || e.getLastName().toLowerCase().contains(lastName.toLowerCase()))
                .filter(e -> birthdateMin == null || e.getBirthdate().isAfter(LocalDate.parse(birthdateMin)))
                .filter(e -> birthdateMax == null || e.getBirthdate().isBefore(LocalDate.parse(birthdateMax)))
                .filter(e -> finalEmail == null || e.getEmail().contains(finalEmail))
                .collect(Collectors.toList());

        // prevent empty search
        if (currentUser == null || !currentUser.getRole().equals(Role.SUPERUSER)) {
            foundUsers = foundUsers.subList(0, Math.min(21, foundUsers.size()));
        }

        if (sort != null) {
            if (sort.equals("firstName"))
                foundUsers.sort(Comparator.comparing(User::getFirstName));
            else if (sort.equals("lastName"))
                foundUsers.sort(Comparator.comparing(User::getLastName));
            else if (sort.equals("birthdate"))
                foundUsers.sort(Comparator.comparing(User::getBirthdate));

            if (order != null && order.equals("desc"))
                Collections.reverse(foundUsers);
        }

        for (User u : foundUsers) {
            JsonObject searchResult = new JsonObject();
            searchResult.addProperty("id", u.getId());
            searchResult.addProperty("username", u.getUsername());
            searchResult.addProperty("firstName", u.getFirstName());
            searchResult.addProperty("lastName", u.getLastName());
            searchResult.addProperty("avatar", u.getAvatarImageURI());
            searchResultsArray.add(searchResult);
        }
        return searchResultsArray;
    }

    public static User registerUser(JsonObject registrationData) {
        String username = registrationData.get("username").getAsString();
        String email = registrationData.get("email").getAsString();
        String firstName = registrationData.get("firstName").getAsString();
        String lastName = registrationData.get("lastName").getAsString();
        String gender = registrationData.get("gender").getAsString();
        String password = registrationData.get("password").getAsString();

        User newUser = new User(
                username,
                hashPassword(password),
                email,
                firstName,
                lastName,
                LocalDate.MIN,
                gender,
                Role.USER,
                null,
                "",
                false,
                false);

        newUser.setPosts(new ArrayList<>());
        newUser.setFriends(new ArrayList<>());
        newUser.setSentRequests(new ArrayList<>());
        newUser.setReceivedRequests(new ArrayList<>());
        newUser.setConversations(new ArrayList<>());

        users.add(newUser);
        users.saveChanges();
        return newUser;
    }

    public static JsonObject updateAccountInfo(User currentUser, JsonObject accountInfoData) {
        String email = accountInfoData.get("email").getAsString();
        String firstName = accountInfoData.get("firstName").getAsString();
        String lastName = accountInfoData.get("lastName").getAsString();
        String gender = accountInfoData.get("gender").getAsString();
        LocalDate birthDate = LocalDate.parse(accountInfoData.get("birthDate").getAsString());
        String bio = accountInfoData.get("bio").getAsString();
        boolean locked = accountInfoData.get("locked").getAsBoolean();

        currentUser.setEmail(email);
        currentUser.setFirstName(firstName);
        currentUser.setLastName(lastName);
        currentUser.setGender(gender);
        currentUser.setBirthdate(birthDate);
        currentUser.setBio(bio);
        currentUser.setLocked(locked);

        if (accountInfoData.get("avatar").getAsInt() == -2) {
            currentUser.setAvatar(null);
        } else if (accountInfoData.get("avatar").getAsInt() != -1) {
            String avatar = posts.get(accountInfoData.get("avatar").getAsInt()).getImageUUID();
            currentUser.setAvatar(avatar);
        }

        users.saveChanges();
        return success(200, "Account info updated successfully.");
    }

    public static JsonObject updateAccountPassword(User currentUser, JsonObject passwordData) {
        String newPassword = passwordData.get("newPassword").getAsString();
        currentUser.setPasswordHash(hashPassword(newPassword));
        users.saveChanges();
        return success(200, "Account password updated successfully.");
    }

    public static JsonObject toggleBanned(User selectedUser) {
        if (selectedUser.getRole().equals(Role.SUPERUSER))
            return error(400, "You can not ban a superuser.");

        selectedUser.setBanned(!selectedUser.isBanned());
        users.saveChanges();

        JsonObject responseData = new JsonObject();
        responseData.addProperty("isBanned", selectedUser.isBanned());
        return success(200, "Ban status successfully toggled.", responseData);
    }

    public static User getUserFromParams(Request req, boolean required) {
        String username = req.params("username");
        User user = users.getByUsername(username);
        if (user == null && required)
            halt(404, error(404, "User not found").toString());
        return user;
    }

    public static boolean validateRegistrationRequest(JsonObject registrationData) {
        try {
            registrationData.get("username").getAsString();
            registrationData.get("email").getAsString();
            registrationData.get("firstName").getAsString();
            registrationData.get("lastName").getAsString();
            registrationData.get("gender").getAsString();
            registrationData.get("password").getAsString();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static JsonObject validateRegistrationData(JsonObject registrationData) {
        String username = registrationData.get("username").getAsString();
        if (!username.matches("^[a-zA-Z0-9_.-]{3,16}$"))
            return error(400, "Username is invalid.");
        if (users.getByUsername(username) != null)
            return error(409, "Username is taken.");

        String email = registrationData.get("email").getAsString();
        if (!email.matches("^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6})*$"))
            return error(400, "Email is invalid.");
        if (users.getByEmail(email) != null)
            return error(409, "Email is already being used.");

        String firstName = registrationData.get("firstName").getAsString();
        if (firstName.length() == 0)
            return error(400, "Missing first name.");
        if (firstName.length() > 30)
            return error(400, "Name too long.");
        if (firstName.contains("|") || firstName.contains("\n"))
            return error(400, "Was that an attempt to corrupt my csv files?");

        String lastName = registrationData.get("lastName").getAsString();
        if (lastName.length() == 0)
            return error(400, "Missing last name.");
        if (lastName.length() > 30)
            return error(400, "Last name too long.");
        if (lastName.contains("|") || lastName.contains("\n"))
            return error(400, "Woah we have a 1337 hacker here...");

        String gender = registrationData.get("gender").getAsString();
        if (gender.length() == 0)
            return error(400, "Missing gender.");
        if (gender.length() > 30)
            return error(400, "Gender too long.");
        if (gender.contains("|") || gender.contains("\n"))
            return error(400, "I mean, really?");

        String password = registrationData.get("password").getAsString();
        if (password.length() < 6)
            return error(400, "Password too short.");

        return null;
    }

    public static boolean validateAccountInfoRequest(JsonObject accountInfoData) {
        try {
            accountInfoData.get("email").getAsString();
            accountInfoData.get("firstName").getAsString();
            accountInfoData.get("lastName").getAsString();
            accountInfoData.get("gender").getAsString();
            accountInfoData.get("birthDate").getAsString();
            accountInfoData.get("bio").getAsString();
            accountInfoData.get("locked").getAsBoolean();
            accountInfoData.get("avatar").getAsInt();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static JsonObject validateAccountInfoData(User currentUser, JsonObject accountInfoData) {
        String email = accountInfoData.get("email").getAsString();
        if (!email.matches("^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6})*$"))
            return error(400, "Email is invalid.");
        if (users.getByEmail(email) != null && !currentUser.getEmail().equals(email))
            return error(409, "Email is already being used.");

        String firstName = accountInfoData.get("firstName").getAsString();
        if (firstName.length() == 0)
            return error(400, "Missing first name.");
        if (firstName.length() > 30)
            return error(400, "Name too long.");
        if (firstName.contains("|") || firstName.contains("\n"))
            return error(400, "Was that an attempt to corrupt my csv files?");

        String lastName = accountInfoData.get("lastName").getAsString();
        if (lastName.length() == 0)
            return error(400, "Missing last name.");
        if (lastName.length() > 30)
            return error(400, "Last name too long.");
        if (lastName.contains("|") || lastName.contains("\n"))
            return error(400, "Woah we have a 1337 hacker here...");

        String gender = accountInfoData.get("gender").getAsString();
        if (gender.length() == 0)
            return error(400, "Missing gender.");
        if (gender.length() > 30)
            return error(400, "Gender too long.");
        if (gender.contains("|") || gender.contains("\n"))
            return error(400, "Even if I let you store nasty characters, I would url encode them first.");

        String birthDate = accountInfoData.get("birthDate").getAsString();
        try {
            LocalDate.parse(birthDate);
        } catch (DateTimeParseException e) {
            return error(400, "Birthdate is invalid.");
        }

        String bio = accountInfoData.get("bio").getAsString();
        if (bio.length() > 200)
            return error(400, "Bio too long.");

        int avatarPhotoId = accountInfoData.get("avatar").getAsInt();
        Post avatarPhoto = posts.get(avatarPhotoId);

        if (avatarPhotoId != -1 && avatarPhotoId != -2) {
            if (avatarPhoto == null)
                return error(404, "Photo not found.");
            if (!avatarPhoto.getType().equals(PostType.PHOTO))
                return error(400, "Post is not a photo. How did you even do that?");
            if (!avatarPhoto.getAuthor().equals(currentUser))
                return error(400, "Photo is not uploaded by you. How did you even do that?");
        }

        return null;
    }

    public static boolean validatePasswordUpdateRequest(JsonObject passwordData) {
        try {
            passwordData.get("oldPassword").getAsString();
            passwordData.get("newPassword").getAsString();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static JsonObject validatePasswordUpdateData(User currentUser, JsonObject passwordData) {
        String oldPassword = passwordData.get("oldPassword").getAsString();
        if (!validatePassword(oldPassword, currentUser.getPasswordHash()))
            return error(400, "Wrong old password. Password unchanged.");

        String newPassword = passwordData.get("newPassword").getAsString();
        if (newPassword.length() < 6)
            return error(400, "New password too short. Password unchanged.");

        return null;
    }

}
