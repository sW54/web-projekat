package service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import model.Friendship;
import model.Request;
import model.User;
import model.enums.Status;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static main.Application.friendships;
import static main.Application.requests;
import static service.Util.error;
import static service.Util.success;

public class FriendshipService {

    public static JsonObject returnRequests(User currentUser) {
        JsonObject requestsContainer = new JsonObject();
        JsonArray sentRequestsArray = new JsonArray();
        JsonArray receivedRequestsArray = new JsonArray();

        List<Request> sentRequests = currentUser.getSentRequests().stream()
                                                .filter(e -> e.getStatus().equals(Status.PENDING))
                                                .collect(Collectors.toList());
        List<Request> receivedRequests = currentUser.getReceivedRequests().stream()
                .filter(e -> e.getStatus().equals(Status.PENDING))
                .collect(Collectors.toList());

        for (Request request : sentRequests) {
            JsonObject requestInfo = new JsonObject();
            JsonObject receiverInfo = new JsonObject();
            User receiver = request.getReceiver();

            requestInfo.addProperty("sentAt", request.getDate().toString());
            requestInfo.add("receiver", receiverInfo);
            receiverInfo.addProperty("username", receiver.getUsername());
            receiverInfo.addProperty("firstName", receiver.getFirstName());
            receiverInfo.addProperty("lastName", receiver.getLastName());
            receiverInfo.addProperty("avatar", receiver.getAvatarImageURI());

            sentRequestsArray.add(requestInfo);
        }

        for (Request request : receivedRequests) {
            JsonObject requestInfo = new JsonObject();
            JsonObject senderInfo = new JsonObject();
            User sender = request.getSender();

            requestInfo.addProperty("sentAt", request.getDate().toString());
            requestInfo.add("sender", senderInfo);
            senderInfo.addProperty("username", sender.getUsername());
            senderInfo.addProperty("firstName", sender.getFirstName());
            senderInfo.addProperty("lastName", sender.getLastName());
            senderInfo.addProperty("avatar", sender.getAvatarImageURI());

            receivedRequestsArray.add(requestInfo);
        }

        requestsContainer.add("sentRequests", sentRequestsArray);
        requestsContainer.add("receivedRequests", receivedRequestsArray);

        return requestsContainer;
    }

    public static JsonArray returnFriends(User currentUser) {
        JsonArray friendsArray = new JsonArray();

        List<User> friends = currentUser.getFriends();

        for (User friend : friends) {
            JsonObject friendInfo = new JsonObject();

            friendInfo.addProperty("username", friend.getUsername());
            friendInfo.addProperty("firstName", friend.getFirstName());
            friendInfo.addProperty("lastName", friend.getLastName());
            friendInfo.addProperty("avatar", friend.getAvatarImageURI());

            friendsArray.add(friendInfo);
        }

        return friendsArray;
    }

    public static JsonObject updateFriendshipStatus(String option, User currentUser, User selectedUser) {
        switch (option) {
            case "send":
                if (areFriends(currentUser, selectedUser))
                    return error(400, "You are already friends.");
                if (requests.hasPendingRequest(currentUser, selectedUser))
                    return error(409, "Request is already sent to this user.");
                if (requests.hasPendingRequest(selectedUser, currentUser))
                    return error(409, "You already have a request from this user.");

                return sendRequest(currentUser, selectedUser);
            case "cancel":
                if (!requests.hasPendingRequest(currentUser, selectedUser))
                    return error(404, "Request does not exist.");

                return cancelRequest(currentUser, selectedUser);
            case "accept":
                if (!requests.hasPendingRequest(selectedUser, currentUser))
                    return error(404, "Request does not exist.");

                return acceptRequest(currentUser, selectedUser);
            case "decline":
                if (!requests.hasPendingRequest(selectedUser, currentUser))
                    return error(404, "Request does not exist.");

                return declineRequest(currentUser, selectedUser);
            case "unfriend":
                if (!areFriends(currentUser, selectedUser))
                    return error(400, "You are already not friends.");

                return unfriendUser(currentUser, selectedUser);
            default:
                return error(400, "No such option.");
        }


    }

    public static JsonObject returnFriendshipStatus(User currentUser, User selectedUser) {
        JsonObject friendshipStatus = new JsonObject();
        JsonArray friendshipOptions = new JsonArray();

        boolean areFriends = false;
        boolean hasPendingRequest = false;

        if (friendships.areFriends(currentUser, selectedUser)) {
            areFriends = true;
            friendshipOptions.add(new JsonPrimitive("unfriend"));
        } else if (requests.hasPendingRequest(currentUser, selectedUser)) {
            hasPendingRequest = true;
            friendshipOptions.add(new JsonPrimitive("cancel"));
        } else if (requests.hasPendingRequest(selectedUser, currentUser)) {
            hasPendingRequest = true;
            friendshipOptions.add(new JsonPrimitive("accept"));
            friendshipOptions.add(new JsonPrimitive("decline"));
        } else {
            friendshipOptions.add(new JsonPrimitive("send"));
        }

        friendshipStatus.addProperty("friends", areFriends);
        friendshipStatus.addProperty("pending", hasPendingRequest);
        friendshipStatus.add("options", friendshipOptions);
        return friendshipStatus;
    }

    private static JsonObject sendRequest(User currentUser, User selectedUser) {
        Request request = new Request(currentUser, selectedUser, Status.PENDING, LocalDateTime.now());
        currentUser.getSentRequests().add(request);
        selectedUser.getReceivedRequests().add(request);
        requests.add(request);
        requests.saveChanges();
        return success(200, "Request sent successfully.");
    }

    private static JsonObject cancelRequest(User currentUser, User selectedUser) {
        Request request = requests.getPendingBySenderAndReceiver(currentUser, selectedUser);
        requests.remove(request);

        if (request.isDeleted()) {
            requests.saveChanges();
            currentUser.getSentRequests().remove(request);
            selectedUser.getReceivedRequests().remove(request);
            return success(200, "Request canceled successfully.");
        }
        return error(500, "Server error.");
    }

    private static JsonObject acceptRequest(User currentUser, User selectedUser) {
        Request request = requests.getPendingBySenderAndReceiver(selectedUser, currentUser);
        request.setStatus(Status.ACCEPTED);
        Friendship friendship = new Friendship(currentUser, selectedUser);

        friendships.add(friendship);
        friendships.saveChanges();
        requests.saveChanges();

        currentUser.getFriends().add(selectedUser);
        selectedUser.getFriends().add(currentUser);

        return success(200, "Request accepted successfully.");
    }

    private static JsonObject declineRequest(User currentUser, User selectedUser) {
        Request request = requests.getPendingBySenderAndReceiver(selectedUser, currentUser);
        request.setStatus(Status.DECLINED);
        requests.saveChanges();

        return success(200, "Request declined successfully.");
    }

    private static JsonObject unfriendUser(User currentUser, User selectedUser) {
        Friendship friendship = friendships.getByUsers(currentUser, selectedUser);

        friendships.remove(friendship);
        friendships.saveChanges();

        currentUser.getFriends().remove(selectedUser);
        selectedUser.getFriends().remove(currentUser);

        return success(200, "Friend removed successfully.");
    }

    public static boolean areFriends(User currentUser, User selectedUser) {
        return friendships.areFriends(currentUser, selectedUser);
    }

}
