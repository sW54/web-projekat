package service;

import com.google.gson.JsonObject;

public class Util {
    public static JsonObject error(int status, String message) {
        JsonObject errorContainer = new JsonObject();
        JsonObject errorInfo = new JsonObject();
        errorContainer.add("error", errorInfo);
        errorInfo.addProperty("status", status);
        errorInfo.addProperty("message", message);
        return errorContainer;
    }

    public static JsonObject success(int status, String message) {
        JsonObject successContainer = new JsonObject();
        JsonObject successInfo = new JsonObject();
        successContainer.add("success", successInfo);
        successInfo.addProperty("status", status);
        successInfo.addProperty("message", message);
        return successContainer;
    }

    public static JsonObject success(int status, String message, JsonObject data) {
        JsonObject successContainer = new JsonObject();
        JsonObject successInfo = new JsonObject();
        successContainer.add("success", successInfo);
        successInfo.addProperty("status", status);
        successInfo.addProperty("message", message);
        successInfo.add("data", data);
        return successContainer;
    }

    public static boolean isError(JsonObject jsonObject) {
        return jsonObject.get("error") != null;
    }

    public static int getErrorCode(JsonObject jsonObject) {
        return jsonObject.get("error").getAsJsonObject().get("status").getAsInt();
    }
}
