package service;

import com.google.gson.JsonObject;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import model.Post;
import model.User;
import model.enums.Role;
import spark.Request;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Date;

import static main.Application.friendships;
import static main.Application.users;
import static service.Util.error;
import static spark.Spark.halt;

public class Auth {
    public static final Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public static boolean validateRequest(JsonObject loginData) {
        try {
            loginData.get("username").getAsString();
            loginData.get("password").getAsString();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static JsonObject validateLogin(JsonObject loginData) {
        String username = loginData.get("username").getAsString();
        String password = loginData.get("password").getAsString();

        User user = users.getByUsername(username);
        if (user == null)
            return error(401, "Invalid login credentials.");
        if (user.isBanned())
            return error(403, "You are banned.");
        if (!validatePassword(password, user.getPasswordHash()))
            return error(401, "Invalid login credentials.");

        return null;
    }

    public static JsonObject issueToken(JsonObject loginData) {
        User validUser = users.getByUsername(loginData.get("username").getAsString());
        JsonObject tokenJson = new JsonObject();
        tokenJson.addProperty("token", generateToken(validUser));
        tokenJson.addProperty("userId", validUser.getId());
        tokenJson.addProperty("username", validUser.getUsername());
        tokenJson.addProperty("role", validUser.getRole().toString());
        return tokenJson;
    }

    public static JsonObject issueToken(User user) {
        JsonObject tokenJson = new JsonObject();
        tokenJson.addProperty("token", generateToken(user));
        tokenJson.addProperty("userId", user.getId());
        tokenJson.addProperty("username", user.getUsername());
        tokenJson.addProperty("role", user.getRole().toString());
        return tokenJson;
    }

    public static String generateToken(User user) {
        return Jwts.builder()
                .setSubject("user:" + user.getUsername())
                .claim("id", String.valueOf(user.getId()))
                .claim("username", user.getUsername())
                .claim("role", user.getRole())
                .setExpiration(Date.from(ZonedDateTime.now().plusDays(7).toInstant()))
                .setIssuedAt(Date.from(ZonedDateTime.now().toInstant()))
                .signWith(SECRET_KEY)
                .compact();
    }

    public static Jws<Claims> readTokenFromRequest(Request req) {
        String header = req.headers("Authorization");
        String token = header.replace("Bearer", "");
        return Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(token);
    }

    public static Jws<Claims> readTokenFromQuery(String query) {
        String token = query.split("token=")[1].split("&")[0];
        return Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(token);
    }

    public static User authenticateFromRequest(Request req, boolean required) {
        try {
            Jws<Claims> claims = readTokenFromRequest(req);
            String id = (String) claims.getBody().get("id");
            return users.get(id);
        } catch (JwtException | NullPointerException e) {
            if (required)
                halt(401, error(401, "You are not logged in.").toString());
        }
        return null;
    }

    public static User authorizeFromRequest(Request req, Role role) {
        try {
            Jws<Claims> claims = readTokenFromRequest(req);
            String id = (String) claims.getBody().get("id");
            if (Role.valueOf(claims.getBody().get("role").toString()).equals(role))
                return users.get(id);
            else
                halt(403, error(403, "You are not authorized for this operation.").toString());
        } catch (JwtException | NullPointerException ignored) {
            halt(401, error(401, "You are not logged in.").toString());
        }
        return null;
    }

    public static String hashPassword(String plaintext) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(plaintext.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder();
            for (byte b : hash)
                hexString.append(String.format("%02x", b));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static boolean validatePassword(String plaintext, String hashed) {
        return hashed.equals(hashPassword(plaintext));
    }

    public static boolean isPostVisibleToUser(Post post, User currentUser) {
        User author = post.getAuthor();
        return isProfileVisibleToUser(author, currentUser);
    }

    public static boolean isProfileVisibleToUser(User selectedUser, User currentUser) {
        return (currentUser != null && currentUser.getRole().equals(Role.SUPERUSER))
                || !selectedUser.isLocked()
                || (currentUser != null && friendships.areFriends(currentUser, selectedUser))
                || selectedUser.equals(currentUser);
    }
}
