package service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import model.Conversation;
import model.Message;
import model.User;
import model.enums.Role;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static main.Application.conversations;
import static main.Application.messages;
import static service.Util.error;
import static service.Util.success;

public class MessageService {

    public static JsonObject markConversationAsRead(User currentUser, User selectedUser) {
        Conversation conversation = conversations.getByUserPair(currentUser, selectedUser);
        conversation.setUnread(false);
        conversations.saveChanges();
        return success(200, "Conversation marked as read successfully.");
    }

    @Deprecated
    public static JsonObject sendDeleteReason(User currentUser, User selectedUser, JsonObject messageData) {
        if (messageData.get("content") == null)
            return error(400, "Bad request.");

        Conversation conversation = conversations.getByUserPair(currentUser, selectedUser);
        if (conversation == null) {
            conversation = new Conversation(currentUser, selectedUser, LocalDateTime.now(), true);
            conversation.setMessages(new ArrayList<>());
            conversations.add(conversation);
            conversations.saveChanges();
        }

        String content = messageData.get("content").getAsString();
        Message message = new Message(
                conversation,
                currentUser,
                content,
                LocalDateTime.now());

        messages.add(message);
        messages.saveChanges();

        conversation.getMessages().add(message);
        conversation.setUnread(true);
        conversation.setTime(LocalDateTime.now());

        return success(200, "Message sent successfully.");
    }

    public static JsonArray returnConversationHeads(User currentUser) {
        JsonArray conversationsArray = new JsonArray();

        List<Conversation> usersConversations = conversations.getByUser(currentUser);
        for (Conversation conversation : usersConversations) {
            JsonObject conversationObject = new JsonObject();

            User receiver = conversation.getFirstParticipant().equals(currentUser) ? conversation.getSecondParticipant() : conversation.getFirstParticipant();


            User last = currentUser;
            if (conversation.getMessages().size() > 0)
                last = conversation.getMessages().get(conversation.getMessages().size() - 1).getSender();

            conversationObject.addProperty("id", receiver.getId());
            conversationObject.addProperty("username", receiver.getUsername());
            conversationObject.addProperty("avatar", receiver.getAvatarImageURI());
            conversationObject.addProperty("time", conversation.getTime().toString());
            conversationObject.addProperty("isUnread", conversation.isUnread() && last.equals(receiver));
            conversationObject.addProperty("isAdmin", receiver.getRole().equals(Role.SUPERUSER));
            conversationsArray.add(conversationObject);
        }
        return conversationsArray;
    }

    public static JsonObject returnConversation(User currentUser, User selectedUser) {
        JsonArray messagesArray = new JsonArray();
        JsonObject conversationJson = new JsonObject();
        JsonObject firstParticipantJson = new JsonObject();
        JsonObject secondParticipantJson = new JsonObject();

        Conversation conversation = conversations.getByUserPair(currentUser, selectedUser);
        if (conversation == null)
            return error(404, "Conversation with user not found.");

        for (Message message : conversation.getMessages()) {
            JsonObject messageJson = new JsonObject();
            messageJson.addProperty("sender", message.getSender().getUsername());
            messageJson.addProperty("content", message.getContent());
            messageJson.addProperty("time", message.getTime().toString());
            messagesArray.add(messageJson);
        }

        firstParticipantJson.addProperty("id", conversation.getFirstParticipant().getId());
        firstParticipantJson.addProperty("username", conversation.getFirstParticipant().getUsername());
        firstParticipantJson.addProperty("avatar", conversation.getFirstParticipant().getAvatarImageURI());

        secondParticipantJson.addProperty("id", conversation.getSecondParticipant().getId());
        secondParticipantJson.addProperty("username", conversation.getSecondParticipant().getUsername());
        secondParticipantJson.addProperty("avatar", conversation.getSecondParticipant().getAvatarImageURI());

        conversationJson.addProperty("id", conversation.getId());
        conversationJson.add("first", firstParticipantJson);
        conversationJson.add("second", secondParticipantJson);
        conversationJson.addProperty("time", conversation.getTime().toString());
        conversationJson.add("messages", messagesArray);
        return conversationJson;
    }

}
