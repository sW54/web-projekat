package service;

import com.google.gson.JsonObject;
import model.Like;
import model.Post;
import model.User;

import static main.Application.likes;
import static service.Auth.isPostVisibleToUser;
import static service.Util.error;
import static service.Util.success;


public class LikeService {

    public static JsonObject toggleLike(Post selectedPost, User currentUser) {
        if (!isPostVisibleToUser(selectedPost, currentUser))
            return error(403, "You do not have access to this post.");

        Like like = likes.getByPostAndUser(selectedPost, currentUser);
        if (like == null) {
            like = new Like(currentUser, selectedPost);
            likes.add(like);
            likes.saveChanges();
            selectedPost.getLikes().add(like);
        } else {
            likes.remove(like);
            likes.saveChanges();
            selectedPost.getLikes().remove(like);
            like = null;
        }

        JsonObject responseData = new JsonObject();
        responseData.addProperty("isLiked", like != null);
        return success(200, "Like successfully toggled.", responseData);
    }

    public static boolean isPostLikedByUser(Post selectedPost, User currentUser) {
        return likes.getByPostAndUser(selectedPost, currentUser) != null;
    }
}
