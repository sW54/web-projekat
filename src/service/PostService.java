package service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import model.Comment;
import model.Post;
import model.User;
import model.enums.PostType;
import model.enums.Role;
import spark.Request;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static main.Application.*;
import static service.LikeService.isPostLikedByUser;
import static service.Util.error;
import static service.Util.success;
import static spark.Spark.halt;

public class PostService {

    public static JsonObject addPost(JsonObject postData, User currentUser) {
        String imageUUID = null;
        String content = null;
        PostType type = PostType.valueOf(postData.get("type").getAsString().toUpperCase());

        if (postData.has("imageData")) {
            String imageStr = postData.get("imageData").getAsString();
            String imageTypeName = imageStr.substring(imageStr.indexOf("/") + 1, imageStr.indexOf(";"));

            byte[] imageBytes = DatatypeConverter.parseBase64Binary(imageStr.substring(imageStr.indexOf(",") + 1));

            try {
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imageBytes));
                imageUUID = UUID.randomUUID().toString();
                File out = new File(STATIC_PATH + UPLOADS_PATH + imageUUID);
                out.getParentFile().mkdirs();
                out.createNewFile();
                ImageIO.write(bufferedImage, imageTypeName, out);
            } catch (IOException e) {
                e.printStackTrace();
                return error(500, "Image failed to upload.");
            }
        }

        if (postData.has("content"))
            content = postData.get("content").getAsString();

        Post post = new Post(
                currentUser,
                imageUUID,
                content,
                LocalDateTime.now(),
                type);

        posts.add(post);
        posts.saveChanges();

        post.setComments(new ArrayList<>());
        post.setLikes(new ArrayList<>());
        currentUser.getPosts().add(post);
        return success(200, "Post created successfully.");
    }

    public static JsonObject deletePost(Post selectedPost, User currentUser) {
        if (!currentUser.getRole().equals(Role.SUPERUSER) && !selectedPost.getAuthor().equals(currentUser))
            return error(403, "Post is not created by you.");

        try {
            posts.remove(selectedPost);
            posts.saveChanges();
            selectedPost.getComments().forEach(e -> comments.remove(e));
            selectedPost.getLikes().forEach(e -> likes.remove(e));
            comments.saveChanges();
            likes.saveChanges();
            selectedPost.getAuthor().getPosts().remove(selectedPost);
            return success(200, "Post deleted successfully.");
        } catch (Exception e) {
            return error(500, "Server error. Deleting failed.");
        }
    }

    public static JsonArray returnUserPosts(User selectedUser, User currentUser) {
        JsonArray postsArray = new JsonArray();

        List<Post> posts = selectedUser.getPosts().stream()
                .sorted(Comparator.comparing(Post::getTime).reversed())
                .collect(Collectors.toList());

        for (Post p : posts) {
            JsonObject postInfo = new JsonObject();
            postInfo.addProperty("id", p.getId());
            postInfo.addProperty("postedAt", p.getTime().toString());
            postInfo.addProperty("content", p.getContent());
            postInfo.addProperty("likeCount", p.getLikes().size());
            postInfo.addProperty("isLiked", isPostLikedByUser(p, currentUser));
            postInfo.addProperty("imageURI", p.getImageURI());
            postInfo.addProperty("type", p.getType().toString());
            postsArray.add(postInfo);
        }
        return postsArray;
    }

    public static JsonArray returnUserPostsWithType(User selectedUser, User currentUser, PostType type) {
        JsonArray postsArray = new JsonArray();

        List<Post> posts = selectedUser.getPosts().stream()
                .filter(e -> e.getType().equals(type))
                .sorted(Comparator.comparing(Post::getTime).reversed())
                .collect(Collectors.toList());

        for (Post p : posts) {
            JsonObject postInfo = new JsonObject();
            postInfo.addProperty("id", p.getId());
            postInfo.addProperty("postedAt", p.getTime().toString());
            postInfo.addProperty("content", p.getContent());
            postInfo.addProperty("likeCount", p.getLikes().size());
            postInfo.addProperty("isLiked", isPostLikedByUser(p, currentUser));
            postInfo.addProperty("imageURI", p.getImageURI());
            postInfo.addProperty("type", p.getType().toString());
            postsArray.add(postInfo);
        }
        return postsArray;
    }

    public static JsonObject returnPost(Post selectedPost, User currentUser) {
        JsonObject postInfo = new JsonObject();
        JsonObject authorInfo = new JsonObject();
        JsonObject commentsContainer = new JsonObject();
        JsonArray commentsArray = new JsonArray();

        User author = selectedPost.getAuthor();
        List<Comment> comments = selectedPost.getComments();

        postInfo.addProperty("id", selectedPost.getId());
        postInfo.addProperty("postedAt", selectedPost.getTime().toString());
        postInfo.addProperty("content", selectedPost.getContent());
        postInfo.addProperty("likeCount", selectedPost.getLikes().size());
        postInfo.addProperty("isLiked", isPostLikedByUser(selectedPost, currentUser));
        postInfo.addProperty("imageURI", selectedPost.getImageURI());
        postInfo.addProperty("type", selectedPost.getType().toString());

        postInfo.add("author", authorInfo);
        authorInfo.addProperty("id", author.getId());
        authorInfo.addProperty("username", author.getUsername());
        authorInfo.addProperty("firstName", author.getFirstName());
        authorInfo.addProperty("lastName", author.getLastName());
        authorInfo.addProperty("avatar", author.getAvatarImageURI());

        postInfo.add("comments", commentsContainer);
        commentsContainer.addProperty("count", comments.size());
        commentsContainer.add("list", commentsArray);
        for (Comment comment : comments) {
            JsonObject commentInfo = new JsonObject();
            JsonObject commentAuthorInfo = new JsonObject();

            User commentAuthor = comment.getAuthor();

            commentInfo.addProperty("id", comment.getId());
            commentInfo.addProperty("postedAt", comment.getPostedAt().toString());
            if (comment.getEditedAt() != null)
                commentInfo.addProperty("editedAt", comment.getEditedAt().toString());
            commentInfo.addProperty("content", comment.getContent());

            commentInfo.add("author", commentAuthorInfo);
            commentAuthorInfo.addProperty("id", commentAuthor.getId());
            commentAuthorInfo.addProperty("username", commentAuthor.getUsername());
            commentAuthorInfo.addProperty("avatar", commentAuthor.getAvatarImageURI());

            commentsArray.add(commentInfo);
        }
        return postInfo;
    }

    public static JsonArray returnPostsForFeed(int offset, int limit, User currentUser) {
        JsonArray postsArray = new JsonArray();

        List<Post> feed = posts.all().values().stream()
                .filter(e -> friendships.areFriends(e.getAuthor(), currentUser) || e.getAuthor().equals(currentUser))
                .sorted(Comparator.comparing(Post::getTime).reversed())
                .collect(Collectors.toList());

        int fromIndex = Math.min(offset, feed.size());
        int toIndex = Math.min(offset+limit, feed.size());
        feed = feed.subList(fromIndex, toIndex);

        for (Post p : feed) {
            JsonObject postInfo = new JsonObject();
            JsonObject authorInfo = new JsonObject();

            User author = p.getAuthor();

            postInfo.addProperty("id", p.getId());
            postInfo.addProperty("postedAt", p.getTime().toString());
            postInfo.addProperty("commentCount", p.getComments().size());
            postInfo.addProperty("likeCount", p.getLikes().size());
            postInfo.addProperty("isLiked", isPostLikedByUser(p, currentUser));
            postInfo.addProperty("content", p.getContent());
            postInfo.addProperty("imageURI", p.getImageURI());
            postInfo.addProperty("type", p.getType().toString());

            postInfo.add("author", authorInfo);
            authorInfo.addProperty("id", author.getId());
            authorInfo.addProperty("username", author.getUsername());
            authorInfo.addProperty("firstName", author.getFirstName());
            authorInfo.addProperty("lastName", author.getLastName());
            authorInfo.addProperty("avatar", author.getAvatarImageURI());

            postsArray.add(postInfo);
        }
        return postsArray;
    }

    public static Post getPostFromParams(Request req, boolean required) {
        String postId = req.params("id");
        Post post = posts.get(postId);
        if (post == null && required)
            halt(404, error(404, "Post not found.").toString());
        return post;
    }

    public static boolean validateAddPostRequest(JsonObject postData) {
        try {
            postData.get("type").getAsString();

            if (postData.get("type").getAsString().equals("text"))
                postData.get("content").getAsString();
            else if (postData.get("type").getAsString().equals("photo"))
                postData.get("imageData").getAsString();
            else
                return false;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static JsonObject validateAddPostData(JsonObject postData, User currentUser) {
        PostType type;
        try {
            PostType.valueOf(postData.get("type").getAsString().toUpperCase());
        } catch (IllegalArgumentException e) {
            return error(400, "Post type is invalid.");
        }

        if (postData.has("content")) {
            String content = postData.get("content").getAsString();
            if (content.length() == 0)
                return error(400, "Missing content.");
            if (content.length() > 100)
                return error(400, "Post text too long.");
        }

        if (postData.has("imageData")) {
            String imageStr = postData.get("imageData").getAsString();

            if (!imageStr.startsWith("data:image/"))
                return error(400, "Uploaded file is not an image.");

            byte[] imageBytes = DatatypeConverter.parseBase64Binary(imageStr.substring(imageStr.indexOf(",") + 1));
            BufferedImage bufferedImage = null;
            try {
                bufferedImage = ImageIO.read(new ByteArrayInputStream(imageBytes));
            } catch (IOException e) {
                return error(400, "Uploaded file is not an image.");
            }
        }

        return null;
    }

}
