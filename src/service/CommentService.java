package service;

import com.google.gson.JsonObject;
import model.Comment;
import model.Post;
import model.User;
import spark.Request;

import java.time.LocalDateTime;

import static main.Application.comments;
import static main.Application.posts;
import static service.Auth.isPostVisibleToUser;
import static service.Util.error;
import static service.Util.success;
import static spark.Spark.halt;

public class CommentService {

    public static JsonObject addComment(JsonObject commentData, User currentUser) {
        String postId = commentData.get("postId").getAsString();
        String content = commentData.get("content").getAsString();

        Post post = posts.get(postId);
        Comment comment = new Comment(
                currentUser,
                post,
                content,
                LocalDateTime.now(),
                null);

        comments.add(comment);
        comments.saveChanges();

        post.getComments().add(comment);
        JsonObject responseData = new JsonObject();
        responseData.addProperty("commentId", comment.getId());

        return success(200, "Comment created successfully.", responseData);
    }

    public static JsonObject updateComment(JsonObject commentData, User currentUser) {
        String commentId = commentData.get("commentId").getAsString();
        String content = commentData.get("content").getAsString();

        Comment comment = comments.get(commentId);
        comment.setContent(content);
        comment.setEditedAt(LocalDateTime.now());
        comments.saveChanges();

        return success(200, "Comment updated successfully.");
    }

    public static JsonObject deleteComment(Comment selectedComment, User currentUser) {
        if (!selectedComment.getAuthor().equals(currentUser))
            return error(403, "Comment is not written by you.");
        if (!isPostVisibleToUser(selectedComment.getPost(), currentUser))
            return error(403, "You no longer have access to this comment.");

        try {
            comments.remove(selectedComment);
            comments.saveChanges();
            selectedComment.getPost().getComments().remove(selectedComment);
            return success(200, "Comment deleted successfully.");
        } catch (Exception e) {
            return error(500, "Server error. Deleting failed.");
        }
    }

    public static boolean validateAddCommentRequest(JsonObject commentData) {
        try {
            commentData.get("postId").getAsString();
            commentData.get("content").getAsString();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static JsonObject validateAddCommentData(JsonObject commentData, User currentUser) {
        String postId = commentData.get("postId").getAsString();
        try {
            Integer.parseInt(postId);
        } catch (NumberFormatException e) {
            return error(400, "Post ID is invalid.");
        }

        Post post = posts.get(postId);
        if (post == null)
            return error(404, "Post not found.");
        if (!isPostVisibleToUser(post, currentUser))
            return error(403, "You do not have access to this user's post.");

        String content = commentData.get("content").getAsString();
        if (content.length() == 0)
            return error(400, "Missing content");
        if (content.length() > 100)
            return error(400, "Comment text too long.");

        return null;
    }

    public static boolean validateEditCommentRequest(JsonObject commentData) {
        try {
            commentData.get("commentId").getAsString();
            commentData.get("content").getAsString();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static JsonObject validateEditCommentData(JsonObject commentData, User currentUser) {
        String commentId = commentData.get("commentId").getAsString();
        try {
            Integer.parseInt(commentId);
        } catch (NumberFormatException e) {
            return error(400, "Comment ID is invalid.");
        }

        Comment comment = comments.get(commentId);
        if (comment == null)
            return error(404, "Comment not found.");
        if (!comment.getAuthor().equals(currentUser))
            return error(403, "Comment is not written by you.");
        if (!isPostVisibleToUser(comment.getPost(), currentUser))
            return error(403, "You no longer have access to this comment.");

        String content = commentData.get("content").getAsString();
        if (content.length() == 0)
            return error(400, "Missing content");
        if (content.length() > 100)
            return error(400, "Comment text too long.");

        return null;
    }

    public static Comment getCommentFromParams(Request req, boolean required) {
        String commentId = req.params("id");
        Comment comment = comments.get(commentId);
        if (comment == null && required)
            halt(404, error(404, "Comment not found.").toString());
        return comment;
    }
}
