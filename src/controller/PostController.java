package controller;

import com.google.gson.JsonObject;
import model.Post;
import model.User;
import model.enums.PostType;
import spark.Request;
import spark.Response;
import spark.Route;

import static main.Application.jsonParser;
import static service.Auth.*;
import static service.PostService.*;
import static service.UserService.getUserFromParams;
import static service.Util.*;

public class PostController {

    public static Route getPostsForFeed = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);

        int offset = 0;
        int limit = 15;
        if (req.queryParams("offset") != null)
            offset = Integer.parseInt(req.queryParams("offset"));
        
        return returnPostsForFeed(offset, limit, currentUser);
    };

    public static Route getPostsByUser = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, false);
        User selectedUser = getUserFromParams(req, true);

        if (isProfileVisibleToUser(selectedUser, currentUser)) {
            return returnUserPosts(selectedUser, currentUser);
        } else {
            res.status(403);
            return error(res.status(), "You do not have access to this user's posts.");
        }
    };

    public static Route getPostsByUserAndType = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, false);
        User selectedUser = getUserFromParams(req, true);
        PostType selectedType;

        if (req.params("type").equals("photos"))
            selectedType = PostType.PHOTO;
        else if (req.params("type").equals("texts"))
            selectedType = PostType.TEXT;
        else {
            res.status(400);
            return error(res.status(), "Post type doesn't exist.");
        }

        if (isProfileVisibleToUser(selectedUser, currentUser)) {
            return returnUserPostsWithType(selectedUser, currentUser, selectedType);
        } else {
            res.status(403);
            return error(res.status(), "You do not have access to this user's posts.");
        }
    };

    public static Route getPost = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, false);
        Post selectedPost = getPostFromParams(req, true);

        if (isPostVisibleToUser(selectedPost, currentUser)) {
            return returnPost(selectedPost, currentUser);
        } else {
            res.status(403);
            return error(res.status(), "You do not have access to this user's post.");
        }
    };

    public static Route addPost = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        JsonObject postData = jsonParser.parse(req.body()).getAsJsonObject();

        if (!validateAddPostRequest(postData)) {
            res.status(400);
            return error(res.status(), "Bad request");
        }

        JsonObject error = validateAddPostData(postData, currentUser);
        if (error != null) {
            res.status(getErrorCode(error));
            return error;
        }

        JsonObject statusMessage = addPost(postData, currentUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };

    public static Route deletePost = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        Post selectedPost = getPostFromParams(req, true);

        JsonObject statusMessage = deletePost(selectedPost, currentUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };
}
