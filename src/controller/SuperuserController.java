package controller;

import com.google.gson.JsonObject;
import model.User;
import model.enums.Role;
import spark.Request;
import spark.Response;
import spark.Route;

import static main.Application.jsonParser;
import static main.Application.users;
import static service.Auth.authorizeFromRequest;
import static service.MessageService.sendDeleteReason;
import static service.UserService.getUserFromParams;
import static service.UserService.toggleBanned;
import static service.Util.getErrorCode;
import static service.Util.isError;

public class SuperuserController {

    public static Route toggleBanned = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authorizeFromRequest(req, Role.SUPERUSER);
        User selectedUser = getUserFromParams(req, true);

        JsonObject statusMessage = toggleBanned(selectedUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };

    @Deprecated
    public static Route sendDeleteReason = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authorizeFromRequest(req, Role.SUPERUSER);
        User selectedUser = getUserFromParams(req, true);
        JsonObject messageData = jsonParser.parse(req.body()).getAsJsonObject();

        JsonObject statusMessage = sendDeleteReason(currentUser, selectedUser, messageData);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };
}
