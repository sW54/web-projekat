package controller;

import com.google.gson.JsonObject;
import model.User;
import spark.Request;
import spark.Response;
import spark.Route;

import static main.Application.jsonParser;
import static service.Auth.*;
import static service.Util.*;

public class AuthController {

    public static Route login = (Request req, Response res) -> {
        res.type("application/json");
        String payload = req.body();
        JsonObject loginData = jsonParser.parse(payload).getAsJsonObject();
        if (!validateRequest(loginData)) {
            res.status(400);
            return error(res.status(), "Bad request");
        }

        JsonObject error = validateLogin(loginData);

        if (error != null) {
            res.status(getErrorCode(error));
            return error;
        }
        return issueToken(loginData);
    };

    public static Route check = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);

        if (currentUser.isBanned()) {
            res.status(403);
            return error(res.status(), "You are banned.");
        }

        return success(200, "You are logged in.");
    };
}
