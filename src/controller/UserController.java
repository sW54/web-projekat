package controller;

import com.google.gson.JsonObject;
import model.User;
import spark.Request;
import spark.Response;
import spark.Route;

import static main.Application.jsonParser;
import static service.Auth.*;
import static service.UserService.*;
import static service.Util.*;

public class UserController {

    public static Route getAccountInfo = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);

        return returnAccountInfo(currentUser);
    };

    public static Route getUserInfo = (Request req, Response res) -> {
        res.type("application/json");
        User selectedUser = getUserFromParams(req, true);

        return returnUserInfo(selectedUser);
    };

    public static Route getFriendList = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, false);
        User selectedUser = getUserFromParams(req, true);

        if (isProfileVisibleToUser(selectedUser, currentUser)) {
            return returnFriendList(currentUser, selectedUser);
        } else {
            res.status(403);
            return error(res.status(), "You do not have access to this user's friend list.");
        }
    };

    public static Route searchUsers = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, false);

        return returnSearchResults(req, currentUser);
    };

    public static Route getMutualFriends = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        User selectedUser = getUserFromParams(req, true);

        return returnMutualFriends(currentUser, selectedUser);
    };

    public static Route register = (Request req, Response res) -> {
        res.type("application/json");
        JsonObject registrationData = jsonParser.parse(req.body()).getAsJsonObject();

        if (!validateRegistrationRequest(registrationData)) {
            res.status(400);
            return error(res.status(), "Bad request");
        }

        JsonObject error = validateRegistrationData(registrationData);
        if (error != null) {
            res.status(error.get("error").getAsJsonObject().get("status").getAsInt());
            return error;
        }

        User validUser = registerUser(registrationData);
        return issueToken(validUser);
    };

    public static Route updateAccountInfo = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        JsonObject accountInfoData = jsonParser.parse(req.body()).getAsJsonObject();

        if (!validateAccountInfoRequest(accountInfoData)) {
            res.status(400);
            return error(res.status(), "Bad request");
        }

        JsonObject error = validateAccountInfoData(currentUser, accountInfoData);
        if (error != null) {
            res.status(getErrorCode(error));
            return error;
        }

        JsonObject statusMessage = updateAccountInfo(currentUser, accountInfoData);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };

    public static Route updatePassword = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        JsonObject passwordData = jsonParser.parse(req.body()).getAsJsonObject();

        if (!validatePasswordUpdateRequest(passwordData)) {
            res.status(400);
            return error(res.status(), "Bad request");
        }

        JsonObject error = validatePasswordUpdateData(currentUser, passwordData);
        if (error != null) {
            res.status(error.get("error").getAsJsonObject().get("status").getAsInt());
            return error;
        }

        JsonObject statusMessage = updateAccountPassword(currentUser, passwordData);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };
}
