package controller;

import com.google.gson.JsonObject;
import model.Comment;
import model.User;
import spark.Request;
import spark.Response;
import spark.Route;

import static main.Application.jsonParser;
import static service.Auth.authenticateFromRequest;
import static service.CommentService.*;
import static service.Util.*;

public class CommentController {

    public static Route postComment = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        JsonObject commentData = jsonParser.parse(req.body()).getAsJsonObject();

        if (!validateAddCommentRequest(commentData)) {
            res.status(400);
            return error(res.status(), "Bad request");
        }

        JsonObject error = validateAddCommentData(commentData, currentUser);
        if (error != null) {
            res.status(getErrorCode(error));
            return error;
        }

        JsonObject statusMessage = addComment(commentData, currentUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };

    public static Route editComment = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        JsonObject commentData = jsonParser.parse(req.body()).getAsJsonObject();

        if (!validateEditCommentRequest(commentData)) {
            res.status(400);
            return error(res.status(), "Bad request");
        }

        JsonObject error = validateEditCommentData(commentData, currentUser);
        if (error != null) {
            res.status(error.get("error").getAsJsonObject().get("status").getAsInt());
            return error;
        }

        JsonObject statusMessage = updateComment(commentData, currentUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };

    public static Route deleteComment = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        Comment selectedComment = getCommentFromParams(req, true);

        JsonObject statusMessage = deleteComment(selectedComment, currentUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };
}
