package controller;

import com.google.gson.JsonObject;
import model.Post;
import model.User;
import spark.Request;
import spark.Response;
import spark.Route;

import static service.Auth.authenticateFromRequest;
import static service.LikeService.toggleLike;
import static service.PostService.getPostFromParams;
import static service.Util.getErrorCode;
import static service.Util.isError;

public class LikeController {

    public static Route toggleLike = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        Post selectedPost = getPostFromParams(req, true);

        JsonObject statusMessage = toggleLike(selectedPost, currentUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };
}
