package controller;

import com.google.gson.JsonObject;
import model.User;
import spark.Request;
import spark.Response;
import spark.Route;

import static service.Auth.authenticateFromRequest;
import static service.MessageService.*;
import static service.UserService.getUserFromParams;
import static service.Util.getErrorCode;
import static service.Util.isError;

public class MessageController {

    public static Route getConversationHeads = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);

        return returnConversationHeads(currentUser);
    };

    public static Route getConversationMessages = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        User selectedUser = getUserFromParams(req, true);

        JsonObject conversation = returnConversation(currentUser, selectedUser);

        if (isError(conversation))
            res.status(getErrorCode(conversation));
        return conversation;
    };

    public static Route markConversationAsRead = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        User selectedUser = getUserFromParams(req, true);

        JsonObject statusMessage = markConversationAsRead(currentUser, selectedUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };
}
