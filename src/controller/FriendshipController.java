package controller;

import com.google.gson.JsonObject;
import model.User;
import spark.Request;
import spark.Response;
import spark.Route;

import static service.Auth.authenticateFromRequest;
import static service.FriendshipService.*;
import static service.UserService.getUserFromParams;
import static service.Util.getErrorCode;
import static service.Util.isError;

public class FriendshipController {

    public static Route getFriendshipStatus = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        User selectedUser = getUserFromParams(req, true);

        return returnFriendshipStatus(currentUser, selectedUser);
    };

    public static Route getRequests = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);

        return returnRequests(currentUser);
    };

    public static Route getFriends = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);

        return returnFriends(currentUser);
    };

    public static Route updateFriendshipStatus = (Request req, Response res) -> {
        res.type("application/json");
        User currentUser = authenticateFromRequest(req, true);
        User selectedUser = getUserFromParams(req, true);
        String option = req.params("option");

        JsonObject statusMessage = updateFriendshipStatus(option, currentUser, selectedUser);

        if (isError(statusMessage))
            res.status(getErrorCode(statusMessage));
        return statusMessage;
    };
}
