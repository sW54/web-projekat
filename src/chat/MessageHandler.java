package chat;

import com.google.gson.JsonObject;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import model.Conversation;
import model.Message;
import model.User;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static main.Application.*;
import static service.Auth.readTokenFromQuery;

@WebSocket
public class MessageHandler extends WebSocketHandler {

    private static final Map<Session, User> sessions = new ConcurrentHashMap<>();

    public static void deliverMessage(User sender, User receiver, String content) {
        try {
            List<Session> receiverSessions = sessions.keySet().stream()
                    .filter(session -> session.isOpen() & sessions.get(session).equals(receiver))
                    .collect(Collectors.toList());

            List<Session> senderSessions = sessions.keySet().stream()
                    .filter(session -> session.isOpen() & sessions.get(session).equals(sender))
                    .collect(Collectors.toList());

            JsonObject message = new JsonObject();
            message.addProperty("sender", sender.getUsername());
            message.addProperty("content", content);
            message.addProperty("time", LocalDateTime.now().toString());

            for (Session s : receiverSessions)
                s.getRemote().sendString(String.valueOf(message));
            for (Session s : senderSessions)
                s.getRemote().sendString(String.valueOf(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void storeMessage(User sender, User receiver, String content) {
        Conversation conversation = conversations.getByUserPair(sender, receiver);
        if (conversation == null) {
            conversation = new Conversation(sender, receiver, LocalDateTime.now(), true);
            conversation.setMessages(new ArrayList<>());
            conversations.add(conversation);
            conversations.saveChanges();
        }

        Message message = new Message(
                conversation,
                sender,
                content,
                LocalDateTime.now());

        messages.add(message);
        messages.saveChanges();

        conversation.getMessages().add(message);
        conversation.setUnread(true);
        conversation.setTime(LocalDateTime.now());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) throws Exception {
        String query = session.getUpgradeRequest().getRequestURI().getQuery();
        try {
            Jws<Claims> claims = readTokenFromQuery(query);
            String senderId = (String) claims.getBody().get("id");
            User currentUser = users.get(senderId);
            sessions.put(session, currentUser);
            System.out.println("Added: " + currentUser);
        } catch (Exception ignored) {

        }
    }

    @OnWebSocketClose
    public void onClose(Session session, int statusCode, String reason) {
        sessions.remove(session);
    }

    @OnWebSocketMessage
    public void onMessage(Session session, String message) {
        JsonObject messageJson = jsonParser.parse(message).getAsJsonObject();

        User sender = sessions.get(session);
        User receiver = users.getByUsername(messageJson.get("receiver").getAsString());
        String content = messageJson.get("content").getAsString();

        deliverMessage(sender, receiver, content);
        storeMessage(sender, receiver, content);
    }

    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.getPolicy().setIdleTimeout(3600000); // 1 hour
    }
}
