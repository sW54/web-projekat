package chat;

import model.Conversation;
import model.User;

public class DirectedUserPair {
    private User sender;

    private User receiver;

    private Conversation conversation;

    public DirectedUserPair(User sender, User receiver, Conversation conversation) {
        this.sender = sender;
        this.receiver = receiver;
        this.conversation = conversation;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    @Override
    public String toString() {
        return "DirectedUserPair{" +
                "sender=" + sender +
                ", receiver=" + receiver +
                '}';
    }
}
