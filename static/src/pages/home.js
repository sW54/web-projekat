define(["Vue"], function(Vue) {
  let Home = Vue.component("home", {
    template: `
    <div class="h-screen bg-gray-100">
      <MainLayout/>
    </div>
    `,
    head () {
      return {
        title: this.$route.meta.title
      }
    },
    watch: {
      $route(to, from) {
        this.$root.$emit('setProfile');
      }
    },
  })

  return Home
})