define(["Vue"], function(Vue) {
  let NotFound = Vue.component("NotFound", {
    template: `
    <div class="text-center font-mono">
      <h1 class="flex justify-center mx-auto w-5/6 md:w-1/2 text-6xl text-gray-900 font-extrabold font-semi
      mt-52 border-b border-gray-300 py-4">
        404
      </h1>
      <h1 class="text-4xl sm:text-6xl text-gray-900 font-light font-semi pt-2">
        Page not found.
      </h1>
    </div>
    `
  })

  return NotFound
})