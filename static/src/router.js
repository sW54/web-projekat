define(function(require){

  let VueRouter = require("VueRouter")

  // its your pages
  let Home = require("./pages/home")
  let NotFound = require("./pages/notfound")

  const routes = [
    {
        path: "/",
        component: Home,
        name: "Home",
    },{
      path: "/:username",
      component: Home,
      name: "Profile",
    },

    // default 404
    {
      path: "*",
      component: NotFound,
      name: "NotFound",
      meta: { title: '404' }
    }
  ]

  let router = new VueRouter({
    // mode: "history",
    routes
  })

  // middleware
  router.beforeEach((to, from, next) => {
    next()
  })
  
  return router
})