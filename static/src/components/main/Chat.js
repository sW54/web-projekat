define(["Vue"], function(Vue) {
  let Chat = Vue.component("Chat", {
    template: `
    <div class="bg-gray-800 bg-opacity-70 flex-col z-40 w-72 h-112 top-1/2 -translate-y-1/2 rounded-xl overflow-hidden">
      <div class="flex h-12 bg-gray-800 my-auto justify-between cursor-pointer select-none" @click.self="$emit('close-chat')">
        <div class="text-lg text-white my-auto ml-4 border-b border-transparent hover:border-gray-500"
        @click="goToProfile(chatter)">
          {{ chatter.username }}
        </div>
      </div>

      <div id="chat-scroll" class="h-39/50 text-sm overflow-auto pb-4">
        <div v-for="msg in messages" :key="msg.text" class="overflow-x-hidden">
          <div v-if="msg.sender == chatter.username" class="text-left">
            <p style="max-width: 250px;" class="inline-block text-left mt-3 ml-2 mr-6 px-4 py-2
            bg-gray-50 rounded-xl break-words">
              {{ msg.content }}
            </p>
          </div>
          <div v-if="msg.sender != chatter.username" class="text-right">
            <p style="max-width: 250px;" class="inline-block text-left mt-3 ml-6 mr-2 px-4 py-2
            text-white rounded-xl break-words bg-red-500">
              {{ msg.content }}
            </p>
          </div>
        </div>
      </div>

      <div class="absolute bottom-0 h-12 px-2 w-full">
        <textarea id="chat-text-area" type="text" @keydown.enter="sendMessage" v-model="message" class="w-full py-1
        bg-transparent rounded-xl border px-4 border-gray-100 text-white border-opacity-75
        outline-none caret-gray-300 resize-none hidden-scrollbar" placeholder="chat..." rows="1"/>
      </div>
    </div>
    `,
    props: [ 'chatter' ],
    data () {
      return {
        messages: [],
        message: ''
      }
    },
    mounted () {
      this.$nextTick(function () {
        window.addEventListener('resize', this.handleResize);
        document.getElementById('chat-text-area').addEventListener('keydown', (e) => {
          if (e.key == 'Enter') {
            e.preventDefault();
          }
        });
        this.handleResize();
      })

      this.getMessages();
      if (this.chatter.isUnread) {
          this.markAsRead();
      }
    },
    methods: {
      sendMessage () {
        if (/\S/.test(this.message)) {
          let message = {
            'content': this.message,
            'receiver': this.chatter.username
          }
          this.$emit('sendMessage', message);
          this.autoscroll();
          this.message = '';
        }
      },
      getMessages () {
        axios.get('/account/conversations/' + this.chatter.username, {
          headers: { 'Authorization': 'Bearer ' + window.localStorage.getItem('token') }
        })
        .then ((response) => {
          if (response.data) {
            this.messages = response.data.messages;
            this.autoscroll();
          }
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      updateMessages(message) {
        this.messages.push(message);
        this.autoscroll();
      },
      goToProfile(user) {
        this.$router.push('/' + this.chatter.username, () => {});
        this.$root.$emit('setProfile');
      },
      handleResize () {
        const chatBoxHeight = document.getElementById('chat').clientHeight;
        document.getElementById('chat-scroll').style.height = (chatBoxHeight - 6*16) + 'px';
      },
      autoscroll() {
        let scrollElement = document.getElementById('chat-scroll');
        setTimeout(() => scrollElement.scrollTo(0, scrollElement.scrollHeight), 25);
      },
      markAsRead() {
        axios.post('/account/conversations/' + this.chatter.username + '/read', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.chatter.isUnread = false;
        })
      }
    },
    watch: { 
      chatter: function(newVal, oldVal) {
        this.getMessages();
      }
    },
    beforeDestroy () {
      window.removeEventListener('resize', this.handleResize);
    }
  })
  return Chat
})