define(["Vue"], function(Vue) {
  let Profile = Vue.component("Profile", {
    template: `
    <div class="block bg-white w-88 px-6">

      <!-- create post button -->
      <button @click="showCreatePostModal = true" v-if="isActiveSession" class="mt-6 h-10 w-full
      rounded-lg bg-gradient-to-br from-red-400 to-pink-500 text-white text-sm font-mono font-bold
      tracking-wide shadow-md transform hover:scale-95">
        create post
      </button>

      <div v-else class="flex gap-x-4">

        <!-- login button -->
        <button @click="showLoginModal = true" class="w-full mt-6 h-10 rounded-lg bg-gradient-to-br
        from-red-400 to-pink-500 text-white text-sm font-mono font-bold tracking-wide shadow-md
        transform hover:scale-95">
          login
        </button>

        <!-- register button -->
        <button @click="showRegistrationModal = true" class="w-full mt-6 h-10 rounded-lg
        bg-gradient-to-br from-pink-500 to-rose-500 text-white text-sm font-mono font-bold
        tracking-wide shadow-md transform hover:scale-95">
          register
        </button>

      </div>

      <div v-if="show404" class="text-center mt-10">
          <img src="./src/assets/images/profile404.png" alt="profile image"
          class="w-22 h-22 bg-white rounded-full mx-auto my-auto">
          <div class="mt-2 text-xl text-gray-400 tracking-wide">
            user not found
          </div>
      </div>

      <ProfileInfo v-if="profile" :profile="profile" class="mt-10"/>

      <Login v-if="showLoginModal" v-on:close="closeLoginModal" />
      <Register v-if="showRegistrationModal" v-on:close="closeRegistrationModal" />
      <CreatePost v-if="showCreatePostModal" v-on:close="closeCreatePostModal" />
    </div>
    `,
    data () {
      return {
        profile: null,
        renderProfile: true,
        showLoginModal: false,
        isActiveSession: false,
        showRegistrationModal: false,
        showCreatePostModal: false,
        show404: false,
      }
    },
    mounted () {
      this.checkSession();
      this.$root.$on('setProfile', () => {
        this.show404 = false;
        this.profile = null;
        this.setProfile();
      });
      this.setProfile();
    },
    methods: {
      closeLoginModal () {
        this.showLoginModal = false;
      },
      closeRegistrationModal () {
        this.showRegistrationModal = false;
      },
      closeCreatePostModal () {
        this.showCreatePostModal = false;
      },
      checkSession () {
        window.localStorage.getItem('session') ? this.isActiveSession = true : this.isActiveSession = false;
      },
      isMe () {
        if (window.localStorage.getItem('session')) {
          if (this.profile.id == JSON.parse(window.localStorage.getItem('session')).id) {
            return true;
          }
          return false;
        }
        return false;
      },
      setProfile () {
        let username;
        if (this.$route.params.username) {
          username = this.$route.params.username;
        }
        else if (window.localStorage.getItem('session')) {
          username = JSON.parse(window.localStorage.getItem('session')).username;
        } else {
          this.$root.$emit('setPostsProfile', null, false);
        }
        if (username) {
          axios.get('/users/' + username)
          .then ((response) => {
            if (response.data) {
              this.profile = response.data;
              this.checkSession();
            }
          })
          .catch ((error) => {
            this.show404 = true;
          })
        }
      },
    },
  })
  return Profile
})