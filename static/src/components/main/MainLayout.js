define(["Vue"], function(Vue) {
  let MainLayout = Vue.component("MainLayout", {
    template: `
    <div class="flex p-6 h-screen">
      <div class="flex h-full w-full rounded-3xl overflow-hidden shadow-md">
        <Sidebar />
        <Content class="flex-1 flex-grow"/>
        <Profile class="hidden md:block"/>
      </div>
    </div>
    `,
    mounted () {
      this.$root.$on('logout', () => { this.logout(); });
      this.checkLogin();
    },
    methods: {
      checkLogin () {
        if (window.localStorage.getItem('token')) {
          axios.get('/auth/check', {
            headers: {'Authorization': window.localStorage.getItem('token')}
          })
          .then((response) => {
            // all good
          })
          .catch(error => {
            this.logout();
          });
        }
      },
      logout () {
        console.log("Logging out...");
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('session');
        var that = this
        setTimeout(function(){
          location.reload();
        }, 150)
      },
    }
  })
  return MainLayout
})