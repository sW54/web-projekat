define(["Vue"], function(Vue) {
  let Posts = Vue.component("Posts", {
    template: `
    <div>
      <div v-if="posts.length > 0" class="w-auto grid grid-cols-1 p-6 md:grid-cols-2 xl:grid-cols-3 grid-rows-1 gap-x-4">
      <!-- 3 cols -->
        <div class="hidden xl:block">
          <transition-group name="post-fade">
            <div v-for="post in postsThreeColumns[0]" :key="post.id" class="mb-4">
              <Post :data="post" :gallery="filter == '/photos'" v-on:showPostModal="onShowPostModal"
              v-on:postDeleted="onPostDeleted" :author="post.author ? post.author : profile"
              v-on:showDeleteReasonModal="onShowDeleteReasonModal"/>
            </div>
          </transition-group>
        </div>
        <div class="hidden xl:block">
          <transition-group name="post-fade">
            <div v-for="post in postsThreeColumns[1]" :key="post.id" class="mb-4">
              <Post :data="post" :gallery="filter == '/photos'" v-on:showPostModal="onShowPostModal"
              v-on:postDeleted="onPostDeleted" :author="post.author ? post.author : profile"
              v-on:showDeleteReasonModal="onShowDeleteReasonModal"/>
            </div>
          </transition-group>
        </div>
        <div class="hidden xl:block">
          <transition-group name="post-fade">
            <div v-for="post in postsThreeColumns[2]" :key="post.id" class="mb-4">
              <Post :data="post" :gallery="filter == '/photos'" v-on:showPostModal="onShowPostModal"
              v-on:postDeleted="onPostDeleted" :author="post.author ? post.author : profile"
              v-on:showDeleteReasonModal="onShowDeleteReasonModal"/>
            </div>
          </transition-group>
        </div>


        <!-- 2 cols -->
        <div class="hidden md:block xl:hidden">
          <transition-group name="post-fade">
            <div v-for="post in postsTwoColumns[0]" :key="post.id" class="mb-4">
              <Post :data="post" :gallery="filter == '/photos'" v-on:showPostModal="onShowPostModal"
              v-on:postDeleted="onPostDeleted" :author="post.author ? post.author : profile"
              v-on:showDeleteReasonModal="onShowDeleteReasonModal"/>
            </div>
          </transition-group>
        </div>
        <div class="hidden md:block xl:hidden">
          <transition-group name="post-fade">
            <div v-for="post in postsTwoColumns[1]" :key="post.id" class="mb-4">
              <Post :data="post" :gallery="filter == '/photos'" v-on:showPostModal="onShowPostModal"
              v-on:postDeleted="onPostDeleted" :author="post.author ? post.author : profile"
              v-on:showDeleteReasonModal="onShowDeleteReasonModal"/>
            </div>
          </transition-group>
        </div>


        <!-- 1 col -->
        <div class="block md:hidden">
          <transition-group name="post-fade">
            <div v-for="post in posts" :key="post.id" class="mb-4">
              <Post :data="post" :gallery="filter == '/photos'" v-on:showPostModal="onShowPostModal"
              v-on:postDeleted="onPostDeleted" :author="post.author ? post.author : profile"
              v-on:showDeleteReasonModal="onShowDeleteReasonModal"/>
            </div>
          </transition-group>
        </div>

        <!-- post modal -->
        <DetailedPost :postId="modalPost.id" :author="modalAuthor" v-if="showPostModal"
        v-on:postDeleted="onPostDeleted"  v-on:close="closePostModal" v-on:toggleLike="onToggleLike" 
        v-on:showDeleteReasonModal="onShowDeleteReasonModal"/>

        <!-- delete reason modal -->
        <DeleteReason v-if="showDeleteReasonModal" :author="modalAuthor" v-on:close="closeDeleteReasonModal"/>
      </div>

      <div v-if="(posts.length == 0 && !(amGuest && !profile) && !(!isFriend && profile && profile.locked)
      && showLoading) || (posts.length == 0 && showLoading) && !(amGuest && !profile) && !(!isFriend && profile && profile.locked)"
      class="block text-center mx-auto">
        <div>
          <i class="fas fa-circle-notch text-gray-700 mt-20 w-20 h-20 mx-auto animate-spin"/>
        </div>
        <div class="text-gray-700 text-4xl tracking-wider">loading</div>
      </div>

    </div>
    
    `,
    data () {
      return {
        profile: null,
        posts: [],
        isFriend: false,
        amGuest: false,
        filter: '',
        showPostModal: false,
        showDeleteReasonModal: false,
        modalPost: null,
        modalAuthor: null,
        showLoading: true,
        isRendered: true
      }
    },
    mounted () {
      window.localStorage.getItem('token') ? this.amGuest = false : this.amGuest = true;
      this.$root.$on('setPostsProfile', (profile, isFriend, amGuest) => {
        this.posts = [];
        this.profile = profile;
        this.isFriend = isFriend;
        this.getPosts();
      })
      this.$root.$on('reloadPosts', () => {
        this.posts = [];
        this.getPosts();
      });
      this.$root.$on('setFilter', (filter) => {
        if (filter == 'all') this.filter = '';
        else if (filter == 'photos') this.filter = '/photos';
        else if (filter == 'texts') this.filter = '/texts';
        else this.filter = '';
        this.getPosts();
      })
    },
    methods: {
      loadMore () {
        if (window.localStorage.getItem('session') != null && !this.$route.params.username) {
          if (this.isRendered == false) {
            return;
          }
          this.isRendered = false;
          axios.get('/feed', 
            {
              headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')},
              params: { 'offset': this.posts.length }
            })
          .then ((response) => {
            if (response.data) {
              this.showLoading = false;
              this.posts = [...this.posts, ...response.data];
            }
          })
          .catch ((error) => {
          })
        }
      },
      onShowPostModal (p, a) {
        this.modalPost = p;
        this.modalAuthor = a;
        this.showPostModal = true;
      },
      onPostDeleted (id) {
        let index = this.posts.map(function(e) { return e.id }).indexOf(id);

        if (index !== -1) {
          this.posts.splice(index, 1);
        }
      },
      onShowDeleteReasonModal (author) {
        console.log(author);
        this.modalAuthor = author;
        this.showDeleteReasonModal = true;
      },
      getPosts () {
        if (window.localStorage.getItem('session') != null && !this.$route.params.username) {
          this.getFeedPosts(); 
          this.filter = '';
        }
        else if (!this.profile) {
          this.posts = [];
          this.showLoading = false;
        }
        else if (!this.profile.locked || (this.profile.locked && this.isFriend) || this.isMe() || this.amSuperuser()) {
          this.getProfilePosts();
        } else {
          this.posts = [];
        }
      },
      getProfilePosts() {
        axios.get('/users/' + this.profile.username + '/posts' + this.filter,
        { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
        .then ((response) => {
          if (response.data) {
            this.posts = response.data;
            this.showLoading = false;
          }
        })
        .catch ((error) => {
        })
      },
      getFeedPosts() {
        this.isRendered = false;
        axios.get('/feed', {headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}})
        .then ((response) => {
          if (response.data) {
            this.posts = [...this.posts, ...response.data];
            this.showLoading = false;
          }
        })
        .catch ((error) => {
        })
      },
      isMe () {
        if (window.localStorage.getItem('session') != null) {
          if (this.profile.id == JSON.parse(window.localStorage.getItem('session')).id) {
            return true;
          }
          return false;
        }
        return false;
      },
      amSuperuser() {
        if (window.localStorage.getItem('session')) {
          let role = JSON.parse(window.localStorage.getItem('session')).role;
          return (role == "SUPERUSER");
        }
      },
      closePostModal () {
        this.showPostModal = false;
      },
      closeDeleteReasonModal() {
        this.showDeleteReasonModal = false;
      },
      onToggleLike (id, isLiked) {
        let post = this.posts.find(e => e.id == id);
        post.isLiked = !post.isLiked;
        isLiked ? post.likeCount++ : post.likeCount--;
      }
    },
    watch: {
      $route(to, from) {
        this.posts = [];
      }
    },
    updated: function () {
      this.$nextTick(function () {
        // prevent duplicate /feed?offset=x requetsts
        this.isRendered = true;
      })
    },
    computed: {
      postsThreeColumns () {
        return [this.posts.filter(e => this.posts.indexOf(e) % 3 == 0).sort((a, b) => {a.postedAt > b.postedAt}),
                this.posts.filter(e => this.posts.indexOf(e) % 3 == 1).sort((a, b) => {a.postedAt > b.postedAt}),
                this.posts.filter(e => this.posts.indexOf(e) % 3 == 2).sort((a, b) => {a.postedAt > b.postedAt})]
      },
      postsTwoColumns () {
        return [this.posts.filter(e => this.posts.indexOf(e) % 2 == 0).sort((a, b) => {a.postedAt > b.postedAt}),
                this.posts.filter(e => this.posts.indexOf(e) % 2 == 1).sort((a, b) => {a.postedAt > b.postedAt})]
      }
    }
  })
  return Posts
})