define(["Vue"], function(Vue) {
  let Post = Vue.component("Post", {
    template: `
    <div @mouseover="showDelete = true" @mouseleave="showDelete = false"
    :class="gallery ? 'relative' : 'p-2 py-4'" class="rounded-3xl bg-white font-mono shadow-sm">

      <!-- gallery view likes -->
      <div v-show="gallery" @click="showPostModal" class="absolute flex flex-col w-full
      h-full bg-gray-900 bg-opacity-50 hover:opacity-100 opacity-0 rounded-xl cursor-pointer">
        <div class="text-center my-auto">
          <i class="fas fa-heart text-white w-6 h-6"/>
          <div class="text-lg text-white font-bold">{{ data.likeCount }}</div>
        </div>
      </div>

      <!-- author -->
      <div v-if="!gallery" class="flex justify-between px-2">
        <div class="flex rounded-full">
          <img @click="goToProfile" :src="author.avatar ? author.avatar : './src/assets/images/profile.png'" alt=""
          class="h-10 w-10 rounded-full cursor-pointer object-cover">
          <div class="block my-auto">
            <div class="ml-2 text-sm font-medium text-gray-700 cursor-pointer"
            @click="goToProfile">{{ author.username }}</div>
            <div class="ml-2 text-xs font-medium text-gray-300 cursor-pointer"
            @click="goToProfile">{{ author.firstName }} {{author.lastName}}</div>
          </div>
        </div>
        <div class="my-auto">
          <a v-if="(authorIsMe() || amSuperuser()) && showDelete" @click="deletePost()">
            <i class="fas fa-trash text-sm text-gray-300 hover:text-gray-500 cursor-pointer" />
          </a>
        </div>
      </div>

      <!-- image/text -->
      <div class="mt-4">
        <div v-if="data.type == 'PHOTO'" @click='showPostModal' class="cursor-pointer">
          <img :src="data.imageURI" class="w-full rounded-2xl">
        </div>
        <div v-else class="px-2">
          <div class="text-sm tracking-tight text-left cursor-pointer"
          @click='showPostModal'>{{ data.content }}</div>
          <div v-if="data.imageURI" @click='showPostModal' class="cursor-pointer">
            <img :src="data.imageURI" class="h-14 text-right mt-2 rounded-xl">
          </div>
        </div>
      </div>

      <!-- like comment share -->
      <div v-if="!gallery" class="flex pt-3 pb-1 px-2 gap-x-2">
        <a @click="toggleLike" :class="data.isLiked ? 'text-red-500 hover:text-gray-500' : 'text-gray-300 hover:text-red-500'">
          <i class="fas fa-heart cursor-pointer"/>
        </a>
        <a @click='showPostModal' class="mt-0 align-begin">
          <i class="fas fa-comment text-gray-300 hover:text-blue-500 cursor-pointer"/>
        </a>
        <a>
          <i class="fas fa-location-arrow text-gray-300 hover:text-purple-500 cursor-pointer" />
        </a>
      </div>

      <!-- liked by -->
      <div v-if="!gallery" class="text-left px-2 text-xs">
        <div v-html="likeMessage"></div>
      </div>

      <!-- description -->
      <div v-if="data.type == 'PHOTO' && data.content && !gallery" class="flex px-2 mt-2">
        <i class="fas fa-quote-left text-gray-700 w-2 h-2 my-auto" />
        <div class="text-xs text-left truncate px-2 font-semibold cursor-pointer"
        @click='showPostModal'> {{ data.content }} </div>
      </div>
      <div v-if="!gallery" class="flex" :class="[data.type == 'PHOTO' && data.content ? 'px-4' : 'mt-0.5']">
        <div class="text-xs text-left truncate px-2 text-gray-500" :title="altPostTime">{{ postTime }}</div>
      </div>
    </div>
    `,
    props: ["data", "author", 'gallery'],
    data () {
      return {
        showDelete: false,
      }
    },
    methods: {
      goToProfile () {
        this.$router.push('/' + this.author.username, () => {});
        this.$root.$emit('setProfile');
      },
      showPostModal () {
        this.$emit('showPostModal', this.data, this.author);
      },
      deletePost () {
        axios.delete('/posts/' + this.data.id, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          if (this.amSuperuser() && !this.authorIsMe()) {
            this.$emit('showDeleteReasonModal', this.author);
          }
          
          if (response.data) {
            this.$emit('postDeleted', this.data.id);
          }
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      authorIsMe () {
        if (window.localStorage.getItem('session')) {
          let me = JSON.parse(window.localStorage.getItem('session')).username;
          if (this.author.username == me) return true;
          return false;
        }
        return false;
      },
      amSuperuser() {
        if (window.localStorage.getItem('session')) {
          let role = JSON.parse(window.localStorage.getItem('session')).role;
          return (role == "SUPERUSER");
        }
      },
      toggleLike () {
        if (window.localStorage.getItem('token')) {
          axios.post('/posts/' + this.data.id + '/like', {},
          { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
          .then ((response) => {
            this.data.isLiked = !this.data.isLiked;
            this.data.isLiked ? this.data.likeCount++ : this.data.likeCount--;
          })
          .catch ((error) => {
            console.log(error);
          })
        }
      },
    },
    computed: {
      postText () {
        if (this.data.text.length > 150) {
          return this.data.text.substring(0, 150) + ' ... (read more)';
        }
        return this.data.text;
      },
      postTime () {
        let dt = new Date(this.data.postedAt);
        
        let now = new Date();
        const minutesSince = Math.floor((now.getTime() - dt.getTime())/(1000 * 60));

        if (minutesSince == 0) {
          return 'now';
        }
        else if (minutesSince < 60) {
          return Math.floor(minutesSince) + ' minutes ago';
        }
        else if (minutesSince / 60 < 24) {
          return Math.floor(minutesSince / 60) + ' hours ago';
        }
        else if (minutesSince / ( 60 * 24) < 14) {
          return Math.floor(minutesSince / ( 60 * 24)) + ' days ago';
        }
        else  {
          return Math.floor(minutesSince / (60 * 24 * 7)) + ' weeks ago';
        }
      },
      altPostTime () {
        let dt = new Date(this.data.postedAt);
        let timeString = dt.toLocaleString(undefined, {
          hourCycle: 'h23',
          hour: '2-digit',
          minute: 'numeric',
        })
        let dateString = dt.toLocaleString(undefined, {
          weekday: 'long',
          day: 'numeric',
          month: 'short',
          year: 'numeric',
        })
        return timeString + ' ' + dateString;
      },
      likeMessage () {
        if (this.data.likeCount == 0) {
          return 'Be the first to like.';
        } else if (this.data.likeCount == 1) {
          return 'Liked by <b>1</b> person.';
        } else {
          return 'Liked by <b>' + this.data.likeCount + '</b> people.';
        }
      }
    }
  })
  return Post
})