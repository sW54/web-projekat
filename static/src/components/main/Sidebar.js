define(["Vue"], function(Vue) {
  let Sidebar = Vue.component("Sidebar", {
    template: `
    <div class="relative bg-white w-22 h-full font-mono flex-shrink-0">

      <div v-if="showFilter" class="absolute w-full top-8 z-10 align-middle text-center ">
        <a @click="expandFilters = !expandFilters" :class="expandFilters ? 'text-black' : 'text-gray-700 hover:text-black'">
          <i class="fas fa-filter h-6 w-6 cursor-pointer align-middle my-auto"
          />
        </a>

        <transition name="slide-fade-b">
          <div v-if="expandFilters" class="flex justify-center flex-col py-4 mx-auto
          text-center gap-y-0.5 text-xs text-gray-700 tracing-widest font-sans font-bold">
            <div @click="setFilter('all')" :class="currentFilter == 'all' ? 'bg-black text-gray-100' : ''"
            class="w-14 cursor-pointer mx-auto hover:bg-black hover:text-gray-100 py-0.5">
              ALL
            </div>
            <div @click="setFilter('photos')" :class="currentFilter == 'photos' ? 'bg-black text-gray-100' : ''"
            class="w-14 cursor-pointer mx-auto hover:bg-black hover:text-gray-100 py-0.5">
              PHOTOS
            </div>
            <div @click="setFilter('texts')" :class="currentFilter == 'texts' ? 'bg-black text-gray-100' : ''"
             class="w-14 cursor-pointer mx-auto hover:bg-black hover:text-gray-100 py-0.5">
              POSTS
            </div>
          </div>
        </transition>

      </div>
      
      <!-- top fade & arrow -->
      <div class="w-full flex absolute hidden top-1/5 -mt-10 z-10 h-20 justify-center
      bg-gradient-to-b from-white via-white to-transparent" id="fade-top">
        <div  class="absolute top-1">
          <i class="fas fa-chevron-up text-gray-800"/>
        </div>
      </div>

      <!-- chatters -->
      <div class="flex-col absolute w-full pt-5 pb-8 top-1/5 justify-between h-3/5
      overflow-auto hidden-scrollbar select-none" id="scroller">

        <div v-for="user in chatters" :key="user.id" class="mx-auto w-14 h-14 mb-2 rounded-full shadow-md
        transform cursor-pointer" @click="openChat(user)"
        :class="[user.isUnread ? 'p-0.5 bg-gradient-to-b from-red-500 to-purple-700' : 'p-0',
        selected != null && user.id == selected.id ? 'translate-x-2 scale-105 hover:scale-95' : 'hover:scale-105']">
          <div v-if="user.isAdmin" class="absolute right-0 bottom-0">
            <i class="fas fa-crown text-yellow-500 text-lg"/>
          </div>
          <img :src="user.avatar ? user.avatar : './src/assets/images/profile.png'" alt=""
          class="rounded-full w-full h-full bg-white border-2 border-white object-cover">
        </div>
      </div>

      <!-- bottom fade & arrow -->
      <div class="w-full flex absolute hidden bottom-1/5 mt-10 z-10 h-20 justify-center
      bg-gradient-to-t from-white via-white to-transparent" id="fade-bot">
        <div class="absolute bottom-1">
          <i class="fas fa-chevron-down text-gray-800"/>
        </div>
      </div>

      <div v-if="session" class="absolute w-full bottom-6 z-10 align-middle text-center">
        
        <transition name="slide-fade">
          <div v-if="expandOptions" class="flex justify-center flex-col pb-2 mx-auto
          text-center gap-y-0.5">
            <a @click="visitProfile">
              <i class="fas fa-user h-6 w-6 text-gray-300
              hover:text-gray-700 cursor-pointer mx-auto"/>
            </a>
            <a @click="openRequestsModal">
              <i class="fas fa-user-clock h-7 w-7 text-gray-300
              hover:text-gray-700 cursor-pointer mx-auto"/>
            </a>
            <a @click="logout">
              <i class="fas fa-sign-out-alt h-6 w-6 text-gray-300
              hover:text-gray-700 cursor-pointer mx-auto"/>
            </a>
          </div>
        </transition>

        <img @click="expandOptions = !expandOptions" :src="session.avatar ? session.avatar : './src/assets/images/profile.png'"
        class="h-10 w-10 rounded-full shadow-md text-gray-300 cursor-pointer mx-auto object-cover"/>
      </div>
      
      <Chat v-if="selected" :chatter="selected"  id="chat"v-on:close-chat="selected=null"
      v-on:sendMessage="send" ref="chat" class="absolute left-28" />
      <Requests v-if="showRequestsModal" v-on:close="closeRequestsModal" />
    </div>
    `,
    data () {
      return {
        chatters: [],
        selected: null,
        session: null,
        expandOptions: false,
        expandFilters: false,
        showFilter: false,
        showRequestsModal: false,
        currentFilter: 'all',
        webSocket: null,
        sound: new Audio("src/assets/sounds/message.mp3")
      }
    },
    mounted () {
      document.getElementById("scroller").addEventListener('scroll', this.handleScroll);
      this.$root.$on('openConversation', (username) => { this.openConversation(username) });
      this.$root.$on('login', () => { this.getChatters(); this.openSocket(); this.getSession(); });
      this.$root.$on('sendDeleteReason', (message) => { this.send(message) });
      this.getChatters();
      this.openSocket();
      this.getSession();
      this.setFilterVisibility();
    },
    methods: {
      openSocket () {
        if (window.localStorage.getItem('token')) {
          axios.get('/auth/check', {
            headers: { "Authorization": 'Bearer ' + window.localStorage.getItem('token') }
          })
          .then ((response) => {
            let that = this;

            this.webSocket = new WebSocket("ws://" + location.host + "/chat?token=" + window.localStorage.getItem('token'));
            this.webSocket.onmessage = function (msg) { 
              let message = JSON.parse(msg.data);

              if (that.selected && (message.sender == that.selected.username || message.sender == that.session.username)) {
                that.$refs.chat.updateMessages(message);

                if (message.sender == that.selected.username) {
                  that.$refs.chat.markAsRead();
                }
              }

              if (!document.hasFocus() || !that.selected || !(message.sender == that.selected.username || message.sender == that.session.username)) {
                that.sound.play();
              }

              that.getChatters(); 
            };
          })
          .catch ((error) => {
            if (error.status == 401) {
              this.$root.$emit('logout');
            }
            console.log(error);
          })
        }
      },
      send(message) {
        this.webSocket.send(JSON.stringify(message));
      },
      getChatters () {
        if (window.localStorage.getItem('token')) {
          axios.get('/account/conversations', {
            headers: { "Authorization": 'Bearer ' + window.localStorage.getItem('token') }
          })
          .then ((response) => {
            if (response.data) {
              this.chatters = response.data;
              setTimeout(() => this.handleScroll(), 25);
            }
          })
          .catch ((error) => {
            if (error.status == 401) {
              this.$root.$emit('logout');
            }
            console.log(error);
          })
        }
      },
      openChat(user) {
        if (!this.selected) this.selected = user;
        else if (this.selected.id == user.id) {
          this.selected = null;
        } else {
          this.selected = user;
        }
      },
      handleScroll () {
        let scroller = document.getElementById("scroller");
        if (scroller.scrollTop == 0) {
          document.getElementById("fade-top").classList.add('hidden');
        } else {
          document.getElementById("fade-top").classList.remove('hidden');
        }
        
        if (scroller.scrollTop + scroller.clientHeight == scroller.scrollHeight) {
          document.getElementById("fade-bot").classList.add('hidden');
        } else {
          document.getElementById("fade-bot").classList.remove('hidden');
        }
      },
      openConversation (user) {
        this.selected = null;
        if (this.chatters.find(e => e.id == user.id)) {
          this.chatters = this.chatters.filter(e => e.id != user.id);
        }
        this.chatters.unshift(user);
        this.selected = user;
      },
      logout () {
        this.$root.$emit('logout');
      },
      visitProfile () {
        this.$router.push('/' + JSON.parse(window.localStorage.getItem('session')).username, () => {});
        this.$root.$emit('setProfile');
      },
      openRequestsModal () {
        this.showRequestsModal = true;
      },
      closeRequestsModal () {
        this.showRequestsModal = false;
      },
      setFilter (filter) {
        this.currentFilter = filter;
        this.$root.$emit('setFilter', filter);
      },
      setFilterVisibility () {
        this.$route.params.username ? this.showFilter = true : this.showFilter = false;
      },
      getSession () {
        if (window.localStorage.getItem('session')) {
          axios.get('/users/' + JSON.parse(window.localStorage.getItem('session')).username)
          .then ((response) => {
            if (response.data) {
              this.session = response.data;
            }
          })
          .catch ((error) => {
            console.log(error);
          })
        }
      },
    },
    watch: {
      $route(to, from) {
        this.currentFilter = 'all';
        this.setFilterVisibility();
      }
    },
  })
  return Sidebar
})