
define(["Vue"], function(Vue) {
  let ProfileInfo = Vue.component("ProfileInfo", {
    template: `
    <div class="font-mono">
      <!--profile main-->
      <div class="block">
        <div class="mx-auto text-center">
          <div class="w-23 h-23 mx-auto rounded-full pointer-events-none select-none">
            <img :src="profile.avatar ? profile.avatar : './src/assets/images/profile.png'" alt="profile image"
            class="w-23 h-23 bg-white rounded-full mx-auto my-auto object-cover">
          </div>
          
          <!-- username -->
          <div class="flex justify-center space-x-2 mt-2"> 
            <span class="inline-block text-xl tracking-tight font-medium my-auto">
              {{ profile.username }}
            </span>

            <span v-if="profile.locked" class="inline-block my-auto">
              <i class="fas fa-lock text-gray-200"/>
            </span>
          </div>

          <button v-if="isMe()" @click="showEditProfileModal = true" class="mt-2 py-1 px-6
          bg-gray-900 hover:bg-gray-800 hover:text-white text-gray-200 shadow-md rounded-md">
            edit
          </button>

          <button v-if="!isMe() && !amGuest" @click="openConversation()" class="mt-2 mx-1 py-1 px-4 bg-gray-900
          hover:bg-gray-800 hover:text-white text-gray-200 shadow-md rounded-md">
            <div>
              <i class="fas fa-comment-dots" />
            </div>
          </button>

          <!-- friendship buttons -->
          <button v-if="buttons.send" @click="sendFriendRequest()" class="mt-2 mx-1 py-1 px-4 bg-gray-900
          hover:bg-gray-800 hover:text-white text-gray-200 shadow-md rounded-md">
            <div v-if="buttons.send">
              <div>
                <i class="fas fa-user-plus"/>
              </div>
            </div>
          </button>

          <button v-if="buttons.cancel" @click="cancelFriendRequest()" @mouseover="hover=true"
          @mouseleave="hover=false" class="mt-2 mx-1 py-1 px-4 bg-gray-900
          hover:bg-red-600 hover:text-white text-gray-200 shadow-md rounded-md">
            <div v-if="buttons.cancel">
              <div :class="hover ? 'hidden' : ''">
                <i class="fas fa-user-clock"/>
              </div>
              <div :class="!hover ? 'hidden' : ''">
                <i class="fas fa-user-times"/>
              </div>
            </div>
          </button>

          <button v-if="buttons.unfriend" @click="unfriend()" @mouseover="hover=true"
          @mouseleave="hover=false" class="mt-2 mx-1 py-1 px-4 bg-gray-900
          hover:bg-red-600 hover:text-white text-gray-200 shadow-md rounded-md">
            <div v-if="buttons.unfriend">
              <div :class="hover ? 'hidden' : ''">
                <i class="fas fa-user-check"/>
              </div>
              <div :class="!hover ? 'hidden' : ''">
                <i class="fas fa-user-minus"/>
              </div>
            </div>
          </button>

          <button v-if="buttons.accept" @click="acceptFriendRequest()" @mouseover="hover=true"
          @mouseleave="hover=false" class="mt-2 mx-1 py-1 px-4 bg-gray-900
          hover:bg-green-700 hover:text-white text-gray-200 shadow-md rounded-md">
            <div v-if="buttons.accept">
              <div>
                <i class="fas fa-check-circle"/>
              </div>
            </div>
          </button>

          <button v-if="buttons.decline" @click="declineFriendRequest()" @mouseover="hover=true"
          @mouseleave="hover=false" class="mt-2 mx-1 py-1 px-4 bg-gray-900
          hover:bg-red-600 hover:text-white text-gray-200 shadow-md rounded-md">
            <div v-if="buttons.decline">
              <div>
                <i class="fas fa-times-circle"/>
              </div>
            </div>
          </button>

          <!-- ban button -->
          <button v-if="amSuperuser() && !isMe()" @click="toggleBan()" class="mt-2 mx-1 py-1 px-4 bg-gray-900
          hover:text-white text-gray-200 shadow-md rounded-md"
          :class="this.profile.banned ? 'hover:bg-green-700' : 'hover:bg-red-600'">
            <div v-if="amSuperuser()">
              <div v-if="this.profile.banned">
                <div>
                  <i class="fas fa-undo"/>
                </div>
              </div>
              <div v-if="!this.profile.banned">
                <div>
                  <i class="fas fa-gavel"/>
                </div>
              </div>
            </div>
          </button>

        </div>
      </div>

      <!--posts and friends-->
      <div class="flex justify-center mt-6 text-center mx-auto space-x-2">

        <div @click="showPosts()" class="block w-24 py-1 hover:bg-gray-100 cursor-pointer rounded-lg">
          <div class="text-gray-900 font-bold">{{ profile.postCount }}</div>
          <div class="text-sm text-gray-400">Posts</div>
        </div>

        <div @click="tryShowFriends()" class="block w-24 py-1 hover:bg-gray-100 cursor-pointer rounded-lg">
          <div class="text-gray-900 font-bold">{{ profile.friendCount }}</div>
          <div class="text-sm text-gray-400">Friends</div>
        </div>
      </div>

      <!--profile description-->
      <div class="block text-left mt-10">
        <div class="text-gray-900 font-bold">{{profile.firstName}} {{profile.lastName}}</div>
        <div class="text-gray-500 mt-3 text-xs font-mono whitespace-pre-wrap">{{ profile.bio }}</div>
        <button v-if="profile.bio.length > 150" @click="handleShow" class="text-gray-500 mt-1 text-sm font-mono hover:text-gray-700">{{ showButtonText }}</button>
      </div>

      <Friends :profile="profile" v-if="showFriendsModal" v-on:close="showFriendsModal = false" />
      <EditProfile v-if="showEditProfileModal" v-on:close="showEditProfileModal = false" />
    </div>
    `,
    props: [ 'profile', 'friendshipData' ],
    data () {
      return {
        showMore: false,
        hover: false,
        buttons: {
          'send': false,
          'cancel': false,
          'accept': false,
          'decline': false,
          'unfriend': false,
        },
        showFriendsModal: false,
        showEditProfileModal: false,
        isFriend: false,
        amGuest: false
      }
    },
    mounted () {
      window.localStorage.getItem('token') ? this.amGuest = false : this.amGuest = true;
      this.getOptions();
    },
    methods: {
      getOptions () {
        if (!this.isMe() && window.localStorage.getItem('token')) {
          axios.get('/users/' + this.profile.username + '/friend', {
            headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
          })
          .then ((response) => {
            if (response.data) {
              this.isFriend = response.data.friends;
              for (const option of response.data.options) {
                this.buttons[option] = true;
              }
              this.sendProfileToPosts();
            }
          })
          .catch ((error) => {
            this.sendProfileToPosts();
          })
        } else {
          this.sendProfileToPosts();
        }
      },
      openConversation () {
        this.$root.$emit('openConversation',
          { username: this.profile.username, id: this.profile.id, avatar: this.profile.avatar }
        );
      },
      sendFriendRequest () {
        axios.post('/users/' + this.profile.username + '/friend/send', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.buttons.send = false;
          this.buttons.cancel = true;
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      cancelFriendRequest () {
        axios.post('/users/' + this.profile.username + '/friend/cancel', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.buttons.cancel = false;
          this.buttons.send = true;
        })
        .catch ((error) => {
          console.log(error);
        })

      },
      acceptFriendRequest () {
        axios.post('/users/' + this.profile.username + '/friend/accept', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.buttons.accept = false;
          this.buttons.decline = false;
          this.buttons.unfriend = true;
          this.hover = false;
          this.isFriend = true;
        })
        .catch ((error) => {
          console.log(error);
        })

      },
      declineFriendRequest () {
        axios.post('/users/' + this.profile.username + '/friend/decline', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.buttons.accept = false;
          this.buttons.decline = false;
          this.buttons.send = true;
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      unfriend () {
        axios.post('/users/' + this.profile.username + '/friend/unfriend', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.buttons.unfriend = false;
          this.buttons.send = true;
          this.isFriend = false;
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      toggleBan() {
        axios.post('/users/' + this.profile.username + '/ban', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.profile.banned = response.data.success.data.isBanned;
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      handleShow () {
        this.showMore = !this.showMore;
      },
      tryShowFriends() {
        if (!this.profile.locked || (this.profile.locked && this.isFriend) || this.isMe() || this.amSuperuser()) {
          this.showFriendsModal = true;
        } else {
          this.showFriendsModal = false;
        }
      },
      closeFriendsModal () {
        
      },
      closeEditProfileModal () {
        
      },
      showPosts () {
        if (this.isMe() && !this.$route.params.username) {
          this.$router.push('/' + this.profile.username, () => {});
        }
      },
      isMe () {
        if (window.localStorage.getItem('session') != null) {
          if (this.profile.id == JSON.parse(window.localStorage.getItem('session')).id) {
            return true;
          }
          return false;
        }
        return false;
      },
      amSuperuser() {
        if (window.localStorage.getItem('session')) {
          let role = JSON.parse(window.localStorage.getItem('session')).role;
          return (role == "SUPERUSER");
        }
      },
      sendProfileToPosts () {
        this.$root.$emit('setPostsProfile', this.profile, this.isFriend);
      },
    },
    computed: {
      description () {
        if (this.profile.bio.length > 150 && !this.showMore) {
          return this.profile.bio.substring(0, 150) + ' ...';
        }
        return this.profile.bio;
      },
      showButtonText () {
        if (this.showMore) {
          return 'show less'
        }
        return 'show more'
      }
    },
  })
  return ProfileInfo
})