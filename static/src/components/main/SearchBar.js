define(["Vue"], function(Vue) {
  let SearchBar = Vue.component("SearchBar", {
    template: `
    <div class="flex justify-between h-22 bg-white border-b border-gray-100 font-mono">
      <div class="flex select-none">
        <div class="h-full my-auto mt-5 ml-6 hidden lg:block">
          <a @click="openFeed">
            <img src="./src/assets/images/doberman2b.png" class="w-11 h-11 my-auto cursor-pointer" alt="app logo">
          </a>
        </div>
        <div class="my-auto">
          <a @click="openFeed">
            <h1 class="ml-2 text-2xl font-thin tracking-wide cursor-pointer">doberman</h1>
          </a>
        </div>
      </div>
      
      <!-- original design --> <!--
      <div class="my-auto mr-6">
        <input type="text" placeholder="> search" class="py-2 px-4 w-80 placeholder-gray-400 bg-gray-100
        rounded-lg text-lg outline-none focus:ring-2 ring-gray-200 text-gray-500" spellcheck="false">
      </div> -->

      <!-- new button design -->
      <button @click="showSearchModal = true" class="my-auto h-10 rounded-lg px-1 md:px-6 mr-6 bg-gradient-to-br from-pink-500
      to-rose-400 text-white text-xs sm:text-sm font-bold tracking-wide shadow-md transform hover:scale-95">
        <span>> search users</span>
      </button>

      <Search v-if="showSearchModal" v-on:close="showSearchModal = false"/>
    </div>
    `,
    data () {
      return {
        showSearchModal: false,
      }
    },
    methods: {
      openFeed () {
        if (this.$route.params.username) {
          this.$router.push('/', () => {});
          this.$root.$emit('setProfile');
        }
      },
    }
  })
  return SearchBar
})