define(["Vue"], function(Vue) {
  let Content = Vue.component("Content", {
    template: `
    <div id="content-view" class="block h-full bg-gradient-to-b from-gray-50 to-white border-r-2
    border-l-2 border-gray-100 font-mono">
      <SearchBar/>
      <div id="posts-view" class="overflow-y-auto">
        <Posts v-on:showButton="buttonShow = $event" ref="posts"/>

        <!-- bottom filler -->
        <div class="h-16 bg-transparent text-center">
          <div v-if="!canLoadMore && !$route.params.username">
            <div>
              <i class="fas fa-circle-notch text-gray-400 mx-auto animate-spin"/>
            </div>
            <div class="text-gray-400 text-sm tracking-wider">
              loading
            </div>
          </div>
        </div>
      </div>
    </div>
    `,
    data() {
      return {
        buttonShow: false,
        prevRemaining: 0,
        canLoadMore: true
      }
    },
    mounted () {
      this.$nextTick(function () {
        window.addEventListener('resize', this.handleResize);
        document.addEventListener("fullscreenChange", this.handleResize);
        setTimeout(() => { this.handleResize() }, 250);
      })

      this.prevRemaining = document.getElementById("posts-view").scrollHeight;
      document.getElementById("posts-view").addEventListener('scroll', this.waitForPause(95, this.onScroll));
    },
    methods: {
      waitForPause (ms, callback) {
        var timer;
        return function() {
            var that = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function() {
                callback.apply(that, args);
            }, ms);
        };
      },
      handleResize () {
        const chatBoxHeight = document.getElementById('content-view').clientHeight;
        document.getElementById('posts-view').style.height = (chatBoxHeight - 88) + 'px';
      },
      loadMore () {
        this.$refs.posts.loadMore();
      },
      onScroll () {
        let postsView = document.getElementById("posts-view");
        let remaining = postsView.scrollHeight - postsView.scrollTop - postsView.clientHeight;
        if ((remaining < this.prevRemaining && remaining < postsView.clientHeight / 2) || remaining == 0) {
          if (this.canLoadMore) {
            this.canLoadMore = false;
            let that = this;
            setTimeout(() => {
              that.canLoadMore = true;
              that.loadMore();
            }, 1750);
          }
        }
        this.prevRemaining = remaining;
      }
    },
    beforeDestroy () {
      window.removeEventListener('resize', this.handleResize);
      document.removeEventListener("fullscreenChange", this.handleResize);
      document.getElementById('posts-view').removeEventListener('scroll', this.onScroll);
    }
  })
  return Content
})