define(["Vue"], function(Vue) {
  Vue.use(vuelidate.default);
  let EditProfile = Vue.component("EditProfile", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-20 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      <div class="w-9/10 md:w-3/4 lg:w-2/3 xl:w-7/12 h-160 bg-white rounded-xl mx-auto
      overflow-hidden pt-10 px-8 pb-28 md:px-12 lg:px-16 xl:px-20 relative">
        
        <transition name="edit-profile-first">
          <div v-if="showFirst">
            <h1 class="mb-8 text-2xl tracking-widest">edit your profile</h1>

            <div class="grid grid-cols-2 grid-rows-1">
              <!-- profile picture -->
              <div class="flex justify-center">
                <img :src="avatar ? avatar : './src/assets/images/profile.png'" alt=""
                class="h-23 w-23 rounded-full object-cover">

                <div class="flex flex-col justify-evenly text-white ml-6 text-sm">

                  <!-- change photo -->
                  <button @click="next('avatar')" class="bg-gradient-to-tr from-red-400 to-pink-500
                  py-1.5 px-3 md:px-4 rounded-md
                  hover:bg-gradient-to-tr hover:from-red-500 hover:to-pink-500 shadow-sm">
                    <i class="far fa-images text-xs"/>
                    change
                  </button>

                  <!-- remove photo -->
                  <button @click="removeAvatar" class="py-1.5 px-3 md:px-4 rounded-md bg-gray-800 hover:bg-black shadow-sm">
                  <i class="far fa-trash-alt text-xs"/>
                    scrap
                  </button>
                </div>
              </div>

              <!-- private -->
              <div>

              </div>
            </div>
            
            <div class="grid grid-cols-2 grid-rows-1 space-x-4">
              <!-- first col -->
              <div class="block mt-4 text-left">

                <!-- first name -->
                <label class="text-xs ml-1.5">first name:</label>
                <input v-model="firstName" placeholder="first name"
                class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-base md:text-lg py-1
                focus:outline-none focus:border-gray-500 w-full caret-gray-700" 
                :class="$v.firstName.$dirty && $v.firstName.$invalid ? 'border-red-500' : ''"/>
                <div class="h-4">
                  <p v-if="$v.firstName.$dirty && !$v.firstName.required" class="text-xs text-red-500 my-0">Field required.</p>
                  <p v-if="$v.firstName.$dirty && !$v.firstName.maxLength" class="text-xs text-red-500 my-0">
                    First name must be shorter than 30 characters.
                  </p>
                </div>

                <!-- last name -->
                <label class="text-xs ml-1.5">last name:</label>
                <input v-model="lastName" placeholder="last name"
                class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-base md:text-lg py-1
                focus:outline-none focus:border-gray-500 w-full caret-gray-700"
                :class="$v.lastName.$dirty && $v.lastName.$invalid ? 'border-red-500' : ''"/>
                <div class="h-4">
                  <p v-if="$v.lastName.$dirty && !$v.lastName.required" class="text-xs text-red-500 my-0">Field required.</p>
                  <p v-if="$v.lastName.$dirty && !$v.lastName.maxLength" class="text-xs text-red-500 my-0">
                    Last name must be shorter than 30 characters.
                  </p>
                </div>

                <!-- email -->
                <label class="text-xs ml-1.5">email:</label>
                <input id="emailInput" type="email" v-model="email" placeholder="email"
                class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-base md:text-lg
                focus:outline-none focus:border-gray-500 w-full caret-gray-700 py-1"
                :class="$v.email.$dirty && $v.email.$invalid ? 'border-red-500' : ''"/>
                <div class="h-4">
                  <p v-if="$v.email.$dirty && !$v.email.required" class="text-xs text-red-500 my-0">Field required.</p>
                  <p v-if="$v.email.$dirty && !$v.email.email" class="text-xs text-red-500 my-0">Invalid email.</p>
                </div>

                <!-- password and visibility -->
                <div class="flex space-x-2 md:space-x-4">
                  <div class="block">
                    <label class="text-xs ml-1.5">password:</label>
                    <button @click="next('password')" type="button" class="block rounded-md shadow-sm
                    py-2 px-4 md:px-3 lg:px-4 xl:px-6 bg-red-500 hover:bg-red-600 font-medium text-white
                    focus:outline-none w-auto text-xs lg:text-sm">
                      change password
                    </button>
                  </div>
    
                  <div class="block">
                    <label class="text-xs ml-1.5">visibility:</label>
                    <button @click="locked = !locked" type="button" class="block rounded-md shadow-sm
                    py-2 px-4 md:px-3 lg:px-4 xl:px-6 font-medium text-white focus:outline-none w-auto text-xs lg:text-sm"
                    :class="locked ? 'bg-red-500 hover:bg-red-600' : 'bg-green-600 hover:bg-green-700'">
                      <span class="my-auto inline-block"><i class="fas fa-lock"/></span>
                      <span class="my-auto inline-block">{{ locked ? 'locked' : 'public' }}</span>
                    </button>
                  </div>
                </div>
          
              </div>

              <!-- second col -->
              <div class="block mt-4 text-left">

                <!-- birth date -->
                <label class="text-xs ml-1.5">birth date:</label>
                <input v-model="birthDate" placeholder="date of birth" type="date"
                class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-base md:text-lg py-1
                focus:outline-none focus:border-gray-500 w-full caret-gray-700"/>
                <div class="h-4"></div>
        
                <!-- gender -->
                <label class="text-xs ml-1.5">gender:</label>
                <select v-model="gender"
                class="block rounded-xl px-3 border border-gray-300 text-base md:text-lg py-1.5
                focus:outline-none focus:border-gray-500 w-full" :class="[gender == '' ? 'text-gray-400' : 'text-gray-700',
                $v.gender.$dirty && $v.gender.$invalid ? 'border-red-500' : '']">
                  <option value="" class="text-gray-400">gender</option>
                  <option value="M" class="text-gray-800">male</option>
                  <option value="F" class="text-gray-800">female</option>
                </select>
                <div class="h-4">
                  <p v-if="$v.gender.$dirty && !$v.gender.required" class="text-xs text-red-500 my-0">Field required.</p>
                </div>

                <!-- bio -->
                <label class="text-xs">profile bio:</label>
                <textarea v-model="bio" class="block w-full px-2 py-2 text-sm md:text-base font-normal
                text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded-md
                transition ease-in-out m-0 ocus:text-gray-700 focus:bg-white focus:border-gray-500
                focus:outline-none resize-none" rows="4" placeholder="describe yourself."/>
                <div class="h-4">
                  <p v-if="$v.bio.$dirty && !$v.bio.maxLength" class="text-xs text-red-500 my-0">
                    The bio must be shorter than 200 characters.
                  </p>
                </div>

              </div>
            </div>
          </div>
        </transition>
        
        <transition name="edit-profile-second">
          <div v-if="showPasswordChange">
            <EditPassword ref="editPassword"/>
          </div>
        </transition>

        <transition name="edit-profile-second">
          <div v-if="showAvatarChange">
            <ChooseAvatar ref="chooseAvatar" v-on:avatarSelected="onAvatarSelected" :username="currentAccount.username"/>
          </div>
        </transition>

        <!-- cancel/save -->
        <div class="bg-gray-100 h-20 px-4 py-5 sm:px-20 flex w-full justify-end absolute bottom-0 left-0">

          <button v-if="showFirst" @click="close" type="button" class="inline-flex justify-center rounded-md
          shadow-sm px-6 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-300 focus:outline-none
          w-auto sm:text-sm my-auto">
            cancel
          </button>

          <button v-if="!showFirst && (showPasswordChange || showAvatarChange)" @click="goBack" type="button" class="inline-flex
          justify-center rounded-md shadow-sm px-6 py-2 bg-white text-base font-medium text-gray-700
          hover:bg-gray-300 focus:outline-none w-auto sm:text-sm my-auto">
            back
          </button>

          <span v-if="showFirst" class="relative">
            <button type="button" @click="tryUpdateAccount" class="inline-flex justify-center
            rounded-md shadow-sm py-2 px-6 bg-gradient-to-br from-sky-500 to-blue-500
            hover:from-sky-500 hover:to-blue-600 text-base font-medium text-white hover:bg-blue-900
            focus:outline-none ml-3 w-auto sm:text-sm">
              save
            </button>
            <span v-if="hasUnsavedChanges" class="flex absolute h-3 w-3 -top-1 -right-1">
              <span class="absolute inline-flex h-full w-full rounded-full bg-fuchsia-600 opacity-75"
              :class="hasUnsavedChanges ? 'animate-ping' : ''"></span>
              <span class="relative inline-flex shadow-lg rounded-full h-3 w-3 bg-fuchsia-300 border-2 border-gray-100"></span>
            </span>
          </span>

          <button v-if="showPasswordChange || showAvatarChange" @click="onConfirm" type="button" class="inline-flex justify-center
          rounded-md shadow-sm py-2 px-6 bg-gradient-to-br from-sky-500 to-blue-500
          hover:from-sky-500 hover:to-blue-600 text-base font-medium text-white hover:bg-blue-900
          focus:outline-none ml-3 w-auto sm:text-sm my-auto">
            confirm
          </button>

        </div>

      </div>
    </div>
    `,
    data () {
      return {
        currentAccount: null,
        avatarID: -1,
        avatar: '',
        email: '',
        firstName: '',
        lastName: '',
        gender: '',
        birthDate: '',
        password: '',
        locked: false,
        bio: '',
        showFirst: true,
        showPasswordChange: false,
        showAvatarChange: false
      }
    },
    validations: {
      email: {
        email: validators.email,
        required: validators.required,
      },
      firstName: {
        required: validators.required,
        maxLength: validators.maxLength(30),
      },
      lastName: {
        required: validators.required,
        maxLength: validators.maxLength(30),
      },
      gender: {
        required: validators.required,
      },
      bio: {
        maxLength: validators.maxLength(200),
      }
    },
    mounted () {
      window.addEventListener('keydown', e => this.resetValidation(e));
      this.getAccount();
    },
    methods: {
      next (where) {
        let that = this;
        this.showFirst = false;
        where == 'password' ? setTimeout(() => { that.showPasswordChange = true }, 305)
        : setTimeout(() => { that.showAvatarChange = true })
      },
      goBack () {
        let that = this;
        this.showPasswordChange = false;
        this.showAvatarChange = false;
        setTimeout(() => { that.showFirst = true }, 305)
      },
      onAvatarSelected (photo) {
        this.avatar = photo.imageURI;
        this.avatarID = photo.id;
        this.goBack();
      },
      removeAvatar () {
        this.avatar = null;
        this.avatarID = -2;
      },
      tryUpdateAccount () {
        this.$v.$touch();
        if (!this.$v.$anyError) {
          this.updateAccount();
        }
      },
      updateAccount () {
        if (this.hasUnsavedChanges) {
          axios.put('/account', {
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            gender: this.gender,
            birthDate: this.birthDate,
            locked: this.locked,
            avatar: this.avatarID,
            bio: this.bio
          }, {
            headers: {
              'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            }
          })
          .then ((response) => {
            var that = this;
            setTimeout(function(){
              location.reload();
            }, 150)
          })
          .catch ((error) => {
            console.log(error);
          })
        }
      },
      getAccount () {
        axios.get('/account', {
          headers: {
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
          }
        })
        .then ((response) => {
          if (response.data) {
            let account = response.data;
            this.currentAccount = account;
            this.email = account.email;
            this.avatar = account.avatar;
            this.firstName = account.firstName;
            this.lastName = account.lastName;
            this.gender = account.gender;
            this.locked = account.locked;
            this.bio = account.bio;
            this.birthDate = account.birthDate;
          }
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      onConfirm () {
        if (this.showPasswordChange) {
          this.$refs.editPassword.tryChangePassword();
        } else if (this.showAvatarChange) {
          this.$refs.chooseAvatar.selectAvatar();
        }
      },
      resetValidation (e) {
        if (e.key != 'Enter') {
          this.$v.$reset();
        }
      },
      close () {
        this.$emit('close')
      },
    },
    beforeDestroy () {
      window.removeEventListener('keydown', e => this.resetValidation(e));
    },
    computed: {
      hasUnsavedChanges () {
        if (this.currentAccount) {
          if (this.currentAccount.email != this.email || this.currentAccount.firstName != this.firstName ||
          this.currentAccount.lastName != this.lastName || this.currentAccount.gender != this.gender ||
          this.currentAccount.birthDate != this.birthDate || this.currentAccount.bio != this.bio ||
          this.currentAccount.locked != this.locked || this.currentAccount.avatar != this.avatar ) {
            return true
          }
          return false;
        }
        return false;
      }
    }
  })
  return EditProfile
})