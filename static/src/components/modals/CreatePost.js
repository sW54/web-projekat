define(["Vue"], function(Vue) {
  let CreatePost = Vue.component("CreatePost", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-50 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      <div class="relative pb-28 flex flex-col w-3/4 sm:w-1/2 lg:w-1/3 h-160 bg-white rounded-xl mx-auto overflow-hidden p-10">

        <!-- publish choice -->
        <div class="text-2xl tracking-widest">publish a new</div>
        <div class="flex items-center justify-center mt-2">
          <div class="inline-flex" role="group">

            <button type="button" class="rounded-l inline-block px-6 py-2.5 bg-white
            border-y border-l border-gray-800 text-xs font-bold
            leading-tight uppercase hover:bg-gray-100 focus:bg-gray-100 focus:outline-none
            focus:ring-0" :class="choice == 'text' ?
            'bg-gradient-to-br from-red-400 to-pink-500 text-white' : 'text-gray-800'"
            @click="choice = 'text'; updatePreview(10);">
              Post
            </button>
            
            <button type="button" class="rounded-r inline-block px-6 py-2.5 bg-white
            border-y border-r border-gray-800 text-xs font-bold
            leading-tight uppercase hover:bg-gray-100 focus:bg-gray-100 focus:outline-none
            focus:ring-0" :class="choice == 'photo' ?
            'bg-gradient-to-br from-red-400 to-pink-500 text-white' : 'text-gray-800'"
            @click="choice = 'photo'; updatePreview(10);">
              Photo
            </button>
          </div>
        </div>

        <!-- text input -->
        <textarea v-model="content" class="block w-full px-3 py-1.5 text-base font-normal
        text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded
        transition ease-in-out m-0 ocus:text-gray-700 focus:bg-white focus:border-gray-500
        focus:outline-none resize-none mt-8" :rows="choice == 'text' ? 3 : 2"
        :placeholder="choice == 'text' ? 'express yourself.' : 'describe your photo.'"
        :class="choice == 'text' ? '' : 'order-1'" @keydown="error=''"/>

        <!-- photo upload -->
        <div class="flex justify-center">
          <div class="rounded-lg w-full" :class="choice == 'text' ? 'mt-6' : 'mt-8'">
            <div>
              <div class="flex items-center justify-center mx-auto">
                <label id="label-input" class="border-4 border-dashed hover:bg-gray-100
                hover:border-gray-300 cursor-pointer" :class="[choice == 'text' ? 'h-52' : 'h-64',
                image ? '' : 'flex flex-col w-5/6 sm:w-full mx-auto']">
                  <div v-if="!image" class="flex flex-col items-center justify-center pt-7 my-auto">
                    <i class="far fa-image w-12 h-12 text-gray-400 group-hover:text-gray-600"/>
                    <p class="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
                      Select a photo
                    </p>
                  </div>
                  <img v-else id="image-preview" :src='image' class="object-cover h-full mx-auto"/>
                  <input type="file" accept="image/*" @change="uploadImage" id="image-input" class="opacity-0" />
                </label>
              </div>
            </div>
          </div>
        </div>

        <div v-if="error" class="absolute bottom-20 left-0 mx-auto w-full h-4 text-xs text-red-600 order-last">
          {{ error }}
        </div>

        <!-- cancel/publish -->
        <div class="bg-gray-100 px-4 py-5 sm:px-6 flex w-full justify-end absolute bottom-0 left-0">
          <button @click="close" type="button" class="inline-flex justify-center rounded-md shadow-sm
          px-6 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-300 focus:outline-none
          w-auto sm:text-sm">
            cancel

          </button>
          <button @click="tryPublishPost" type="button" class="inline-flex justify-center
          rounded-md shadow-sm py-2 px-6 bg-gradient-to-br from-sky-500 to-blue-500
          hover:from-sky-500 hover:to-blue-600 text-base font-medium text-white hover:bg-blue-900
          focus:outline-none ml-3 w-auto sm:text-sm">
            publish
          </button>
        </div>
        
      </div>
    </div>
    `,
    data () {
      return {
        choice: 'text',
        content: '',
        image: null,
        imageFile: null,
        imageWidth: 'flex flex-col w-5/6 sm:w-full mx-auto',
        error: ''
      }
    },
    methods: {
      close () {
        this.$emit('close')
      },
      tryPublishPost () {
        if (this.isValidPost()) {
          this.prepareAndPublish();
        }
      },
      prepareAndPublish () {
        if (this.image) {
          let fileReader = new FileReader();
          fileReader.readAsDataURL(this.imageFile);
          let that = this;
          fileReader.onload = function(e) {
            var rawLog = fileReader.result;
            that.publish(rawLog);
          };
        } else {
          this.publish('');
        }
      },
      publish (photo) {
        let data = this.image ? {
          type: this.choice,
          content: this.content,
          imageData: photo
        } : {
          type: this.choice,
          content: this.content,
        }
        if (!this.content) {
          delete data.content;
        }
        axios.post('/posts', 
          data,
          {
            headers: {
              'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            }
          })
          .then ((response) => {
            this.$route.params.username ? this.handleProfileReload() : this.$root.$emit('reloadPosts');
            this.close();
          })
          .catch ((error) => {
            console.log(error);
          })
      },
      isValidPost () {
        this.validatePost();
        return this.error == '' ?  true : false;
      },
      validatePost() {
        if (this.content == '' && !this.image) {
          this.error = 'Give me some data to work with.';
        } else if (this.choice == 'text' && this.content == '') {
          this.error = 'A post must contain some text.';
        } else if (this.choice == 'photo' && !this.image) {
          this.error = 'Please upload a photo first.';
        } else if (this.content.length > 100) {
          this.error = 'Text is too long.';
        } else {
          this.error = '';
        }
      },
      uploadImage () {
        const [file] = document.getElementById('image-input').files;
        if (file) {
          this.imageFile = file;
          this.image = URL.createObjectURL(file);
          this.updatePreview(50);
        }
      },
      updatePreview (time) {
        if (this.image) {
          setTimeout(() => {
            let imageComponent = document.getElementById('image-preview');
            if (imageComponent) {
              document.getElementById('label-input').style.width = '100%';
              document.getElementById('label-input').style.width = imageComponent.clientWidth + 8 + 'px';
            }
          }, time)
        }
      },
      handleProfileReload () {
        const me = JSON.parse(window.localStorage.getItem('session')).username;
        if (this.$route.params.username == me) {
          this.$root.$emit('reloadPosts');
        } else {
          this.$router.push('/', () => {});
        }
      }
    },
  })
  return CreatePost
})