define(["Vue"], function(Vue) {
  Vue.use(vuelidate.default);
  let Login = Vue.component("Login", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-20 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      <div class="w-3/4 sm:w-1/2 md:w-2/5 lg:w-1/3 xl:w-1/4 bg-white rounded-xl mx-auto overflow-hidden">

        <h1 class="mt-6 text-2xl tracking-widest">login</h1>

        <div class="block px-6 sm:px-10 p-6">
          <!-- username -->
          <input id="usernameInput" @keydown.enter="tryLogin" v-model="username" placeholder="username"
          class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl py-1
          focus:outline-none focus:border-gray-500 w-full caret-gray-700"
          :class="$v.username.$dirty && $v.username.$invalid ? 'border-red-500' : ''"/>
          <div class="h-6">
            <p v-if="$v.username.$dirty && !$v.username.required" class="text-xs text-red-500 my-0">Field required.</p>
          </div>

          <!-- password -->
          <input @keydown.enter="tryLogin" type="password" v-model="password" placeholder="password"
          class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl py-1
          focus:outline-none focus:border-gray-500 w-full caret-gray-700" 
          :class="$v.password.$dirty && $v.password.$invalid ? 'border-red-500' : ''"/>
          <div class="h-2">
            <p v-if="$v.password.$dirty && !$v.password.required" class="text-xs text-red-500 my-0">Field required.</p>
          </div>

          <div class="h-2">
            <p v-if="error != ''" class="text-xs text-red-500 tracking-wide">{{ error }}</p>
          </div>

        </div>

        <div class="bg-gray-100 px-4 py-5 sm:px-6 flex justify-end">
          <button @click="close" type="button" class="inline-flex justify-center rounded-md shadow-sm
          px-6 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-300 focus:outline-none
          w-auto sm:text-sm">
          cancel
          </button>
          <button @click="tryLogin" type="button" class="inline-flex justify-center rounded-md shadow-sm
          px-6 py-2 bg-blue-700 text-base font-medium text-white hover:bg-blue-900 focus:outline-none ml-3
          w-auto sm:text-sm">
          sign in
          </button>
        </div>

      </div>
    </div>
    `,
    data () {
      return {
        username: '',
        password: '',
        error: ''
      }
    },
    validations: {
      username: {
        required: validators.required,
      },
      password: {
        required: validators.required,
      },
    },
    mounted () {
      window.addEventListener('keydown', e => this.resetValidation(e));
      document.getElementById('usernameInput').focus();
    },
    methods: {
      tryLogin () {
        // simulates login
        this.error = '';
        this.$v.$touch();
        if (!this.$v.$anyError) {
          this.login();
        }
      },
      login () {
        axios.post('/auth/login', { username: this.username, password: this.password })
        .then((response) => {
          window.localStorage.setItem('token', response.data.token);
          axios.get('/users/' + response.data.username, {
            headers: {
              'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            }
          })
          .then ((response) => {
            if (response.data) {
              window.localStorage.setItem('session', JSON.stringify({
                id: response.data.id,
                username: response.data.username,
                role: response.data.role
              }));
              this.$root.$emit('setProfile');
              this.$root.$emit('login');
              this.close();
            }
          })
          .catch ((error) => {
            console.log(error);
          })
        })
        .catch((error) => {
          this.error = error.response.data.error.message;
        });
      },
      close () {
        this.$emit('close')
      },
      resetValidation (e) {
        if (e.key != 'Enter') {
          this.$v.$reset();
          this.error = '';
        }
      }
    }
  })
  return Login
})