define(["Vue"], function(Vue) {
  let ChooseAvatar = Vue.component("ChooseAvatar", {
    template: `
    <div class="w-full h-120">
      <transition-group name="post-fade"
      class="overflow-auto grid grid-cols-3 sm:grid-cols-4 md:grid-cols-5 lg:grid-cols-6 gap-2 mt-4">
        <div v-for="photo in photos" :key="photo.id" class="p-1">
          <img @click="selected=photo" :src="photo.imageURI" :class="photo == selected ? 'outline outline-3 outline-purple-400' : ''"
          class="object-cover w-24 h-24 rounded-lg cursor-pointer mx-auto"/>
        </div>
      </transition-group>
    </div>
    `,
    props: ['username'],
    data () {
      return {
        photos: [],
        selected: null
      }
    },
    mounted () {
      this.getPhotos();
    },
    methods: {
      selectAvatar () {
        if (this.selected) {
          this.$emit('avatarSelected', this.selected);
        }
      },
      getPhotos () {
        axios.get('/users/' + this.username + '/posts/photos',
        { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
        .then ((response) => {
          if (response.data) {
            this.photos = response.data;
          }
        })
        .catch ((error) => {
        })
      },
    }
  })
  return ChooseAvatar
})