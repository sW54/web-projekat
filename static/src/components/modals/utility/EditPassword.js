define(["Vue"], function(Vue) {
  Vue.use(vuelidate.default);
  let EditPassword = Vue.component("EditPassword", {
    template: `
    <div class="block">

      <h1 class="mb-12 text-2xl tracking-widest">change password</h1>

      <!-- current password -->
      <input @keydown.enter="tryChangePassword" type="password" v-model="currentPassword" placeholder="current password"
      id="current-password-input" class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg py-1 mx-auto
      focus:outline-none focus:border-gray-500 w-9/10 sm:w-60 caret-gray-700"
      :class="$v.currentPassword.$dirty && $v.currentPassword.$invalid ? 'border-red-500' : ''"/>
      <div class="h-6">
        <p v-if="$v.currentPassword.$dirty && !$v.currentPassword.required" class="text-xs text-red-500 my-0">
          Field required.
        </p>
      </div>

      <!-- password -->
      <input @keydown.enter="tryChangePassword" type="password" v-model="newPassword" placeholder="new password"
      class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg py-1 mx-auto
      focus:outline-none focus:border-gray-500 w-9/10 sm:w-60 caret-gray-700"
      :class="$v.newPassword.$dirty && $v.newPassword.$invalid ? 'border-red-500' : ''"/>
      <div class="h-6">
        <p v-if="$v.newPassword.$dirty && !$v.newPassword.required" class="text-xs text-red-500 my-0">Field required.</p>
        <p v-if="$v.newPassword.$dirty && !$v.newPassword.minLength" class="text-xs text-red-500 my-0">
          Password must have at least 6 characters.
        </p>
      </div>

      <!-- repeat password -->
      <input @keydown.enter="tryChangePassword" type="password" v-model="repeatPassword" placeholder="repeat password"
      class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg py-1 mx-auto
      focus:outline-none focus:border-gray-500 w-9/10 sm:w-60 caret-gray-700"
      :class="$v.repeatPassword.$dirty && $v.repeatPassword.$invalid ? 'border-red-500' : ''"/>
      <div class="h-4">
        <p v-if="$v.repeatPassword.$dirty && !$v.repeatPassword.sameAsPassword" class="text-xs text-red-500 my-0">
          Passwords do not match.
        </p>
      </div>

      <div v-if="message" :class="message.includes('successfully') ? 'text-green-700' : 'text-red-600'">
        {{ message }}
      </div>

    </div>
    `,
    data () {
      return {
        currentPassword: '',
        newPassword: '',
        repeatPassword: '',
        message: ''
      }
    },
    validations: {
      currentPassword: {
        required: validators.required,
      },
      newPassword: {
        required: validators.required,
        minLength: validators.minLength(6),
      },
      repeatPassword: {
        required: validators.required,
        sameAsPassword: validators.sameAs('newPassword')
      },
    },
    mounted () {
      window.addEventListener('keydown', e => this.resetValidation(e));
    },
    methods: {
      tryChangePassword () {
        this.$v.$touch();
        if (!this.$v.$anyError) {
          this.changePassword();
        }
      },
      changePassword() {
        axios.post('/account/password', { oldPassword: this.currentPassword, newPassword: this.newPassword }, {
          headers: { 'Authorization': 'Bearer ' + window.localStorage.getItem('token') }
        })
        .then((response) => {
          console.log("SUCCESS");
          this.message = 'Password changed successfully.';
        })
        .catch(error => {
          console.log(error);
          this.message = 'Password was not changed.';
        });
      },
      resetValidation (e) {
        if (e.key != 'Enter') {
          this.$v.$reset();
        }
      }
    },
    beforeDestroy () {
      window.removeEventListener('keydown', e => this.resetValidation(e));
    }
  })
  return EditPassword
})