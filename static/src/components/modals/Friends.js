define(["Vue"], function(Vue) {
  let Friends = Vue.component("Friends", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-20 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      <div class="w-3/4 sm:w-72 bg-white rounded-xl mx-auto
      overflow-hidden h-112">

        <div class="w-full h-6 bg-gray-100 text-gray-300 font-extrabold tracing-widest align-middle">
          frens
        </div>

        <div class="overflow-auto h-full mt-1">
          <div v-for="friend in friends" :key="friend.id" class="h-12 m-0.5 mx-2">
            <div @click="goToProfile(friend.username)" class="flex justify-between my-auto rounded-xl p-0.5 hover:bg-gray-100  cursor-pointer">
              <div class="flex">
                <img :src="friend.avatar ? friend.avatar : './src/assets/images/profile.png'"
                class="h-10 w-10 rounded-full shrink-0 object-cover"/>

                <div class="block mx-2">
                  <div class="text-gray-700 text-sm text-left my-0 py-0">{{friend.username}}</div>
                  <div class="text-gray-400 text-xs text-left my-0 py-0">{{friend.firstName}} {{friend.lastName}}</div>
                </div>
              </div>

              <div class="my-auto mr-2" v-if="friend.isFriend">
                <i class="fas fa-user-check text-gray-300 text-lg"/>
              </div>
            </div>
          </div>
          <div v-if="friends.length > 8" class="h-6"></div>
          
        </div>

      </div>
    </div>
    `,
    props: ['profile'],
    data () {
      return {
        friends: []
      }
    },
    mounted () {
      this.getFriends();
    },
    methods: {
      goToProfile (username) {
        this.close();
        this.$router.push('/' + username, () => {});
        this.$root.$emit('setProfile');
      },
      close () {
        this.$emit('close')
      },
      getFriends () {
        axios.get('/users/' + this.profile.username + '/friend-list', {
          headers: {
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
          }})
        .then((response) => {
          if (response.data) {
            this.friends = response.data;
          }
        })
        .catch(error => {

        });
      }
    }
  })
  return Friends
})