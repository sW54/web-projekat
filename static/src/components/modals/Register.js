define(["Vue"], function(Vue) {
  Vue.use(vuelidate.default);
  let Register = Vue.component("Register", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-20 w-full min-h-screen h-screen
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity text-center">
      <div class="relative w-3/4 sm:w-1/2 md:w-2/5 lg:w-1/3 xl:w-1/4 lg:h-112 bg-white rounded-xl mx-auto overflow-hidden">
  
        <h1 class="mt-8 text-2xl tracking-widest">registration</h1>
        
        <!-- step 1 -->
        <transition name="registration-first">
          <div v-if="showFirst" class="block px-6 sm:px-10 p-8">

            <!-- email -->
            <input id="emailInput" @keydown.enter="next" type="email" v-model="email" placeholder="email"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl
            focus:outline-none focus:border-gray-500 w-full caret-gray-700 py-1"
            :class="$v.email.$dirty && $v.email.$invalid ? 'border-red-500' : ''"/>
            <div class="h-6">
              <p v-if="$v.email.$dirty && !$v.email.required" class="text-xs text-red-500 my-0">Field required.</p>
              <p v-if="$v.email.$dirty && !$v.email.email" class="text-xs text-red-500 my-0">Invalid email.</p>
            </div>

            <!-- username -->
            <input @keydown.enter="next" v-model="username" placeholder="username"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl py-1
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"
            :class="$v.username.$dirty && $v.username.$invalid ? 'border-red-500' : ''"/>
            <div class="h-6">
              <p v-if="$v.username.$dirty && !$v.username.required" class="text-xs text-red-500 my-0">Field required.</p>
              <p v-if="$v.username.$dirty && (!$v.username.minLength || !$v.username.maxLength)" class="text-xs text-red-500 my-0">
                Username must have between 3 and 16 characters.
              </p>
              <p v-if="$v.username.$dirty && !$v.username.regex && $v.username.minLength && $v.username.maxLength"
              class="text-xs text-red-500 my-0">
                Username invalid.
              </p>
            </div>

            <!-- password -->
            <input @keydown.enter="next" type="password" v-model="password" placeholder="password"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl py-1
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"
            :class="$v.password.$dirty && $v.password.$invalid ? 'border-red-500' : ''"/>
            <div class="h-6">
              <p v-if="$v.password.$dirty && !$v.password.required" class="text-xs text-red-500 my-0">Field required.</p>
              <p v-if="$v.password.$dirty && !$v.password.minLength" class="text-xs text-red-500 my-0">
                Password must have at least 6 characters.
              </p>
            </div>

            <!-- repeat password -->
            <input @keydown.enter="next" type="password" v-model="repeatPassword" placeholder="repeat password"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl py-1
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"
            :class="$v.repeatPassword.$dirty && $v.repeatPassword.$invalid ? 'border-red-500' : ''"/>
            <div class="h-4">
              <p v-if="$v.repeatPassword.$dirty && !$v.repeatPassword.sameAsPassword" class="text-xs text-red-500 my-0">
                Passwords do not match.
              </p>
            </div>

          </div>
        </transition>

        <!-- step 2 -->
        <transition name="registration-second">
          <div v-if="showSecond" class="block px-6 sm:px-10 p-8">

            <!-- first name -->
            <input @keydown.enter="tryRegistration" v-model="firstName" placeholder="first name"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl py-1
            focus:outline-none focus:border-gray-500 w-full caret-gray-700" 
            :class="$v.firstName.$dirty && $v.firstName.$invalid ? 'border-red-500' : ''"/>
            <div class="h-6">
              <p v-if="$v.firstName.$dirty && !$v.firstName.required" class="text-xs text-red-500 my-0">Field required.</p>
              <p v-if="$v.firstName.$dirty && !$v.firstName.maxLength" class="text-xs text-red-500 my-0">
                First name must be shorter than 30 characters.
              </p>
            </div>

            <!-- last name -->
            <input @keydown.enter="tryRegistration" v-model="lastName" placeholder="last name"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-lg lg:text-xl py-1
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"
            :class="$v.lastName.$dirty && $v.lastName.$invalid ? 'border-red-500' : ''"/>
            <div class="h-6">
              <p v-if="$v.lastName.$dirty && !$v.lastName.required" class="text-xs text-red-500 my-0">Field required.</p>
              <p v-if="$v.lastName.$dirty && !$v.lastName.maxLength" class="text-xs text-red-500 my-0">
                Last name must be shorter than 30 characters.
              </p>
            </div>

            <!-- gender -->
            <select v-model="gender"
            class="block rounded-xl px-3 border border-gray-300 text-lg lg:text-xl py-1.5
            focus:outline-none focus:border-gray-500 w-full" :class="[gender == '' ? 'text-gray-400' : 'text-gray-700',
            $v.gender.$dirty && $v.gender.$invalid ? 'border-red-500' : '']">
              <option value="" class="text-gray-400">gender</option>
              <option value="M" class="text-gray-800">male</option>
              <option value="F" class="text-gray-800">female</option>
            </select>
            <div class="h-6">
              <p v-if="$v.gender.$dirty && !$v.gender.required" class="text-xs text-red-500 my-0">Field required.</p>
            </div>
    
          </div>
        </transition>
  
        <div class="bg-gray-100 w-full px-4 py-5 sm:px-6 absolute bottom-0 flex justify-end">
          <button v-if="showFirst" @click="close" type="button" class="inline-flex justify-center rounded-md shadow-sm px-6
          py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-300 focus:outline-none w-auto sm:text-sm">
          cancel
          </button>
          <button v-if="showFirst" @click="next" type="button" class="inline-flex justify-center rounded-md shadow-sm
          px-6 py-2 bg-blue-700 text-base font-medium text-white hover:bg-blue-900 focus:outline-none
          ml-3 w-auto sm:text-sm">
          next
          </button>
          <button v-if="!showFirst" @click="goBack" type="button" class="inline-flex justify-center rounded-md shadow-sm px-6
          py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-300 focus:outline-none w-auto sm:text-sm">
          back
          </button>
          <button v-if="!showFirst" @click="tryRegistration" type="button" class="inline-flex justify-center rounded-md shadow-sm
          px-6 py-2 bg-blue-700 text-base font-medium text-white hover:bg-blue-900 focus:outline-none
          ml-3 w-auto sm:text-sm">
          sign up
          </button>
        </div>
  
      </div>
    </div>
    `,
    data () {
      return {
        showFirst: true,
        showSecond: false,
        email: '',
        username: '',
        password: '',
        repeatPassword: '',
        firstName: '',
        lastName: '',
        gender: '',
      }
    },
    validations: {
      email: {
        email: validators.email,
        required: validators.required,
      },
      username: {
        required: validators.required,
        minLength: validators.minLength(3),
        maxLength: validators.maxLength(16),
        regex: validators.helpers.regex('alpha', /^[a-zA-Z0-9_.-]*$/)
      },
      password: {
        required: validators.required,
        minLength: validators.minLength(6),
      },
      repeatPassword: {
        required: validators.required,
        sameAsPassword: validators.sameAs('password')
      },
      firstName: {
        required: validators.required,
        maxLength: validators.maxLength(30),
      },
      lastName: {
        required: validators.required,
        maxLength: validators.maxLength(30),
      },
      gender: {
        required: validators.required,
      },
    },
    mounted () {
      window.addEventListener('keydown', e => this.resetValidation(e));
      document.getElementById('emailInput').focus();
    },
    methods: {
      next () {
        this.$v.email.$touch();
        this.$v.username.$touch();
        this.$v.password.$touch();
        this.$v.repeatPassword.$touch();
        if (!this.$v.email.$error && !this.$v.username.$error &&
        !this.$v.password.$error && !this.$v.repeatPassword.$error) {
          let that = this;
          setTimeout(() => {that.showSecond = true}, 305);
          this.showFirst = false;
        }
      },
      goBack () {
        let that = this;
        setTimeout(() => {that.showFirst = true}, 305);
        this.showSecond = false;
      },
      tryRegistration () {
        this.$v.firstName.$touch();
        this.$v.lastName.$touch();
        this.$v.gender.$touch();
        if (!this.$v.$anyError) {
          this.register();
        }
      },
      register () {
        axios.post('/users/register', { 
          email: this.email,
          username: this.username,
          password: this.password,
          firstName: this.firstName,
          lastName: this.lastName,
          gender: this.gender,
        })
        .then((response) => {
          this.getLoginData(response);
        })
        .catch(error => {
          console.log(error);
          this.error = 'Wrong credentials.'
        });
      },
      getLoginData (response) {
        window.localStorage.setItem('token', response.data.token);
        console.log(response.data);
        axios.get('/users/' + response.data.username, {
          headers: {
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
          }
        })
        .then ((response) => {
          if (response.data) {
            window.localStorage.setItem('session', JSON.stringify({ id: response.data.id, username: response.data.username }));
            this.$root.$emit('setProfile');
            this.$root.$emit('login');
            this.close();
          }
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      close () {
        this.$emit('close')
      },
      resetValidation (e) {
        if (e.key != 'Enter') {
          this.$v.$reset();
        }
      }
    }
  })
  return Register
})