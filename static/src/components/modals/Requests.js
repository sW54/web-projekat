define(["Vue"], function(Vue) {
  let Requests = Vue.component("Requests", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-20 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      <div class="w-3/4 sm:w-100 bg-white rounded-xl mx-auto
      overflow-hidden h-112">

        <div class="w-full h-6 bg-gray-100 text-gray-300 font-extrabold tracing-widest align-middle">
          Pending friend requests
        </div>

        <div class="overflow-auto h-full mt-1">
          <div v-for="request in receivedRequests" :key="request.id" class="h-12 m-0.5 mx-2">
            <div @click="goToProfile(request.sender.username)" class="flex relative my-auto rounded-xl p-0.5 hover:bg-gray-100  cursor-pointer">
              <img :src="request.sender.avatar ? request.sender.avatar : './src/assets/images/profile.png'"
              class="h-10 w-10 rounded-full shrink-0 object-cover"/>
              
              <!-- user info -->
              <div class="block mx-2">
                <div class="text-gray-700 text-sm text-left my-0 py-0">{{request.sender.username}}</div>
                <div class="text-gray-400 text-xs text-left my-0 py-0">{{request.sender.firstName}} {{request.sender.lastName}}</div>
              </div>

              <!-- actions -->
              <div class="absolute right-0 bottom-1">
                <button @click.stop="acceptFriendRequest(request.sender.username)" class="w-12 mt-2 mx-1 py-1 px-4 bg-gray-900
                hover:bg-green-700 hover:text-white text-gray-200 shadow-md rounded-md">
                    <div>
                      <i class="fas fa-check-circle"/>
                    </div>
                </button>

                <button @click.stop="declineFriendRequest(request.sender.username)" class="w-12 mt-2 mx-1 py-1 px-4 bg-gray-900
                hover:bg-red-600 hover:text-white text-gray-200 shadow-md rounded-md">
                    <div>
                      <i class="fas fa-times-circle"/>
                    </div>
                </button>
              </div>
            </div>
          </div>

          <div v-for="request in sentRequests" :key="request.id" class="h-12 m-0.5 mx-2">
            <div @click="goToProfile(request.receiver.username)" class="flex relative my-auto rounded-xl p-0.5 hover:bg-gray-100  cursor-pointer">
              <img :src="request.receiver.avatar ? request.receiver.avatar : './src/assets/images/profile.png'"
              class="h-10 w-10 rounded-full shrink-0 object-cover"/>
              
              <!-- user info -->
              <div class="block mx-2">
                <div class="text-gray-700 text-sm text-left my-0 py-0">{{request.receiver.username}}</div>
                <div class="text-gray-400 text-xs text-left my-0 py-0">{{request.receiver.firstName}} {{request.receiver.lastName}}</div>
              </div>

              <!-- actions -->
              <div class="absolute right-0 bottom-1">
                <button @click.stop="cancelFriendRequest(request.receiver.username)" class="mt-2 w-12 mx-1 py-1 px-4 bg-gray-900
                hover:bg-red-600 hover:text-white text-gray-200 shadow-md rounded-md">
                    <div>
                      <i class="fas fa-user-times"/>
                    </div>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    `,
    props: ['profile'],
    data () {
      return {
        receivedRequests: [],
        sentRequests: []
      }
    },
    mounted () {
      this.getRequests();
    },
    methods: {
      goToProfile (username) {
        this.close();
        this.$router.push('/' + username, () => {});
        this.$root.$emit('setProfile');
      },
      close () {
        this.$emit('close')
      },
      getRequests () {
        axios.get('/account/requests', {
          headers: {
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
          }})
        .then((response) => {
          if (response.data) {
            this.receivedRequests = response.data.receivedRequests;
            this.sentRequests = response.data.sentRequests;
          }
        })
        .catch(error => {

        });
      },
      cancelFriendRequest (username) {
        axios.post('/users/' + username + '/friend/cancel', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.getRequests();
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      acceptFriendRequest (username) {
        axios.post('/users/' + username + '/friend/accept', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.getRequests();
        })
        .catch ((error) => {
          console.log(error);
        })

      },
      declineFriendRequest (username) {
        axios.post('/users/' + username + '/friend/decline', {}, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          this.getRequests();
        })
        .catch ((error) => {
          console.log(error);
        })
      },
    }
  })
  return Requests
})