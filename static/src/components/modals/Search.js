define(["Vue"], function(Vue) {
  let Search = Vue.component("Search", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-20 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      <div class="w-9/10 sm:w-3/4 lg:w-3/5 h-180 bg-white rounded-xl mx-auto overflow-hidden pt-6 px-10">

        <h1 class="text-2xl tracking-widest">search</h1>

        <!-- filter inputs -->
        <div class="grid grid-cols-2 xl:grid-cols-4 grid-rows-2 gap-x-4 gap-y-1 mt-4 text-left">

          <!-- first name -->
          <div>
            <label class="text-xs ml-1.5">first name:</label>
            <input @keydown.enter="doSearch" v-model="firstName" placeholder="first name"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-sm md:text-base xl:text-lg py-1
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"/>
          </div>

          <!-- last name -->
          <div>
            <label class="text-xs ml-1.5">last name:</label>
            <input @keydown.enter="doSearch" v-model="lastName" placeholder="last name"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-sm md:text-base xl:text-lg py-1
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"/>
          </div>

          <!-- born after -->
          <div>
            <label class="text-xs ml-1.5">born after:</label>
            <input v-model="bornAfter" placeholder="born before" type="date"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-sm md:text-base py-1.5
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"/>
          </div>

          <!-- born before -->
          <div>
            <label class="text-xs ml-1.5">born before:</label>
            <input v-model="bornBefore" placeholder="born before" type="date"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-sm md:text-base py-1.5
            focus:outline-none focus:border-gray-500 w-full caret-gray-700"/>
          </div>

          <!-- email -->
          <div v-if="amSuperuser()">
            <label class="text-xs ml-1.5">email:</label>
            <input @keydown.enter="doSearch" id="emailInput" type="email" v-model="email" placeholder="email"
            class="block rounded-xl px-3 border text-gray-700 border-gray-300 text-base md:text-lg
            focus:outline-none focus:border-gray-500 w-full caret-gray-700 py-1"/>
          </div>
          
          <div>
            <label class="text-xs ml-1.5">sort by:</label>
            <select v-model="sortCriteria"
            class="block rounded-xl px-3 border border-gray-300 text-base md:text-lg py-1.5
            focus:outline-none focus:border-gray-500 w-full" :class="sortCriteria == '' ? 'text-gray-400' : 'text-gray-700'">
              <option value="" class="text-gray-400">sort criteria</option>
              <option value="firstName" class="text-gray-800">first name</option>
              <option value="lastName" class="text-gray-800">last name</option>
              <option value="birthdate" class="text-gray-800">birth date</option>
            </select>
          </div>

          <div>
            <label class="text-xs ml-1.5">order:</label>
            <select v-model="order"
            class="block rounded-xl px-3 border border-gray-300 text-base md:text-lg py-1.5
            focus:outline-none focus:border-gray-500 w-full" :class="order == '' ? 'text-gray-400' : 'text-gray-700'">
              <option value="" class="text-gray-400">order</option>
              <option value="desc" class="text-gray-800">ascending</option>
              <option value="asc" class="text-gray-800">descending</option>
            </select>
          </div>
          <div :class="!amSuperuser() ? '' : 'hidden'"></div>

          <div>
            <button class="my-auto h-9 mt-6 w-full rounded-lg px-6 mr-6 bg-gradient-to-br from-pink-500
            to-rose-400 text-white text-sm font-bold tracking-wide shadow-md hover:bg-gradient-to-br
            hover:from-pink-500 hover:to-rose-500" @click="doSearch">
              search
            </button>
          </div>
        </div>

        <!-- users list -->
        <div class="overflow-auto max-h-104 xl:max-h-120 grid grid-cols-3 mt-3 border-t border-gray-100 pt-2 pb-2">
  
          <div v-for="user in users" :key="user.id" class="h-12 m-0.5 mx-2">
            <div @click="goToProfile(user.username)" class="flex my-auto rounded-xl p-0.5 hover:bg-gray-100  cursor-pointer">
              <img :src="user.avatar ? user.avatar : './src/assets/images/profile.png'"
              class="h-10 w-10 rounded-full shrink-0 object-cover"/>
              <div class="block mx-2">
                <div class="text-gray-700 text-sm text-left my-0 py-0">{{user.username}}</div>
                <div class="text-gray-400 text-xs text-left my-0 py-0">{{user.firstName}} {{user.lastName}}</div>
              </div>
            </div>
          </div>
          
        </div>

      </div>
    </div>
    `,
    data () {
      return {
        firstName: '',
        lastName: '',
        bornAfter: '',
        bornBefore: '',
        sortCriteria: '',
        order: '',
        email: '',
        users: [],
      }
    },
    mounted () {
      if (this.amSuperuser()) {
        this.doSearch();
      }
    },
    methods: {
      doSearch () {
        const params = {
          firstName: this.firstName,
          lastName: this.lastName,
          birthdateMin: this.bornAfter,
          birthdateMax: this.bornBefore,
          email: this.email,
          sort: this.sortCriteria,
          order: this.order
        };

        for (const key of Object.keys(params)) {
          if (params[key] === "") {
            delete params[key];
          }
        }
        if (Object.keys(params).length > 0 || this.amSuperuser()) {
          axios.get('/users', {
            params: params,
            headers: {
              'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            }
          })
          .then ((response) => {
            if (response.data) {
              this.users = response.data;
            }
          })
          .catch ((error) => {
            console.log(error);
          })
        }
      },
      goToProfile (username) {
        this.$router.push('/' + username, () => {});
        this.$root.$emit('setProfile');
      },
      close () {
        this.$emit('close')
      },
      amSuperuser() {
        if (window.localStorage.getItem('session')) {
          return (JSON.parse(window.localStorage.getItem('session')).role == "SUPERUSER");
        }
      },
    }
  })
  return Search
})