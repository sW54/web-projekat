define(["Vue"], function(Vue) {
  let DeleteReason = Vue.component("DeleteReason", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-50 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      <div class="relative pb-28 flex flex-col w-3/4 sm:w-1/2 lg:w-1/3 h-88
        bg-white rounded-xl mx-auto overflow-hidden p-10">

        <div class="text-2xl tracking-widest">explain the reason for deletion</div>

        <!-- text input -->
        <textarea v-model="content" class="block w-full px-3 py-1.5 text-base font-normal
        text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded
        transition ease-in-out m-0 ocus:text-gray-700 focus:bg-white focus:border-gray-500
        focus:outline-none resize-none mt-8" id="exampleFormControlTextarea1" rows="3"
        placeholder="example: Your post is deleted because it violates our terms and conditions."
        @keydown="error=''"/>

        <div v-if="error" class="absolute bottom-20 left-0 mx-auto w-full h-4 text-xs text-red-600 order-last">
          {{ error }}
        </div>

        <!-- skip/send -->
        <div class="bg-gray-100 px-4 py-5 sm:px-6 flex w-full justify-end absolute bottom-0 left-0">
          <button @click="close" type="button" class="inline-flex justify-center rounded-md shadow-sm
          px-6 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-300 focus:outline-none
          w-auto sm:text-sm">
          skip
          </button>
          <button @click="trySendMessage" type="button" class="inline-flex justify-center
          rounded-md shadow-sm x-6 py-2 px-6 bg-gradient-to-br from-sky-500 to-blue-500
          hover:from-sky-500 hover:to-blue-600 text-base font-medium text-white hover:bg-blue-900
          focus:outline-none ml-3 w-auto sm:text-sm">
          send
          </button>
        </div>
        
      </div>
    </div>
    `,
    props: [ 'author' ],
    data () {
      return {
        content: '',
        error: ''
      }
    },
    methods: {
      close () {
        this.$emit('close')
      },
      trySendMessage () {
        if (this.isValidMessage()) {
          this.sendMessage();
        }
      },
      sendMessage() {
        this.$root.$emit('sendDeleteReason', 
        { 
          content: "Your post has been deleted. Reason:", 
          receiver: this.author.username 
        });

        this.$root.$emit('sendDeleteReason', 
        { 
          content: this.content, 
          receiver: this.author.username 
        });

        this.close();
      },
      isValidMessage () {
        this.validateMessage();
        return this.error == '' ?  true : false;
      },
      validateMessage() {
        if (this.content == '') {
          this.error = 'A reason must contain some text.';
        } else if (this.content.length > 100) {
          this.error = 'Text is too long.';
        } else {
          this.error = '';
        }
      },
    },
  })
  return DeleteReason
})