define(["Vue"], function(Vue) {
  let DetailedPost = Vue.component("DetailedPost", {
    template: `
    <div @mousedown.self="close" class="fixed top-0 left-0 z-50 w-full min-h-screen h-screen text-center
    flex items-center justify-center bg-gray-900 bg-opacity-70 font-mono transition-opacity">
      
      <!-- modal itself -->
      <div id="modal" v-if="data" class="bg-transparent mx-auto overflow-hidden rounded-xl text-left">

        <!-- if it has image -->
        <div :class="data.imageURI ? 'md:flex flex-row w-full lg:w-auto h-180 md:h-120 xl:h-140' :
         'flex flex-col h-140'">
          <img id="post-image" v-if="data.imageURI" :src="data.imageURI" class="bg-white w-120 lg:w-auto h-88 md:h-120
          xl:h-140 object-cover"/>

          <div class="block relative bg-white p-4 flex-none" id="white-column"
          :class="data.imageURI ? 'w-full md:w-72 lg:w-80' : 'w-88 h-140'">

            <!-- user -->
            <div id="post-author" @mouseover="showDelete = true" @mouseleave="showDelete = false"
            class="flex justify-between pb-3 border-b border-gray-200">
              <div class="flex rounded-full">
                <img :src="data.author.avatar ? data.author.avatar : './src/assets/images/profile.png'" alt=""
                class="h-10 w-10 cursor-pointer rounded-full object-cover"
                @click="goToProfile(data.author.username)">
                <div class="block my-auto">
                  <div class="ml-2 text-sm font-medium text-gray-700 cursor-pointer"
                  @click="goToProfile(data.author.username)">{{ data.author.username }}</div>
                  <div class="ml-2 text-xs font-medium text-gray-300 cursor-pointer"
                  @click="goToProfile(data.author.username)">{{ data.author.firstName }} {{data.author.lastName}}</div>
                </div>
              </div>
              <div class="mx-2">

                <a v-if="((me && me.username == data.author.username) || amSuperuser()) && showDelete" @click="deletePost()">
                  <i class="fas fa-trash text-sm text-gray-300 hover:text-gray-500 cursor-pointer" />
                </a>
              </div>
            </div>

            <div id="comment-scroll" @mouseover="showDelete = true" @mouseleave="showDelete = false"
             class="overflow-auto h-auto hidden-scrollbar">

              <!-- description -->
              <div v-if="data.content && data.imageURI" class="flex border-b border-gray-200 py-2">
                <div class="max-w-full text-xs text-gray-500 p-1 my-auto font-bold break-words">
                  <i class="fas fa-quote-left text-gray-700 w-2 h-2 my-auto" />
                  {{ data.content }}
                </div>
              </div>

              <!-- post text -->
              <div v-if="data.content && !data.imageURI" class="flex border-b border-gray-200 py-2 leading-5">
                <div class="max-w-full text-gray-700 p-1 my-auto break-words">
                  {{ data.content }}
                </div>
              </div>
              
              <!-- comment section -->
              <div class="pt-1 pb-8">
                <div v-for="comment in data.comments.list" :key="comment.id">
                  <div @mouseover="showCommentOptions = comment.id" @mouseleave="showCommentOptions=''"
                  class="flex py-1">
                    <!-- comment author image -->
                    <div class="shrink-0 rounded-full">
                      <img :src="comment.author.avatar ? comment.author.avatar : './src/assets/images/profile.png'"
                      alt="" class="h-8 w-8 text-center mx-auto object-cover cursor-pointer rounded-full"
                      @click="goToProfile(comment.author.username)">
                    </div>

                    <div class="ml-2 leading-3 text-sm">
                      <!-- comment username and content -->
                      <div >
                        <span class="font-bold text-gray-700 cursor-pointer" @click="goToProfile(comment.author.username)">
                          {{ comment.author.username }}
                        </span>
                        <span class="ml-1 leading-4 text-gray-700 break-words max-w-full">
                          {{ comment.content }}
                        </span>
                      </div>

                      <!-- comment info -->
                      <div class="flex">
                        <div class="text-xs text-left truncate text-gray-500 mt-0.5" :title="altPostedTime(comment.postedAt)">
                          {{ postedTime(comment.postedAt) }}
                        </div>
                        <a v-if="me && me.username == comment.author.username && showCommentOptions == comment.id"
                        @click="letEditComment(comment.content); editingComment = comment.id;">
                          <i class="fas fa-edit text-xs font-bold my-auto mt-1 ml-3 text-gray-300
                          hover:text-gray-500 cursor-pointer" />
                        </a>
                        <a v-if="(me && me.username == comment.author.username || me && me.username == data.author.username)
                        && showCommentOptions == comment.id" @click="deleteComment(comment.id)">
                          <i class="fas fa-trash text-xs my-auto mt-1 ml-3 text-gray-300
                          hover:text-gray-500 cursor-pointer" />
                        </a>
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            </div>


            <!-- absolute element -->
            <div id="post-comment-element" class="absolute bottom-0 right-0 px-4 pt-2
            pb-4 w-full border-t border-gray-200 bg-white">

              <!-- like comment share -->
              <div class="flex py-2 gap-x-3">
                <a @click="toggleLike" :class="data.isLiked ? 'text-red-500 hover:text-gray-500' : 'text-gray-300 hover:text-red-500'">
                  <i class="fas fa-heart text-xl cursor-pointer" />
                </a>
                <a @click="selectCommentInput" class="mt-0 align-begin">
                  <i class="fas fa-comment text-gray-300
                  hover:text-gray-500 text-xl cursor-pointer"/>
                </a>
                <a>
                  <i class="fas fa-location-arrow text-gray-300 hover:text-gray-500 text-xl cursor-pointer" />
                </a>
              </div>
        
              <!-- liked by -->
              <div class="text-left text-xs px-1">
                <div v-html="likeMessage"></div>
              </div>
              <div class="text-xs text-left truncate text-gray-500 mt-0.5 px-1" :title="altPostedTime(data.postedAt)">
                {{ postedTime(data.postedAt) }}
              </div>

              <!-- comment input -->
              <input :disabled="!sessionActive()" id="comment" type="text"
              @keydown.enter="editingComment ? editComment(editingComment) : postComment()" v-model="comment"
              class="w-full py-1 px-2 mt-2 bg-transparent rounded-xl border border-gray-300 focus:border-gray-500
              text-gray-700 outline-none caret-gray-300" placeholder="comment...">

            </div>

          </div>

        </div>
      </div>

    </div>
    `,
    props: [ 'postId' ],
    data () {
      return {
        data: null,
        comment: '',
        me: null,
        showDelete: false,
        showCommentOptions: '',
        editingComment: '',
        beforeEditContent: ''
      }
    },
    mounted () {
      this.getMe();
      this.$nextTick(function () {
        window.addEventListener('resize', this.handleResize);
        this.getPostDetails();
      })
    },
    methods: {
      close () {
        this.$emit('close');
      },
      goToProfile (what) {
        this.$router.push('/' + what, () => {});
        this.$root.$emit('setProfile');
        this.close();
      },
      postComment () {
        axios.post('/comments', { postId: this.data.id, content: this.comment },
        { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
        .then ((response) => {
          if (response.data) {
            this.addComment(this.comment, response.data.success.data.commentId);
            this.comment = '';
            this.handleResize();
          }
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      editComment (id) {
        if (this.beforeEditContent != this.comment) {
          axios.put('/comments', { commentId: id, content: this.comment },
          { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
          .then ((response) => {
            if (response.data) {
              this.updateComment(id);
              this.handleResize();
            }
          })
          .catch ((error) => {
            console.log(error);
          })
        } else {
          this.comment = '';
          this.editingComment = '';
          this.beforeEditContent = '';
        }
      },
      deletePost () {
        axios.delete('/posts/' + this.data.id, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          if (this.amSuperuser() && !(this.me && this.me.username == this.data.author.username)) {
            this.$emit('showDeleteReasonModal', this.data.author);
          }

          if (response.data) {
            this.$emit('postDeleted', this.data.id);
            this.close();
          }
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      deleteComment (id) {
        axios.delete('/comments/' + id, {
          headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')}
        })
        .then ((response) => {
          if (response.data) {
            this.removeComment(id);
          }
        })
        .catch ((error) => {
          console.log(error);
        })
      },
      addComment (comment, id) {
        if (this.me) {
          this.data.comments.count++;
          this.data.comments.list.push({ author: this.me, content: comment, postedAt: (new Date()).toString(), id: id });
          this.autoscroll();
        }
      },
      removeComment (id) {
        this.data.comments.count--;
        this.data.comments.list = this.data.comments.list.filter(e => e.id != id);
      },
      updateComment (id) {
        this.data.comments.list.find(e => e.id == id).content = this.comment;
        this.comment = '';
        this.editingComment = '';
        this.beforeEditContent = '';
      },
      letEditComment (content) {
        this.beforeEditContent = content;
        this.editingComment = true;
        this.comment = content;
        this.selectCommentInput();
      },
      amSuperuser() {
        if (window.localStorage.getItem('session')) {
          let role = JSON.parse(window.localStorage.getItem('session')).role;
          return (role == "SUPERUSER");
        }
      },
      getMe () {
        if (window.localStorage.getItem('session')) {
          axios.get('/users/' + JSON.parse(window.localStorage.getItem('session')).username,
          { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
          .then ((response) => {
            if (response.data) {
              this.me = response.data;
            }
          })
          .catch ((error) => {
            console.log(error);
          })
        }
      },
      getPostDetails () {
        axios.get('/posts/' + this.postId,
        { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
        .then ((response) => {
          if (response.data) {
            this.data = response.data;
            setTimeout(() => {
              this.handleResize();
            }, 150);
          }
        })
        .catch ((error) => {
        })
      },
      toggleLike () {
        if (window.localStorage.getItem('token')) {
          axios.post('/posts/' + this.data.id + '/like', {},
          { headers: {'Authorization': 'Bearer ' + window.localStorage.getItem('token')} })
          .then ((response) => {
            this.data.isLiked = !this.data.isLiked;
            this.data.isLiked ? this.data.likeCount++ : this.data.likeCount--;
            this.$emit('toggleLike', this.data.id, this.data.isLiked);
          })
          .catch ((error) => {
            console.log(error);
          })
        }
      },
      selectCommentInput () {
        document.getElementById('comment').focus();
      },
      handleResize () {
        const modalHeight = document.getElementById('modal').clientHeight;
        const postAuthorHeight = document.getElementById('post-author').clientHeight;
        const postCommentElementHeight = document.getElementById('post-comment-element').clientHeight;
        document.getElementById('comment-scroll').style.height = (modalHeight - postAuthorHeight -
          postCommentElementHeight) + 'px';
        
        if (document.body.clientWidth < 768) {
          const postImageHeight = document.getElementById('post-image').clientHeight;
          document.getElementById('comment-scroll').style.height = (modalHeight - postImageHeight -
            postCommentElementHeight) + 'px';
          document.getElementById('white-column').style.height = (modalHeight - postImageHeight) + 'px';
        } else {
          document.getElementById('white-column').style.height = '';
        }
      },
      autoscroll() {
        let scrollElement = document.getElementById('comment-scroll');
        setTimeout(() => scrollElement.scrollTo(0, scrollElement.scrollHeight), 25);
      },
      postedTime (what) {
        let dt = new Date(what);
        
        let now = new Date();
        const minutesSince = Math.floor((now.getTime() - dt.getTime())/(1000 * 60));

        if (minutesSince == 0) {
          return 'now';
        }
        else if (minutesSince < 60) {
          return Math.floor(minutesSince) + ' minutes ago';
        }
        else if (minutesSince / 60 < 24) {
          return Math.floor(minutesSince / 60) + ' hours ago';
        }
        else if (minutesSince / ( 60 * 24) < 14) {
          return Math.floor(minutesSince / ( 60 * 24)) + ' days ago';
        }
        else  {
          return Math.floor(minutesSince / (60 * 24 * 7)) + ' weeks ago';
        }
      },
      altPostedTime (what) {
        let dt = new Date(what);
        let timeString = dt.toLocaleString(undefined, {
          hourCycle: 'h23',
          hour: '2-digit',
          minute: 'numeric',
        })
        let dateString = dt.toLocaleString(undefined, {
          weekday: 'long',
          day: 'numeric',
          month: 'short',
          year: 'numeric',
        })
        return timeString + ' ' + dateString;
      },
      sessionActive () {
        return window.localStorage.getItem('token') != null && window.localStorage.getItem('session') != null;
      }
    },
    computed: {
      likeMessage () {
        if (this.data.likeCount == 0) {
          return 'Be the first to like.';
        } else if (this.data.likeCount == 1) {
          return 'Liked by <b>1</b> person.';
        } else {
          return 'Liked by <b>' + this.data.likeCount + '</b> people.';
        }
      }
    },
    beforeDestroy () {
      window.removeEventListener('resize', this.handleResize);
    }
  })
  return DetailedPost
})