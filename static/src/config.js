require.config({
  baseUrl: "src",
  paths: {
      // from libs
      Vue: "lib/vue",
      VueRouter: "lib/vue-router",
    
      // from pages
      // Test: "./components/test.js",

      // main
      Router: "router"
  }
})