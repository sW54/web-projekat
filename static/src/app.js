define(function(require) {

  let Vue = require("Vue")
  let VueRouter = require("VueRouter")
  let Router = require("Router")
  // let Vuelidate = require('./src/lib/vuelidate.min.js');

  Vue.use(VueRouter);

  // components
  // main
  const MainLayout = require("./components/main/MainLayout")
  const Sidebar = require("./components/main/Sidebar")
  const Content = require("./components/main/Content")
  const SearchBar = require("./components/main/SearchBar")
  const Profile = require("./components/main/Profile")
  const ProfileInfo = require("./components/main/ProfileInfo")
  const Posts = require("./components/main/Posts")
  const Post = require("./components/main/Post")
  const Chat = require("./components/main/Chat")

  // modals
  const Login = require("./components/modals/Login")
  const Register = require("./components/modals/Register")
  const Friends = require("./components/modals/Friends")
  const DetailedPost = require("./components/modals/DetailedPost")
  const CreatePost = require("./components/modals/CreatePost")
  const Requests = require("./components/modals/Requests")
  const DeleteReason = require("./components/modals/DeleteReason")
  const EditProfile = require("./components/modals/EditProfile")
  const Search = require("./components/modals/Search")

  // modals/utility
  const EditPassword = require("./components/modals/utility/EditPassword")
  const ChooseAvatar = require("./components/modals/utility/ChooseAvatar")

  let App = new Vue({
      el: "#app",
      router: Router,
      components: {
        MainLayout: MainLayout,
        Sidebar: Sidebar,
        Content: Content,
        SearchBar: SearchBar,
        Profile: Profile,
        ProfileInfo: ProfileInfo,
        Posts: Posts,
        Post: Post,
        Chat: Chat,
        Login: Login,
        Register: Register,
        Friends: Friends,
        DetailedPost: DetailedPost,
        CreatePost: CreatePost,
        Requests: Requests,
        DeleteReason: DeleteReason,
        EditProfile: EditProfile,
        EditPassword: EditPassword,
        ChooseAvatar: ChooseAvatar,
        Search: Search,
        
      },
  })

})