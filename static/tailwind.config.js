tailwind.config = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {
      colors: {
        'doberman': '#ff49db',
      },
      spacing: {
        '22': '5.5rem',
        '23': '5.75rem',
        '84': '21rem',
        '88': '22rem',
        '100': '25rem',
        '104': '26rem',
        '112': '28rem',
        '120': '30rem',
        '140': '35rem',
        '160': '40rem',
        '180': '45rem',
        '1/6': '16.66%',
        '1/5': '20%',
        '9/10': '90%',
        '11/12': '91.66%',
        '7/12': '58.33%'
      }
    }
  },
}