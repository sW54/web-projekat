**Registracija**

POST    /api/register


**Login**

POST    /auth/login

GET     /auth/whoami


**Logout**

*client side*


**Izmena ličnih podataka**

GET     /account

POST    /account

POST    /account/password

GET     /account/pictures

POST    /account/avatar


**Pretraga korisnika**

GET     /users

GET     /users?firstname=

GET     /users?lastname=

GET     /users?birthdateMin=

GET     /users?birthdateMax=

GET     /users?sort=a,b,c (in order)


**Profil korisnika**

GET     /users/{id}

GET     /users/{id}/posts*(posebna)*

GET     /users/{id}/pictures*(posebna)*

GET     /users/{id}/mutual-friends

WS/chat?auth={jwt}&receiver={username}*(posebna)*


**Pregled sopstvenih slika**

GET     /account/pictures

POST    /account/pictures*(posebna)*

GET     /account/pictures/{id}

DELETE  /account/pictures/{id}


**Pregled sopstvenih objava**

GET     /account/posts

POST    /account/posts*(posebna)*

GET     /account/posts/{id}

DELETE  /account/posts/{id}


**Postavljanje slike**

POST    /account/pictures


**Postavljanje objave**

POST    /account/posts


**Detaljni prikaz slike ili objave**

GET     /posts/{id}

POST    /posts/{id}/comments


**Pregled objava drugih korisnika**

GET     /feed/posts/

DELETE  /comments/{id}


**Dodavanje prijatelja**

POST    /users/{id}/friendship

GET     /account/requests

GET     /account/requests?status=pending

POST    /account/requests/{id}*(posebna)*


**Pregled zahteva za prijateljstvo**

GET     /account/requests?status=pending

POST    /account/requests/{id}


**Lista prijatelja**

GET     /account/friends


**Prekidanje prijateljstva**

DELETE  /users/{id}/friendship

GET     /users/{id}/friendship


**Direktne poruke** (messages=conversations kod mene)

GET     /account/messages

GET     /account/messages?user=

GET     /account/messages/{id}


**Komentarisanje objava ili slika**

bilo


**Pregled svih korisnika**

GET     /users

GET     /users?firstname=

GET     /users?lastname=

GET     /users?email=

POST    /users/{id}/block

DELETE  /users/{id}/block


**Pregled svih objava i slika**

POST    /users/{id}/deletion
